//
//  optionsTableViewCell.m
//  Piczly
//
//  Created by sekhar on 23/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "optionsTableViewCell.h"

@implementation optionsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.optionsTF.delegate = self;
    _stringArray = [[NSMutableArray alloc] init];
    _stringDic = [[NSMutableDictionary alloc] init];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ((int)textField.tag == 0)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(getTexfieldIndexValue:)]) {
            [_delegate getTexfieldIndexValue:(int)textField.tag];
        }
    }
    if ((int)textField.tag == 1)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(getTexfieldIndexValue:)]) {
            [_delegate getTexfieldIndexValue:(int)textField.tag];
        }
    }
    if ((int)textField.tag == 2)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(getTexfieldIndexValue:)]) {
            [_delegate getTexfieldIndexValue:(int)textField.tag];
        }
    }
    if ((int)textField.tag == 3)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(getTexfieldIndexValue:)]) {
            [_delegate getTexfieldIndexValue:(int)textField.tag];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ((int)textField.tag == 0)
    {
        [_stringDic setValue:textField.text forKey:@"textField0"];
        if (_delegate && [_delegate respondsToSelector:@selector(getValuesOfTableView:andTagValue:)])
        {
            [_delegate getValuesOfTableView:textField.text andTagValue:(int)textField.tag];
        }
    }
    if (textField.tag == 1)
    {
        [_stringDic setValue:textField.text forKey:@"textField1"];
        if (_delegate && [_delegate respondsToSelector:@selector(getValuesOfTableView:andTagValue:)])
        {
            [_delegate getValuesOfTableView:textField.text andTagValue:(int)textField.tag];
        }
    }
    if (textField.tag == 2)
    {
        [_stringDic setValue:textField.text forKey:@"textField2"];
        if (_delegate && [_delegate respondsToSelector:@selector(getValuesOfTableView:andTagValue:)])
        {
            [_delegate getValuesOfTableView:textField.text andTagValue:(int)textField.tag];
        }
    }
    if (textField.tag == 3)
    {
        [_stringDic setValue:textField.text forKey:@"textField3"];
        if (_delegate && [_delegate respondsToSelector:@selector(getValuesOfTableView:andTagValue:)])
        {
            [_delegate getValuesOfTableView:textField.text andTagValue:(int)textField.tag];
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}

- (IBAction)optionsRemoveBtnAction:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(removeBtnPressed:value:button:)]) {
        
        [_delegate removeBtnPressed:self value:(int)sender.tag button:sender];
        
    }
    
    
}



@end
