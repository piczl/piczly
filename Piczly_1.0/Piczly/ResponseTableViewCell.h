//
//  ResponseTableViewCell.h
//  Piczly
//
//  Created by sekhar on 09/03/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResponseTableViewCell : UITableViewCell


@property (nonatomic,retain) IBOutlet UIImageView *imgview_userImg;
@property (nonatomic,retain) IBOutlet UILabel     *lbl_userName;
@property (nonatomic,retain) IBOutlet UILabel     *lbl_userDiscription;
@property (nonatomic,retain) IBOutlet UILabel     *lbl_userTime;




@end
