//
//  InviteViewController.h
//  Piczly
//
//  Created by sekhar on 20/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCZ_Piczly.h"

@protocol InviteViewDelegate <NSObject>

-(void)dataTransfertoViewController:(NSMutableArray*)arry;

@end

@protocol InviteViewDelegate;
@interface InviteViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *piczlyInviteTableView;
@property (strong, nonatomic)  NSMutableArray      *piczlyUsersArray;
@property (strong, nonatomic)  PCZ_Piczly          *piczly;
@property (weak , nonatomic)   id<InviteViewDelegate> delegate;
@property (strong , nonatomic) UIImage              *viewBackgroundImage;
@property (strong , nonatomic) NSString             *inviteMorepeople;
@property (strong, nonatomic) IBOutlet UIImageView *viewImageView;

@end
