//
//  ViewPCZCustomTableViewCell.h
//  Piczly
//
//  Created by sekhar on 25/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewPCZCustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;

@property (weak, nonatomic) IBOutlet UILabel *PCZQuesLabel;
@property (weak, nonatomic) IBOutlet UILabel *PCZDateLabel;


@end
