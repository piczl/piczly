//
//  PCZ_DetailView.h
//  Piczly
//
//  Created by Admin on 14/03/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@class PiczlyUser;
@class PCZ_Piczly;

@interface PCZ_DetailView : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString   *userAnswer;
@property (nonatomic, strong) NSNumber   *decision;
@property (nonatomic, strong) PiczlyUser *user_Info;
@property (nonatomic, strong) PCZ_Piczly *piczly_Info;
@property (nonatomic, strong) NSNumber   *answer_Type;

+(NSString*)parseClassName;



@end
