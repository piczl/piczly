//
//  HomeTableViewCell.m
//  Piczly
//
//  Created by Admin on 11/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)dropDownAction:(id)sender {
    
    UIButton *btn=(UIButton*)sender;
    btn.selected =!btn.selected;
    if(_HomeTableCell && [_HomeTableCell respondsToSelector:@selector(cellDidTapOnOptionsBtn:btnIndex:)]){
        [_HomeTableCell cellDidTapOnOptionsBtn:self btnIndex:(int)btn.tag];
        
        
    }
    
}
- (IBAction)createNewPiczlBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_HomeTableCell && [_HomeTableCell respondsToSelector:@selector(cellDidTapOnCreatePiczlyBasedOnItBtn:btnIndex:)]){
        [_HomeTableCell cellDidTapOnCreatePiczlyBasedOnItBtn:self btnIndex:(int)btn.tag];
        
    }
    
}
- (IBAction)reportPiczlBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_HomeTableCell && [_HomeTableCell respondsToSelector:@selector(cellDidTapOnReportThePiczlyBtn:btnIndex:)]){
        [_HomeTableCell cellDidTapOnReportThePiczlyBtn:self btnIndex:(int)btn.tag];
        
    }
    
}
- (IBAction)shareBtnAction:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    if(_HomeTableCell && [_HomeTableCell respondsToSelector:@selector(cellDidTapOnFBShareBtn:btnIndex:)]){
        [_HomeTableCell cellDidTapOnFBShareBtn:self btnIndex:(int)btn.tag];
        
    }
    
}
- (IBAction)seeOnMapBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_HomeTableCell && [_HomeTableCell respondsToSelector:@selector(cellDidTapOnSeeOnMapBtn:btnIndex:)]){
        [_HomeTableCell cellDidTapOnSeeOnMapBtn:self btnIndex:(int)btn.tag];
        
    }
    
    
}
@end
