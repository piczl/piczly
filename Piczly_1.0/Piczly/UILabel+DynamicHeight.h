//
//  UILabel+DynamicHeight.h
//  Breakk
//
//  Created by Hanuman Saini on 09/10/14.
//  Copyright (c) 2014 Umesh Naidu. All rights reserved.
//

#import <UIKit/UIKit.h>

#define iOS7_0 @"7.0"

@interface UILabel (DynamicHeight)

-(CGSize)sizeOfMultiLineLabel;
-(CGSize)sizeOfMultiLineLabelWithMaxHeight:(CGFloat)maxHeight;

@end
