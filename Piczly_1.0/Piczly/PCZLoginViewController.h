//
//  ViewController.h
//  Piczly
//
//  Created by sekhar on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"
#import "Define.h"
#import "Profile.h"
#import "PersistencyManager.h"
#import "PCZHomeViewController.h"

@interface PCZLoginViewController : UIViewController
{
    IBOutlet UIView      *registerPopupView,*loginPopupView,*forgotPwdView;
    IBOutlet UIButton    *rememberbtn;
    IBOutlet UITextField *usernameTxt,*passwordTxt,*forgotEmailTxt;
    PersistencyManager   *persistencyManager;
    NSMutableDictionary  *resultDict;
}
@property (strong, nonatomic) IBOutlet UIButton     *signinWithFB;
@property (strong, nonatomic) PCZHomeViewController *PCZHomeVC;


- (IBAction)signUpWithFBAction:(UIButton *)sender;


@end

