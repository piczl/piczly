//
//  CreatePiczlyViewController.m
//
//
//  Created by Mahesh on 19/02/15.
//
//

#import "CreatePiczlyViewController.h"
#import "PiczlyUser.h"
#import "FBHandler.h"
#import "InviteTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "AVCamPreviewView.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "PiczlyUser.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"



static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;



@interface CreatePiczlyViewController () <AVCaptureFileOutputRecordingDelegate,InviteViewDelegate>

{
    int i;
    CLLocationManager *locManager;
    NSMutableSet *selectedTF;
    optionsTableViewCell *cell;
    NSArray *piczlyUsersArray;
    NSMutableSet *selectedPiczlyUsersSet,*selectedUsersSet,*checkedUserList;
    NSNumber *longi;
    NSNumber *lat;
    FBHandler *fbHandler;
    NSMutableArray *arrayCheckUnchek,*selectedUserDetailsArray,*invitedPeople;
    UIImage *snapshotImage;
    
}
@property (strong, nonatomic) IBOutlet AVCamPreviewView *previewView;
@property (nonatomic, strong) UITextField *PCZTopicTF;

@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;





@end
NSMutableArray *checkedList;
@implementation CreatePiczlyViewController

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

#pragma mark View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _PCZAnswerDropdownBtn.tag=1;
    self.cameraBtnPrp.tag=130;
    selectedPiczlyUsersSet = [NSMutableSet set];
    piczlyUsersArray = [NSArray array];
    
    fbHandler = [[FBHandler alloc] init];
    arrayCheckUnchek = [[NSMutableArray alloc]init];
    checkedList=[[NSMutableArray alloc]init];
    self.piczlyInviteTableView.layer.borderWidth=0.1;
    [self.piczlyInviteTableView.layer setCornerRadius:10.0f];
    [self.piczlyInviteTableView.layer setMasksToBounds:YES];
    self.piczlyInviteTableView.layer.borderColor=[[UIColor grayColor] CGColor];
    
    arrayForUpdatingObjects=[[NSMutableArray alloc]init];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:53.0/255.0 blue:101.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    if(_piczly)
    {
        [self loadPiczly];
    }
    
    
    self.dropDownTableView.hidden=self;
    self.PCZMapview.hidden = YES;
    [self getTopicsData];
    UIBarButtonItem *cancelButtonitem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction:)];
    self.navigationItem.leftBarButtonItem = cancelButtonitem;
    self.navigationItem.title = @"New Piczl";
    
    
    _abcdBtn.hidden=YES;
    _yesnoBtn.hidden=YES;
    _addOptionBtn.hidden=YES;
    
    _optionsTableView.hidden=YES;
    _optionsTableView.backgroundColor = [UIColor clearColor];
    _optionsTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    arrForOptions=[[NSMutableArray alloc]init];
    arrForOptionsLabel = [[NSMutableArray alloc]initWithObjects:@"A",@"B",@"C",@"D", nil];
    _cancelPCZQuesBtn.hidden=YES;
    _confirmPCZQuesBtn.hidden=YES;
    _exceedBtn.hidden=YES;
    
    [_piczlyInviteTableView.layer setBorderWidth: 0.4];
    [_piczlyInviteTableView.layer setCornerRadius:15.0f];
    [_piczlyInviteTableView.layer setMasksToBounds:YES];
    [_piczlyInviteTableView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    UITapGestureRecognizer *tapGuester=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    [_scrollView addGestureRecognizer:tapGuester];
    
    UITapGestureRecognizer *tapGestureRecognizerForLbl = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped)];
    tapGestureRecognizerForLbl.numberOfTapsRequired = 1;
    [self.PCZAnswerLabel addGestureRecognizer:tapGestureRecognizerForLbl];
    self.PCZAnswerLabel.userInteractionEnabled = YES;
    if ([_editPiczy isEqualToString:@"Edit"])
    {
        self.sendToBtn.userInteractionEnabled=NO;
    }
    
    [self vedioCapture];
    [self storingTopicNames];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    selectedTF = [NSMutableSet set];
    selectedUsersSet=[[NSMutableSet alloc]init];
    checkedUserList=[[NSMutableSet alloc]init];
    selectedUserDetailsArray=[[NSMutableArray alloc]init];
    _tableViewTFValues = [[NSMutableDictionary alloc] init];
    [self getfacebookFriends];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[self session] startRunning];
}

-(void)storingTopicNames
{
    PFQuery *queryForTopicNames=[PFQuery queryWithClassName:@"Topics"];
    [queryForTopicNames findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [_topicNamesArray addObjectsFromArray:objects];
    }];
}

//for edit piczly

-(void)loadPiczly
{
    if([_createOnThtTopicRef isEqualToString:@"topic"])
    {
        self.PCZTopicLabel.text=self.piczly.topic;
    }else
    {
        
        
        
        self.PCZTopicLabel.text=self.piczly.topic;
        self.PCZQuestionTF.text=self.piczly.question;
        self.peopleAdded.text=_peopleAdded.text=[NSString stringWithFormat:@"%@ people added",self.piczly.invitedPeopleCount];
        
        PFFile *imgFile=self.piczly.image;
        [imgFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if(!error)
            {
                if ([_nameOfTheClass isEqualToString:@"Chat"])
                {
                    
                }
                else
                {
                    _PCZImageView.image=[UIImage imageWithData:data];
                }
            }
        }];
        
        
        if ([self.piczly.answerType doubleValue]==[[NSNumber numberWithInt:0] doubleValue])
        {
            self.PCZAnswerLabel.text=@"Yes-No";
        }
        else
        {
            self.PCZAnswerLabel.text=@"A-B-C-D";
        }
        
    }
    
    
    PFQuery *queryForInvitedFriends=[PFQuery queryWithClassName:@"InvitedFriends"];
    [queryForInvitedFriends includeKey:@"piczlyObjectId"];
    [queryForInvitedFriends includeKey:@"invitedFriends"];
    [queryForInvitedFriends whereKey:@"piczlyObjectId" equalTo:_piczly];
    [queryForInvitedFriends findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (objects.count>0) {
            [checkedList addObjectsFromArray:objects ];
            [checkedUserList addObjectsFromArray:[objects valueForKey:@"invitedFriends"]];
            [_piczlyInviteTableView reloadData];
        }
        
    }];
    
}

-(void)labelTapped
{
    if (_PCZAnswerDropdownBtn.tag==1)
    {
        _abcdBtn.hidden=NO;
        _yesnoBtn.hidden=NO;
        _PCZAnswerLabel.textColor=[UIColor blackColor];
        _PCZAnswerDropdownBtn.tag=2;
    }
    else if(_PCZAnswerDropdownBtn.tag==2)
    {
        _abcdBtn.hidden=YES;
        _yesnoBtn.hidden=YES;
        if (!([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) && !([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])) {
            _PCZAnswerLabel.textColor=[UIColor lightGrayColor];
            _PCZAnswerDropdownBtn.tag=1;
        }
        
    }
    
    
}
#pragma mark -AVCapture

-(void)vedioCapture
{
    //    [self setPreviewView:nil];
    [self setSession:nil];
    [self setSessionQueue:nil];
    
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    
    [self setSession:session];
    // Setup the preview view
    [[self previewView] setSession:session];
    
    // Check for device authorization
    [self checkDeviceAuthorizationStatus];
    
    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
    
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];
    
    dispatch_async(sessionQueue, ^{
        [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [CreatePiczlyViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error)
        {
            //for error message
        }
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                
                [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)[self interfaceOrientation]];
            });
        }
        
        
        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([session canAddOutput:stillImageOutput])
        {
            [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
            [session addOutput:stillImageOutput];
            [self setStillImageOutput:stillImageOutput];
        }
    });
    
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

#pragma mark UI

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[self previewView] layer] setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [[[self previewView] layer] setOpacity:1.0];
        }];
    });
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted)
        {
            //Granted access to mediaType
            [self setDeviceAuthorized:YES];
        }
        else
        {
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"AVCam!"
                                            message:@"AVCam doesn't have permission to use Camera, please change privacy settings"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}





-(void)getTopicsData
{
    PFQuery *topicsQry=[PCZ_topics query];
    [topicsQry findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            topics=[[NSMutableArray alloc]initWithArray:objects];
            [_dropDownTableView reloadData];
        }
    }];
}

//dismiss keyboard

-(void)hideKeyboard
{
    
    [self.view endEditing:YES];
    
    if (_PCZQuestionTF.text.length>25)
    {
        Com_AlertView(@"", @"Question length must be 25 characters only");
        [_PCZQuestionTF becomeFirstResponder];
        return;
    }
    else
    {
        _exceedBtn.hidden = YES;
        _cancelPCZQuesBtn.hidden = YES;
        _confirmPCZQuesBtn.hidden = YES;
    }
    
    
    if (i==2)
    {
        [_scrollView setContentOffset:CGPointMake(0,95) animated:YES];
    }
    else if (i==3)
    {
        [_scrollView setContentOffset:CGPointMake(0,168) animated:YES];
    }
    else if (i==4)
    {
        [_scrollView setContentOffset:CGPointMake(0,240) animated:YES];
    }
    else if (i==5)
        [_scrollView setContentOffset:CGPointMake(0,305) animated:YES];
}

#pragma mark - TextField Delegate Methods

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    self.scrollView.contentOffset = CGPointMake(0, 0);
    [UIView commitAnimations];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [UIView beginAnimations:@"Start" context:nil];
    [UIView  setAnimationDelay:0.1];
    
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _PCZQuestionTF)
    {
        
        [UIView beginAnimations:@"Start" context:nil];
        [UIView  setAnimationDelay:0.1];
        
        _cancelPCZQuesBtn.hidden=NO;
        _confirmPCZQuesBtn.hidden=NO;
        
        [UIView commitAnimations];
        self.scrollView.contentOffset = CGPointMake(0, 120);
    }
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _PCZQuestionTF)
    {
        newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [[UITextField appearance] setTintColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:1.0]];
        
        if (newString.length>25)
        {
            int value=(int)newString.length-25;
            [_exceedBtn setTitle:[NSString stringWithFormat:@"%d",value] forState:UIControlStateNormal];
            _exceedBtn.hidden=NO;
        }
        else
        {
            [_exceedBtn setTitle:@"0" forState:UIControlStateNormal];
            _exceedBtn.hidden=YES;
        }
    }
    return YES;
}
#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Pop to Previous controller

- (void)cancelButtonAction:(UIBarButtonItem*)sender
{
    
    UIAlertController *projectSelector = [UIAlertController alertControllerWithTitle:@"Changes will not be saved. Continue?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil]];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    
                                    if(self.piczly)
                                    {
                                        
                                        if ([_nameOfTheClass isEqualToString:@"Chat"])
                                        {
                                            [self dismissViewControllerAnimated:YES completion:nil];
                                        }
                                        else
                                        {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }
                                    }
                                    else if( [self.menuNavStr isEqual:@"createPiczlFromMenu"])
                                    {
                                        
                                        
                                        [self.navigationController popToRootViewControllerAnimated:NO];
                                        
                                    }
                                    
                                    else
                                    {
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                    }
                                    
                                }]];
    
    
    
    [self presentViewController:projectSelector animated:YES completion:nil];
    
}


- (IBAction)dropDownBtnAction:(UIButton *)sender
{
    sender.selected=!sender.selected;
    if (sender.selected)
        self.dropDownTableView.hidden=NO;
    else
        self.dropDownTableView.hidden=YES;
}


# pragma mark
# pragma mark create new Piczly topic


- (IBAction)createNewPiczlyAction:(UIButton *)sender
{
    _dropDownTableView.hidden = YES;
    
    
    UIAlertController *projectSelector = [UIAlertController alertControllerWithTitle:@"Create new topic" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ( self.PCZTopicTF.text.length < 1) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Wrong Information", nil) message:NSLocalizedString(@"Please enter topic name", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        }
        else
        {
            self.PCZTopicLabel.text = self.PCZTopicTF.text;
            self.PCZTopicLabel.textColor=[UIColor blackColor];
        }
    }]];
    [projectSelector addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        [textField setPlaceholder:@"Enter topic name"];
        self.PCZTopicTF = textField;
        
    }];
    [self presentViewController:projectSelector animated:YES completion:nil];
}



# pragma mark
#pragma mark camera button Action
- (IBAction)cameraBtnAction:(UIButton *)sender
{
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        if(_cameraBtnPrp.tag==130)
        {
            self.PCZImageView.hidden=NO;
            self.cameraBtnPrp.tag=131;
            dispatch_async([self sessionQueue], ^{
                // Update the orientation on the still image output video connection before capturing.
                
                [[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
                
                
                
                // Capture a still image.
                [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                    
                    if (imageDataSampleBuffer)
                    {
                        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                        UIImage *image = [[UIImage alloc] initWithData:imageData];
                        [[[ALAssetsLibrary alloc] init] writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:nil];
                        [self processImage:[UIImage imageWithData:imageData]];
                        
                        
                    }
                }];
            });
        }else if(_cameraBtnPrp.tag==131)
        {
            self.PCZImageView.hidden=YES;
            
            self.cameraBtnPrp.tag=130;
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                       message:@"Unable to find a camera on your device."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

#pragma mark
#pragma  mark ImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.PCZImageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark Processing Captured Image

- (void) processImage:(UIImage *)image
{ //process captured image, crop, resize and rotate
    
    //Device is iphone
    // Resize image
    UIGraphicsBeginImageContext(CGSizeMake(320, 426));
    [image drawInRect: CGRectMake(0, 0, 320, 426)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGRect cropRect = CGRectMake(0, 55, 320, 320);
    CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
    
    [self.PCZImageView setImage:[UIImage imageWithCGImage:imageRef]];
    
    CGImageRelease(imageRef);
    
    
    
    
}
-(AVCaptureVideoOrientation)interfaceToVideoOrientation
{
    switch ([UIApplication sharedApplication].statusBarOrientation)
    {
        case UIInterfaceOrientationPortrait:
            return AVCaptureVideoOrientationPortrait;
        case UIInterfaceOrientationPortraitUpsideDown:
            return AVCaptureVideoOrientationPortraitUpsideDown;
        case UIInterfaceOrientationLandscapeRight:
            return AVCaptureVideoOrientationLandscapeRight;
        case UIInterfaceOrientationLandscapeLeft:
            return AVCaptureVideoOrientationLandscapeLeft;
        default:
            return AVCaptureVideoOrientationPortrait;
    }
}


//Photo gallery for selecting the photo from our phone gallery

- (IBAction)photoLibraryAction:(UIButton *)sender
{
    self.PCZMapview.hidden = YES;
    self.PCZImageView.hidden=NO;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


#pragma marks
#pragma mark gpsLocationAction
- (IBAction)gpsLocationAction:(UIButton *)sender
{
    self.PCZMapview.hidden = NO;
    [self designMapViewContainer];
    
    self.PCZMapview.delegate = self;
    self.PCZMapview.showsUserLocation = YES;
    [self.PCZMapview setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    [self.PCZMapview setUserInteractionEnabled:YES];
    [self performSelector:@selector(getMapViewImage) withObject:sender afterDelay:5.0];
}

#pragma mark
#pragma mark Map image
-(void)getMapViewImage
{
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    options.region = _PCZMapview.region;
    options.scale = [UIScreen mainScreen].scale;
    options.size = _PCZMapview.frame.size;
    
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        UIImage *image = snapshot.image;
        
        CGRect finalImageRect = CGRectMake(0, 0, image.size.width, image.size.height);
        MKAnnotationView *pin = [[MKPinAnnotationView alloc] initWithAnnotation:nil reuseIdentifier:@""];
        UIImage *pinImage = pin.image;
        
        UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
        [image drawAtPoint:CGPointMake(0, 0)];
        for (id<MKAnnotation>annotation in self.PCZMapview.annotations)
        {
            CGPoint point = [snapshot pointForCoordinate:annotation.coordinate];
            if (CGRectContainsPoint(finalImageRect, point))
            {
                CGPoint pinCenterOffset = pin.centerOffset;
                point.x -= pin.bounds.size.width / 2.0;
                point.y -= pin.bounds.size.height / 2.0;
                point.x += pinCenterOffset.x;
                point.y += pinCenterOffset.y;
                
                [pinImage drawAtPoint:point];
            }
        }
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *data = UIImagePNGRepresentation(finalImage);
        _PCZImageView.image=[UIImage imageWithData:data];
        _PCZMapview.hidden = YES;
    }];
}

-(void)designMapViewContainer
{
    locManager = [[CLLocationManager alloc]init];
    locManager.delegate = self;
    if ([locManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locManager requestWhenInUseAuthorization];
    }
    [locManager startUpdatingLocation];
    
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
    
    lat = [NSNumber numberWithFloat:newLocation.coordinate.latitude];
    longi = [NSNumber numberWithFloat:newLocation.coordinate.longitude];
    [locManager stopUpdatingLocation];
    
}

#pragma mark
#pragma mark Submitting Data server

- (IBAction)postPiczlyBtnAction:(UIButton *)sender
{
    
    
    if(_piczly && ![_nameOfTheClass isEqualToString:@"Chat"])
    {
        
        
        PFObject *updatingObj = [PFObject objectWithoutDataWithClassName:@"Piczly" objectId:_piczly.objectId];
        
        
        if ([_PCZTopicLabel.text isEqualToString:@"Choose any piczl's topic.."])
        {
            Com_AlertView(@"", @"Please select topic");
            return;
        }
        if (_PCZQuestionTF.text.length>25)
        {
            Com_AlertView(@"", @"Question length must be 25 characters only");
            return;
        }
        else
        {
            _cancelPCZQuesBtn.hidden = YES;
            _confirmPCZQuesBtn.hidden = YES;
        }
        
        if (_PCZQuestionTF.text.length<1)
        {
            Com_AlertView(@"", @"Please enter question");
            return;
        }
        
        if (!([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) && !([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])) {
            Com_AlertView(@"", @"please select answer type");
            return;
        }
        if (arrForOptions.count>0 && arrForOptions.count<2) // Changed on 26th Feb
        {
            Com_AlertView(@"", @"Please select at least 2 options");
            return;
        }
        
        
        if ([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"] && arrForOptions.count<1)// Added on 26th Feb
        {
            Com_AlertView(@"", @"please enter options");
            return;
        }
        
        
        
        NSData *data = UIImagePNGRepresentation(self.PCZImageView.image);
        PFFile *imageFile = [PFFile fileWithName:@"image" data:data];
        
        
        
        updatingObj[@"topic"] = _PCZTopicLabel.text;
        updatingObj[@"image"] = imageFile;
        
        
        if (longi)
        {
            updatingObj[@"longitude"] = longi;
        }
        if (lat)
        {
            updatingObj[@"latitude"] = lat;
        }
        updatingObj[@"question"] = _PCZQuestionTF.text;
        updatingObj[@"createdBy"] = [PFUser currentUser];
        
        if ([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) {
            updatingObj[@"answerType"] = [NSNumber numberWithInt:0];
        }
        else if([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])
        {
            updatingObj[@"answerType"] = [NSNumber numberWithInt:1];
        }
        if([_tableViewTFValues valueForKey:@"textfield0"]){
            if([[_tableViewTFValues valueForKey:@"textfield0"] length]>0)
            {
                updatingObj[@"optionA"] = [_tableViewTFValues valueForKey:@"textfield0"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option A value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield1"]) {
            if([[_tableViewTFValues valueForKey:@"textfield1"] length]>0)
            {
                updatingObj[@"optionB"] = [_tableViewTFValues valueForKey:@"textfield1"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option B value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield2"]){
            if([[_tableViewTFValues valueForKey:@"textfield2"] length]>0)
            {
                updatingObj[@"optionC"] = [_tableViewTFValues valueForKey:@"textfield2"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option C value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield3"] ){
            if([[_tableViewTFValues valueForKey:@"textfield3"] length]>0)
            {
                updatingObj[@"optionD"] = [_tableViewTFValues valueForKey:@"textfield3"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option D value");
                return;
                
            }
        }
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [updatingObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded)
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                Com_AlertView(@"", @"Please try again");
            }
        }];
        
        
        
    }
    
    else
    {
        [cell.optionsTF resignFirstResponder];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        
        if ([_PCZTopicLabel.text isEqualToString:@"Choose any piczl's topic.."])
        {
            Com_AlertView(@"", @"Please select topic");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            return;
        }
        if (_PCZQuestionTF.text.length>25)
        {
            Com_AlertView(@"", @"Question length must be 25 characters only");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            return;
        }
        else
        {
            _cancelPCZQuesBtn.hidden = YES;
            _confirmPCZQuesBtn.hidden = YES;
        }
        
        if (_PCZQuestionTF.text.length<1)
        {
            Com_AlertView(@"", @"Please enter question");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            return;
        }
        
        if (!([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) && !([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])) {
            Com_AlertView(@"", @"please select answer type");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return;
        }
        if (arrForOptions.count>0 && arrForOptions.count<2) // Changed on 26th Feb
        {
            Com_AlertView(@"", @"Please select at least 2 options");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            return;
        }
        if (arrForOptions.count != _tableViewTFValues.count)
        {
            Com_AlertView(@"", @"please enter options");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return;
        }
        
        if ([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"] && arrForOptions.count<1)
        {
            Com_AlertView(@"", @"please enter options");
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            return;
        }
        
        NSDate *date=[NSDate date];
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"d.MM.yyyy"];
        NSData *data = UIImagePNGRepresentation(self.PCZImageView.image);
        PFFile *imageFile = [PFFile fileWithName:@"image" data:data];
        
        
        
        PFObject *obj = [PFObject objectWithClassName:@"Piczly"];
        obj[@"topic"] = _PCZTopicLabel.text;
        obj[@"image"] = imageFile;
        if (longi)
        {
            obj[@"longitude"] = longi;
        }
        if (lat)
        {
            obj[@"latitude"] = lat;
        }
        obj[@"question"] = _PCZQuestionTF.text;
        obj[@"createdBy"] = [PFUser currentUser];
        obj[@"piczlyStatus"]=[NSNumber numberWithInt:1];
        
        obj[@"createdDate"]=[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:date]];
        if([_nameOfTheClass isEqualToString:@"Chat"])
        {
            obj[@"invitedPeopleCount"]=[NSNumber numberWithInteger:checkedList.count];
        }
        else
        {
            obj[@"invitedPeopleCount"]=[NSNumber numberWithInteger:invitedPeople.count];
            
        }
        
        if ([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) {
            obj[@"answerType"] = [NSNumber numberWithInt:0];
        }
        else if([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])
        {
            obj[@"answerType"] = [NSNumber numberWithInt:1];
        }
        if([_tableViewTFValues valueForKey:@"textfield0"]){
            if([[_tableViewTFValues valueForKey:@"textfield0"] length]>0)
            {
                obj[@"optionA"] = [_tableViewTFValues valueForKey:@"textfield0"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option A value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield1"]) {
            if([[_tableViewTFValues valueForKey:@"textfield1"] length]>0)
            {
                obj[@"optionB"] = [_tableViewTFValues valueForKey:@"textfield1"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option B value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield2"]){
            if([[_tableViewTFValues valueForKey:@"textfield2"] length]>0)
            {
                obj[@"optionC"] = [_tableViewTFValues valueForKey:@"textfield2"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option C value");
                return;
                
            }
        }
        if([_tableViewTFValues valueForKey:@"textfield3"] ){
            if([[_tableViewTFValues valueForKey:@"textfield3"] length]>0)
            {
                obj[@"optionD"] = [_tableViewTFValues valueForKey:@"textfield3"];
            }
            else
            {
                Com_AlertView(@"", @"please enter option D value");
                return;
                
            }
        }
        
        
        
        [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded)
            {
                if(!error)
                {
                    PFQuery *queryForObj=[PFQuery queryWithClassName:@"Piczly"];
                    [queryForObj whereKey:@"topic" equalTo:_PCZTopicLabel.text];
                    [queryForObj findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        int count=(int)objects.count;
                        count++;
                        for (PFObject *updateTopicCount in objects)
                        {
                            updateTopicCount[@"topicCount"]=[NSNumber numberWithInt:count];
                            [updateTopicCount saveEventually];
                            
                        }
                    }];
                    if ([_nameOfTheClass isEqualToString:@"Chat"])
                    {
                        for(int k=0;k<[checkedList count];k++)
                        {
                            PFObject *objectForInvitedFriends=[PFObject objectWithClassName:@"InvitedFriends"];
                            objectForInvitedFriends[@"piczlyObjectId"]=obj;
                            objectForInvitedFriends[@"invitedFriends"]=[[checkedList objectAtIndex:k] valueForKey:@"invitedFriends"];
                            objectForInvitedFriends[@"AcceptanceStatus"]=[NSNumber numberWithInt:1];
                            [objectForInvitedFriends saveInBackground];
                        }
                    }
                    else
                    {
                        for(int k=0;k<[invitedPeople count];k++)
                        {
                            PFObject *objectForInvitedFriends=[PFObject objectWithClassName:@"InvitedFriends"];
                            objectForInvitedFriends[@"piczlyObjectId"]=obj;
                            objectForInvitedFriends[@"invitedFriends"]=[invitedPeople objectAtIndex:k];
                            objectForInvitedFriends[@"AcceptanceStatus"]=[NSNumber numberWithInt:1];
                            [objectForInvitedFriends saveInBackground];
                        }
                    }
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    Com_AlertView(@"", @"New piczl is successfully created.");
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                
                
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                Com_AlertView(@"", @"Please try again");
            }
        }];
    }
    
}
// Selecting Answertype options
- (IBAction)answerCategoriesBtnAction:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected)
    {
        _abcdBtn.hidden=NO;
        _yesnoBtn.hidden=NO;
        _PCZAnswerLabel.textColor=[UIColor blackColor];
    }
    else
    {
        _abcdBtn.hidden=YES;
        _yesnoBtn.hidden=YES;
        if (!([_PCZAnswerLabel.text isEqualToString:@"Yes-No"]) && !([_PCZAnswerLabel.text isEqualToString:@"A-B-C-D"])) {
            _PCZAnswerLabel.textColor=[UIColor lightGrayColor];
        }
        
    }
    
}

- (IBAction)yesnoBtnAction:(UIButton *)sender
{
    self.PCZAnswerDropdownBtn.selected = !self.PCZAnswerDropdownBtn.selected;
    _PCZAnswerLabel.text=@"Yes-No";
    _abcdBtn.hidden=YES;
    _yesnoBtn.hidden=YES;
    _addOptionBtn.hidden=YES;
    _optionsTableView.hidden=YES;
    [arrForOptions removeAllObjects];
    [_optionsTableView reloadData];
    i=1;
    _addOptionBtn.userInteractionEnabled=YES;
    _addOptionBtn.alpha=1.0;
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    
    
}

- (IBAction)abcdBtnAction:(UIButton *)sender
{
    
    self.PCZAnswerDropdownBtn.selected = !self.PCZAnswerDropdownBtn.selected;
    _PCZAnswerLabel.text=@"A-B-C-D";
    _abcdBtn.hidden=YES;
    _yesnoBtn.hidden=YES;
    if (i>1)
    {
        return;
    }
    i = 1;
    [_scrollView setContentOffset:CGPointMake(0, 50) animated:YES];
    _addOptionBtn.hidden=NO;
}


- (IBAction)addOptionsBtnAction:(UIButton *)sender
{
    i++;
    [_scrollView setContentOffset:CGPointMake(0, 61*i) animated:YES];
    //    i++;
    _optionsTableView.hidden=NO;
    [arrForOptions addObject:@"a"];
    [_optionsTableView reloadData];
    if (i==5)
    {
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, 800)];
        _addOptionBtn.alpha=0.5;
        _addOptionBtn.userInteractionEnabled=NO;
    }
    
}

#pragma mark TableView Delegate and Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView == self.optionsTableView)
    {
        return 70;
    }
    else if(tableView==self.piczlyInviteTableView)
        
    {
        return 60;
    }else
    {
        return 44;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.optionsTableView)
    {
        return arrForOptions.count;
    }
    else if (tableView == self.piczlyInviteTableView)
    {
        
        return [piczlyUsersArray count];
        
    }
    
    else
    {
        return topics.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.optionsTableView)
    {
        cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.backgroundColor = [UIColor clearColor];
        if (!cell) {
            cell=[[optionsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        
        
        
        
        cell.optionLabel.text = [arrForOptionsLabel objectAtIndex:indexPath.row];
        cell.delegate=self;
        
        cell.optionsRemoveBtn.tag=indexPath.row;
        cell.optionsTF.tag = (int)indexPath.row;
        
        return cell;
    }
    else if(tableView == _piczlyInviteTableView)
    {
        static NSString *cellIdentifier = @"cellIdentifier";
        
        InviteTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(!cell2)
        {
            
            cell2 = [[InviteTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
        
        PiczlyUser *user;
        
        user = [piczlyUsersArray objectAtIndex:indexPath.row];
        cell2.facebookFriendsImage.layer.cornerRadius = cell2.facebookFriendsImage.frame.size.width / 2;
        cell2.facebookFriendsImage.clipsToBounds = YES;
        [cell2.facebookFriendsImage.layer setBorderWidth:3.5];
        
        cell2.facebookFriendsImage.layer.borderColor = [[UIColor colorWithRed:248.0 green:248.0 blue:250.0 alpha:100]CGColor];
        cell2.delegateForInvite=self;
        cell2.facebookFriendsImage.image = [UIImage imageNamed:@"profile_default"];
        
        NSURL *url = [NSURL URLWithString:user.userImageFile.url];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage = [UIImage imageNamed:@"profile_default"];
        [cell2.facebookFriendsImage setContentMode:UIViewContentModeScaleAspectFit];
        
        
        NSSet *containsObject = [selectedUsersSet filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@", user.objectId]];
        
        
        
        if ([containsObject count] > 0)
        {
            cell2.chekBtn.selected = true;
            cell2.selected=true;
        }
        else
        {
            cell2.chekBtn.selected = false;
            cell2.selected=false;
        }
        
        [cell2.facebookFriendsImage setImageWithURLRequest:request
                                          placeholderImage:placeholderImage
                                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                       cell2.facebookFriendsImage.image = image;
                                                       [cell2.facebookFriendsImage setNeedsLayout];
                                                       
                                                   } failure:nil];
        
        cell2.facebookFriendsNames.text = user.displayName;
        
        
        
        
        
        if (checkedList.count>0)
        {
            NSSet *containsObject = [checkedUserList filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@", user.objectId]];
            
            
            
            if ([containsObject count] > 0)
            {
                cell2.chekBtn.selected = true;
                cell2.selected=true;
            }
            else
            {
                cell2.chekBtn.selected = false;
                cell2.selected=false;
            }
            
        }
        
        return cell2;
    }
    
    else
    {
        static NSString *str=@"topics";
        UITableViewCell *cell1=[tableView dequeueReusableCellWithIdentifier:str];
        pcz_topicsObj = [topics objectAtIndex:indexPath.row];
        
        if (!cell1) {
            cell1=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell1.textLabel.text=pcz_topicsObj.topicName;
        return cell1;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.optionsTableView)
    {
        
    }
    else
    {
        self.PCZTopicLabel.text=[[topics objectAtIndex:indexPath.row] valueForKey:@"topicName"];
        self.dropDownTableView.hidden=YES;
        _dropDownBtn.selected = !_dropDownBtn.selected;
        self.PCZTopicLabel.textColor=[UIColor blackColor];
    }
}


-(void)cellDidTapCheckedBtn:(InviteTableViewCell *)myCell isSelected:(BOOL)isSelected
{
    
    PiczlyUser *user=[piczlyUsersArray objectAtIndex:[_piczlyInviteTableView indexPathForCell:myCell].row];
    
    if (_piczly)
    {
        if (isSelected)
        {
            
            [checkedUserList addObject:user];
            [checkedList addObject:user];
        }
        else
        {
            NSSet *containsObject = [checkedUserList filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",user.objectId]];
            for (id czeUser in containsObject)
            {
                
                [checkedUserList removeObject:czeUser];
                [checkedList removeObject:czeUser];
            }
            
        }
        
    }
    else
    {
        if (isSelected)
        {
            
            [selectedUsersSet addObject:user];
            [selectedUserDetailsArray addObject:user];
        }
        else
        {
            NSSet *containsObject = [selectedUsersSet filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",user.objectId]];
            for (id czeUser in containsObject)
            {
                
                [selectedUsersSet removeObject:czeUser];
                [selectedUserDetailsArray removeObject:czeUser];
            }
            
        }
    }
    [_piczlyInviteTableView reloadData];
    
    
}


-(void)removeBtnPressed:(optionsTableViewCell *)myCell value:(int)myValue button:(UIButton *)btn
{
    
    i--;
    [_scrollView setContentOffset:CGPointMake(0, 61*i) animated:YES];
    [arrForOptions removeObjectAtIndex:myValue];
    for (int j = myValue; j < _tableViewTFValues.count; j++) // Added on 26th Feb
    {
        [_tableViewTFValues setValue:[_tableViewTFValues valueForKey:[NSString stringWithFormat:@"textfield%d",j+1]] forKey:[NSString stringWithFormat:@"textfield%d",j]];
    }
    if([_tableViewTFValues objectForKey:[NSString stringWithFormat:@"textfield%lu",(unsigned long)_tableViewTFValues.count]])
    {
        [_tableViewTFValues removeObjectForKey:[NSString stringWithFormat:@"textfield%lu",(unsigned long)_tableViewTFValues.count]];
    }
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    [self.optionsTableView beginUpdates];
    [self.optionsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.optionsTableView endUpdates];
    myCell.optionsTF.text = @"";
    
    
    if (arrForOptions.count<1)
    {
        [_scrollView setContentOffset:CGPointMake(0, 50) animated:YES];
        [_tableViewTFValues removeAllObjects];
    }
    [_optionsTableView reloadData];
    _addOptionBtn.userInteractionEnabled=YES;
    _addOptionBtn.alpha=1.0;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [_scrollView endEditing:YES];
}

-(void)getValuesOfTableView:(NSString*)dic andTagValue:(int)tag
{
    [_tableViewTFValues setValue:dic forKey:[NSString stringWithFormat:@"textfield%d",tag]];
    
}

-(void)getTexfieldIndexValue:(int)tfTag
{
    
    tfTagValueInt=tfTag;
    if (tfTag==0) {
        [_scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
        
    }
    else if (tfTag==1)
    {
        [_scrollView setContentOffset:CGPointMake(0, 265) animated:YES];
        
    }
    else if (tfTag==2)
    {
        [_scrollView setContentOffset:CGPointMake(0, 330) animated:YES];
    }
    else
    {
        [_scrollView setContentOffset:CGPointMake(0, 400) animated:YES];
    }
}

- (IBAction)cancelPCZQuesTFAction:(UIButton *)sender
{
    _PCZQuestionTF.text=@"";
    [_exceedBtn setTitle:@"0" forState:UIControlStateNormal];
    _exceedBtn.hidden = YES;
}

- (IBAction)confirmPCZQuesTFAction:(UIButton *)sender
{
    if (newString.length>25)
    {
        
        Com_AlertView(@"", @"Enter 25 characters only");
        return;
    }
    _cancelPCZQuesBtn.hidden = YES;
    
    sender.hidden = YES;
    [self.view endEditing:YES];
}




- (IBAction)sendToBtnAction:(UIButton *)sender
{
    
    UIView *subView = self.view;
    UIGraphicsBeginImageContextWithOptions(subView.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [subView.layer renderInContext:context];
    snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [self performSelector:@selector(nextViewController) withObject:nil afterDelay:0.5];
    
    
}
#pragma mark Navigating to InviteFriends

-(void)nextViewController
{
    if ([_nameOfTheClass isEqualToString:@"Chat"])
    {
        
    }
    else
    {
        InviteViewController *inviteVC=[self.storyboard instantiateViewControllerWithIdentifier:@"InviteVC"];
        inviteVC.delegate=self;
        inviteVC.piczly=_piczly;
        inviteVC.viewBackgroundImage=snapshotImage;
        inviteVC.piczlyUsersArray=[NSMutableArray arrayWithArray:piczlyUsersArray];
        [self presentViewController:inviteVC animated:YES completion:nil];
    }
    
}
-(void)dataTransfertoViewController:(NSMutableArray *)arry
{
    invitedPeople=[[NSMutableArray alloc]initWithArray:arry];
    _peopleAdded.text=[NSString stringWithFormat:@"%ld people added",[arry count]];
}

// Display facebook friends custom popup

- (IBAction)cancelBtnClicked:(id)sender
{
    
    _piczlyInviteView.hidden=YES;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
}




// invited people
- (IBAction)inviteeBtnClicked:(id)sender
{
    if (_piczly)
    {
        _piczlyInviteView.hidden=YES;
        _peopleAdded.text=[NSString stringWithFormat:@"%ld people added",[checkedList count]];
    }
    else{
        _piczlyInviteView.hidden=YES;
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
        _peopleAdded.text=[NSString stringWithFormat:@"%ld people added",[selectedUserDetailsArray count]];
    }
    
}


#pragma mark Fetch FaceBook Friends

- (void)getfacebookFriends
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Issue a Facebook Graph API request to get your user's friend list
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            // result will contain an array with your user's friends in the "data" key
            NSArray *friendObjects = [result objectForKey:@"data"];
            NSMutableArray *friendIds = [NSMutableArray arrayWithCapacity:friendObjects.count];
            // Create a list of friends' Facebook IDs
            for (NSDictionary *friendObject in friendObjects) {
                [friendIds addObject:[friendObject objectForKey:@"id"]];
            }
            
            PFQuery *friendQuery = [PiczlyUser query];
            [friendQuery whereKey:@"facebookId" containedIn:friendIds];
            [friendQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                
                piczlyUsersArray = [NSArray arrayWithArray:objects];
                [_piczlyInviteTableView reloadData];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }];
        }
    }];
}



@end
