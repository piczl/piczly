//
//  TextFieldCornerRadius.h
//  Piczly
//
//  Created by sekhar on 21/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface TextFieldCornerRadius : UITextField
@property (nonatomic, assign) int style;
@end
