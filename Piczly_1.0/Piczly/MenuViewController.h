//
//  MenuViewController.h
//  Piczly
//
//  Created by BHANU on 27/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PiczlyUser.h"

@interface MenuViewController : UITableViewController
@property NSMutableArray *dataArray;
@property NSMutableArray *settingsArray;
@property NSMutableArray *settingImage;
@property NSMutableArray *itemImage;

@end
