//
//  Detail_ViewPCZ_ABCDViewController.m
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "Detail_ViewPCZ_ABCDViewController.h"
#import "Session.h"
#import "CircleProgressView.h"
#import "PCZ_DetailView.h"
#import "PiczlyUser.h"
#import "ChatViewController.h"


#import "UIImageView+UIActivityIndicatorForSDWebImage.h"


@interface Detail_ViewPCZ_ABCDViewController ()
{
    PFObject     *detailViewObj;
    int          btnIndexValue,addThreeObjs,tableViewTotalCount,chatcount;
    NSDictionary *dictForSorting;
}
@property (nonatomic)         Session        *session;
@property (nonatomic, strong) PCZ_DetailView *PCZ_DetailObj,*PCZ_DetailObjByCurrentUser;
@property (nonatomic, strong) PiczlyUser     *PCZUser;

@end
UIView          *resizeView;
UIBarButtonItem *flipBtn;

@implementation Detail_ViewPCZ_ABCDViewController
@synthesize tablVCell_responseTable;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.txtf_because.textColor = [UIColor colorWithRed:72/255.0 green:72/255.0 blue:72/255.0 alpha:1];
    self.chatPopUp_View.layer.borderWidth=0.1;
    [self.chatPopUp_View.layer setCornerRadius:10.0f];
    [self.chatPopUp_View.layer setMasksToBounds:YES];
    self.chatPopUp_View.layer.borderColor=[[UIColor grayColor] CGColor];
    self.chat_View.layer.borderWidth=0.1;
    [self.chat_View.layer setCornerRadius:10.0f];
    [self.chat_View.layer setMasksToBounds:YES];
    self.chat_View.layer.borderColor=[[UIColor grayColor] CGColor];
    
    [self.chatPopUp_View addSubview:resizeView];
    //resizeView.hidden = YES;
    resizeView.backgroundColor=[UIColor cyanColor];
    self.becauseTxtf_ChatView.delegate =self;
    self.sendBtn  =[[UIButton alloc]initWithFrame:CGRectMake(275, 440, 30, 30)];
    [self.view addSubview:self.sendBtn];
    self.sendBtn.hidden =YES;
    [self.sendBtn addTarget:self action:@selector(sendBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    self.submitBtn.hidden = YES;
    self.becauseTxtf_View.hidden = NO;
    self.chatPopUp_View.hidden = YES;
    self.chat_View.hidden = NO;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:singleTap];
    
    _scrollPrp.tag=11;
    // Do any additional setup after loading the view.
    _flipView.hidden=YES;
    //UIProgressView Color bar code
    self.session = [[Session alloc] init];
    self.session.state = kSessionStateStop;
    _txtf_because.delegate=self;
    _view_flip.hidden=YES;
    
    self.makeDecissionView.hidden=YES;
    
    [self fetchingDataForTableViewAndQuardCurve];
    
    [self.navigationItem setTitle:@"Opened Piczl"];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor]];
    
    detailViewObj=[PFObject objectWithClassName:@"DetailView"];
    
    dictForSorting=[[NSMutableDictionary alloc]init];
    
    dataArray=[[NSMutableArray alloc]init];
    aDataArray=[[NSMutableArray alloc]init];
    bDataArray=[[NSMutableArray alloc]init];
    cDataArray=[[NSMutableArray alloc]init];
    dDataArray=[[NSMutableArray alloc]init];
    
    dataArrayForTableView=[[NSMutableArray alloc]init];
    addThreeObjs=3;
    _tableViewForUserAnswers.delegate=self;
    _tableViewForUserAnswers.dataSource=self;
    UISwipeGestureRecognizer *oneFingerSwipeRight =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    [ _detailPCZImg setUserInteractionEnabled:YES];
    [_detailPCZImg addGestureRecognizer:oneFingerSwipeRight];
    UISwipeGestureRecognizer *oneFingerSwipeLeft =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionRight];
    _view_flip.userInteractionEnabled = YES;
    [_view_flip addGestureRecognizer:oneFingerSwipeLeft];
    UISwipeGestureRecognizer *progressRight =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(progressswipeRight)];
    [progressRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    _img_ViewSwipe.userInteractionEnabled = YES;
    [_img_ViewSwipe addGestureRecognizer:progressRight];
    
    
    [self gettingDetailViewData];
    if ([_currentUser isEqualToString:@"currentUser"])
    {
        
    }
    else
    {
        _flipPrp.hidden=YES;
    }
    
    
    [self.circleProgressViewA setupView:[UIColor colorWithRed:39.0/255.0 green:105.0/255.0 blue:123.0/255.0 alpha:100.0]];
    [self.circleProgressViewB setupView:[UIColor colorWithRed:35.0/255.0 green:101.0/255.0 blue:134.0/255.0 alpha:100.0]];
    [self.circleProgressViewC setupView:[UIColor colorWithRed:60.0/255.0 green:103.0/255.0 blue:115.0/255.0 alpha:100.0]];
    [self.circleProgressViewD setupView:[UIColor colorWithRed:68.0/255.0 green:82.0/255.0 blue:117.0/255.0 alpha:100.0]];
    
    
    self.circleProgressViewA.tintColor=[UIColor colorWithRed:115.0/255.0 green:231.0/255.0 blue:137.0/255.0 alpha:100.0];
    self.circleProgressViewB.tintColor=[UIColor colorWithRed:115.0/255.0 green:231.0/255.0 blue:209.0/255.0 alpha:100.0];
    self.circleProgressViewC.tintColor=[UIColor colorWithRed:220.0/255.0 green:231.0/255.0 blue:115.0/255.0 alpha:100.0];
    self.circleProgressViewD.tintColor=[UIColor colorWithRed:231.0/255.0 green:115.0/255.0 blue:115.0/255.0 alpha:100.0];
    
    
    
    
    self.circleProgressViewA.timeLimit =100;
    
    
    self.circleProgressViewB.timeLimit =100;
    
    
    self.circleProgressViewC.timeLimit =100;
    
    
    self.circleProgressViewD.timeLimit =100;
    
    
    if (_PCZObj)
    {
        _aLabelPrp.text            = _PCZObj.optionA;
        _bLabelPrp.text            = _PCZObj.optionB;
        _cLabelPrp.text            = _PCZObj.optionC;
        _dLabelPrp.text            = _PCZObj.optionD;
        _detailOptionALbl.text     = _PCZObj.optionA;
        _detailOptionBLbl.text     = _PCZObj.optionB;
        _detailOptionCLbl.text     = _PCZObj.optionC;
        _detailOptionDLbl.text     = _PCZObj.optionD;
        _detailPCZTopicLbl.text    = _PCZObj.topic;
        _detailQusetionPCZLbl.text = _PCZObj.question;
        
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center           = _detailPCZImg.center;
        activityIndicator.hidesWhenStopped = YES;
        [_detailPCZImg addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        [_detailPCZImg sd_setImageWithURL:[NSURL URLWithString:_PCZObj.image.url] placeholderImage:[UIImage imageNamed:@"vdo_preview.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [activityIndicator stopAnimating];
             [activityIndicator removeFromSuperview];
         }];
        
    }
    [self  numofChatMessages];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

#pragma mark Keboard Disappearing

-(void)dismissKeyboard
{
    [self.txtf_because resignFirstResponder];
}

-(void)oneTap
{
    [self.view endEditing:YES];
    
}

#pragma mark-Parsing

-(void)gettingDetailViewData
{
    //detiled data about particular Piczly
    
    PFQuery *detailViewQuery=[PCZ_DetailView query];
    [detailViewQuery whereKey:@"piczly_Info" equalTo:self.PCZObj];
    [detailViewQuery whereKey:@"user_Info" equalTo:[PiczlyUser currentUser]];
    [detailViewQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (objects.count>0)
         {
             _PCZ_DetailObjByCurrentUser=[objects objectAtIndex:0];
             _txtf_because.text=_PCZ_DetailObjByCurrentUser.userAnswer;
             
             detailViewObj[@"decision"]=_PCZ_DetailObjByCurrentUser.decision;
             
             if ([_PCZ_DetailObjByCurrentUser.decision intValue]==0)
             {
                 
                 [_aBtnPrp setBackgroundImage:[UIImage imageNamed:@"circleforchekmark.png"]
                                     forState:UIControlStateNormal];
             }
             else if([_PCZ_DetailObj.decision intValue]==1)
             {
                 [_bBtnPrp setBackgroundImage:[UIImage imageNamed:@"circleforchekmark.png"]
                                     forState:UIControlStateNormal];
             }
             else if([_PCZ_DetailObj.decision intValue]==2)
             {
                 [_cBtnPrp setBackgroundImage:[UIImage imageNamed:@"circleforchekmark.png"]
                                     forState:UIControlStateNormal];
             }
             else if([_PCZ_DetailObj.decision intValue]==3)
             {
                 [_dBtnPrp setBackgroundImage:[UIImage imageNamed:@"circleforchekmark.png"]
                                     forState:UIControlStateNormal];
             }
         }
         
     }];
    
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Piczly Graph Distribution

- (IBAction)flipBtnClicked:(id)sender
{
    
    if([[PiczlyUser currentUser].objectId isEqualToString:_PCZObj.createdBy.objectId])
    {
        _flipPrp.hidden=YES;
        _flipView.hidden=NO;
        _makeDecissionView.hidden=NO;
        
        [self fetchingDataForTableViewAndQuardCurve];
        
        UIButton *but=[UIButton buttonWithType:UIButtonTypeSystem];
        but.frame=CGRectMake(0, 0, 30, 30) ;
        [but setBackgroundImage:[UIImage imageNamed:@"flipBackImge.png"] forState:UIControlStateNormal];
        [but addTarget:self action:@selector(flipBackClicked:) forControlEvents:UIControlEventTouchUpInside];
        flipBtn=[[UIBarButtonItem alloc]initWithCustomView:but];
        self.navigationItem.rightBarButtonItem=flipBtn;
        
        
        
        [UIView transitionWithView:_flipView duration:.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^
         {
             
         } completion:^(BOOL finished)
         {
         }];
        
    }
    else
    {
        self.flipView.hidden=NO;
        
        UIImageView *imgView=[[UIImageView alloc]initWithFrame:_flipView.bounds];
        [_flipView addSubview:imgView];
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:_PCZObj.image.url] placeholderImage:[UIImage imageNamed:@"profile_bg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.makeDecissionView.hidden=NO;
    }
}
- (IBAction)flipBackClicked:(id)sender
{
    _becauseTxtf_View.hidden = NO;
    _flipPrp.hidden=NO;
    _flipView.hidden=YES;
    _makeDecissionView.hidden=YES;
    self.navigationItem.rightBarButtonItem.enabled=NO;
    self.navigationItem.rightBarButtonItem=nil;
    
    [_scrollViewForFullView setContentOffset:CGPointMake(0, 0) animated:NO];
    [_scrollPrp setImage:[UIImage imageNamed:@"downArow.png"] forState:UIControlStateNormal];
    
    [self.view endEditing:YES];
    [UIView transitionWithView:_flipView duration:.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        
    } completion:^(BOOL finished){
    }];
}

#pragma mark - TableView DataSource and Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArrayForTableView.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ResponseTableViewCell";
    ResponseTableViewCell *cell = (ResponseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"CustomtableviewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        
        
    }
    _PCZ_DetailObj=[dataArrayForTableView objectAtIndex:indexPath.row];
    _PCZUser=_PCZ_DetailObj.user_Info;
    cell.lbl_userName.text=_PCZUser.username;
    cell.lbl_userDiscription.text=_PCZ_DetailObj.userAnswer;
    
    cell.imgview_userImg.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.imgview_userImg.layer.cornerRadius=cell.imgview_userImg.frame.size.height/2;
    cell.imgview_userImg.layer.borderWidth=4.0;
    cell.imgview_userImg.layer.masksToBounds = YES;
    cell.imgview_userImg.layer.borderColor=[[UIColor grayColor] CGColor];
    [cell.imgview_userImg sd_setImageWithURL:[NSURL URLWithString:_PCZUser.userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    NSString *str=[self.session timeLeftSinceDate:_PCZ_DetailObj.updatedAt];
    str=[str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    cell.lbl_userTime.text=str;
    
    
    return cell;
}


#pragma mark - Button Actions

- (IBAction)loadMoreBtnAction:(UIButton *)sender
{
    
    
    tableViewTotalCount=(int)[dataArrayForTableView count];
    
    [dataArrayForTableView removeAllObjects];
    if (btnIndexValue==0)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (aDataArray.count>i)
            {
                [dataArrayForTableView addObject:[aDataArray objectAtIndex:i]];
                
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    else if (btnIndexValue==1)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (bDataArray.count>i)
            {
                [dataArrayForTableView addObject:[bDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    else if (btnIndexValue==2)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (cDataArray.count>i)
            {
                [dataArrayForTableView addObject:[cDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    else if (btnIndexValue==3)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (dDataArray.count>i)
            {
                [dataArrayForTableView addObject:[dDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    addThreeObjs+=3;
    [_tableViewForUserAnswers reloadData];
    
}



- (IBAction)scrollDown:(id)sender
{
    if(_scrollPrp.tag==11)
    {
        [_scrollViewForFullView setContentOffset:CGPointMake(0, 204) animated:YES];
        [_scrollPrp setImage:[UIImage imageNamed:@"upArrow.png"] forState:UIControlStateNormal];
        _scrollPrp.tag=12;
    } else if(_scrollPrp.tag==12)
    {
        [_scrollViewForFullView setContentOffset:CGPointMake(0, 0) animated:YES];
        [_scrollPrp setImage:[UIImage imageNamed:@"downArow.png"] forState:UIControlStateNormal];
        _scrollPrp.tag=11;
    }
}



-(void)progressswipeRight
{
    _view_flip.hidden=YES;
    _makeDecissionView.hidden=YES;
    _flipPrp.hidden=NO;
    
    
}
#pragma mark UserAnswers

//To view the user answers for particualr piczly
-(void)swipeLeft
{
    _becauseTxtf_View.hidden = NO;
    _view_flip.hidden=YES;
    if ([_currentUser isEqualToString:@"currentUser"])
    {
        _flipPrp.hidden=NO;
    }
    else
    {
        _flipPrp.hidden=YES;
    }
    _makeDecissionView.hidden=YES;
    
    
}
-(void)swipeRight
{
    
    _becauseTxtf_View.hidden = YES;
    _view_flip.hidden=NO;
    
    if ([_currentUser isEqualToString:@"currentUser"])
    {
        _flipPrp.hidden=YES;
    }
    else
    {
        _flipPrp.hidden=YES;
    }
    
    _makeDecissionView.hidden=NO;
}

#pragma mark Data Parsing

-(void)fetchingDataForTableViewAndQuardCurve
{
    
    [dataArray  removeAllObjects];
    [aDataArray removeAllObjects];
    [bDataArray removeAllObjects];
    [cDataArray removeAllObjects];
    [dDataArray removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    PFQuery *query=[PFQuery queryWithClassName:@"DetailView"];
    [query whereKey:@"piczly_Info" equalTo:self.PCZObj];
    [query whereKey:@"user_Info" notEqualTo:_PCZObj.createdBy];
    [query includeKey:@"user_Info"];
    [query includeKey:@"piczly_Info"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (objects.count>0)
         {
             
             [dataArray addObjectsFromArray:objects];
             total_countLbl.text=[NSString stringWithFormat:@"%ld",dataArray.count];
             for (int i=0; i<dataArray.count; i++)
             {
                 _PCZ_DetailObj=[dataArray objectAtIndex:i];
                 if ([_PCZ_DetailObj.decision doubleValue]==0)
                 {
                     
                     [aDataArray addObject:[dataArray objectAtIndex:i]];
                 }
                 else if ([_PCZ_DetailObj.decision doubleValue]==1)
                 {
                     [bDataArray addObject:[dataArray objectAtIndex:i]];
                 }
                 else if ([_PCZ_DetailObj.decision doubleValue]==2)
                 {
                     [cDataArray addObject:[dataArray objectAtIndex:i]];
                 }
                 else if ([_PCZ_DetailObj.decision doubleValue]==3)
                 {
                     [dDataArray addObject:[dataArray objectAtIndex:i]];
                 }
             }
             
             for (int i=0; i<3; i++)
             {
                 if (aDataArray.count>i)
                 {
                     [dataArrayForTableView addObject:[aDataArray objectAtIndex:i]];
                     [_btnAPrp setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"] forState:UIControlStateNormal];
                     [_btnAPrp setTintColor:[UIColor whiteColor]];
                 }
             }
             
             [_tableViewForUserAnswers reloadData];
             
             float a,b,c,d,all,f_value;
             
             a=(float)aDataArray.count ;
             b=(float)bDataArray.count;
             c=(float)cDataArray.count;
             d=(float)dDataArray.count;
             all=(float)dataArray.count;
             [dictForSorting setValue:[NSNumber numberWithInt:(int)aDataArray.count] forKey:@"A"];
             [dictForSorting setValue:[NSNumber numberWithInt:(int)bDataArray.count] forKey:@"B"];
             [dictForSorting setValue:[NSNumber numberWithInt:(int)cDataArray.count] forKey:@"C"];
             [dictForSorting setValue:[NSNumber numberWithInt:(int)dDataArray.count] forKey:@"D"];
             
             NSArray *sortedArray = [dictForSorting keysSortedByValueUsingSelector:@selector(compare:)];
             
             for (int i=0; i<=3; i++)
             {
                 NSString *str=[sortedArray objectAtIndex:i];
                 if (i==0)
                 {
                     f_value = [[dictForSorting valueForKey:str] floatValue];
                     
                     self.circleProgressViewD.elapsedTime=(f_value/all)*100;
                     
                     dTitleLbl.text=str;
                     
                     float a=(f_value/all)*100;
                     NSString *str1=[NSString stringWithFormat:@"%d",(int)a];
                     _lblDPercentage.text=[NSString stringWithFormat:@"%@%%", str1];
                 }
                 else if (i==1)
                 {
                     f_value = [[dictForSorting objectForKey:str] floatValue];
                     
                     self.circleProgressViewC.elapsedTime=(f_value/all)*100;
                     cTitleLbl.text=str;//[dictForSorting objectForKey:str];
                     float a=(f_value/all)*100;
                     NSString *str1=[NSString stringWithFormat:@"%d",(int)a];
                     _lblCPercentage.text=[NSString stringWithFormat:@"%@%%", str1];;
                 }
                 else if (i==2)
                 {
                     f_value = [[dictForSorting objectForKey:str] floatValue];
                     
                     self.circleProgressViewB.elapsedTime=(f_value/all)*100;
                     bTitleLbl.text=str;
                     float a=(f_value/all)*100;
                     
                     NSString *str1=[NSString stringWithFormat:@"%d",(int)a];
                     _lblBPercentage.text=[NSString stringWithFormat:@"%@%%", str1];;
                 }
                 else if (i==3)
                 {
                     f_value = [[dictForSorting objectForKey:str] floatValue];
                     
                     self.circleProgressViewA.elapsedTime=(f_value/all)*100;
                     aTitleLbl.text=str;
                     float a=(f_value/all)*100;
                     
                     NSString *str1=[NSString stringWithFormat:@"%d",(int)a];
                     _lblAPercentage.text=[NSString stringWithFormat:@"%@%%", str1];;
                 }
             }
             
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }];
    
    
    
}

#pragma mark - UITextfield Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField ==self.txtf_because)
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x,-180,
                                     self.view.frame.size.width, self.view.frame.size.height);
    }
    else
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
        
        self.chatPopUp_View.frame = CGRectMake(self.chatPopUp_View.frame.origin.x,320, self.chatPopUp_View.frame.size.width, self.chatPopUp_View.frame.size.height);
        self.chatPopUp_View.frame = CGRectMake(self.chatPopUp_View.frame.origin.x,320, self.chatPopUp_View.frame.size.width, self.chatPopUp_View.frame.size.height);
        ViewFrame=self.chat_View.frame;
        self.chat_View.frame = CGRectMake(self.chat_View.frame.origin.x,100, self.chat_View.frame.size.width, 185);
        self.chatView_chatBtn=[[UIButton alloc]initWithFrame:CGRectMake(225, 133, 40, 30)];
        self.becauseTxtf_img= [[UIImageView alloc]initWithFrame:CGRectMake(10, 130, 210, 45)];
        self.becauseTxtf_img.image = [UIImage imageNamed:@"Detail_View_BecauseTxt"];
        [self.chat_View addSubview:self.becauseTxtf_img];
        self.becauseTxtf_ChatView =[[UITextField alloc]initWithFrame:CGRectMake(25, 133, 95, 30)];
        self.becauseTxtf_ChatView.placeholder=@"Because...";
        [self.chat_View addSubview:self.becauseTxtf_ChatView];
        
        [self.chatView_chatBtn setBackgroundImage:[UIImage imageNamed:@"msgsending_img"] forState:UIControlStateNormal];
        
        [self.chat_View addSubview:self.chatView_chatBtn];
        
    }
    [UIView commitAnimations];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (textField ==self.txtf_because)
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,+65,
                                      self.view.frame.size.width, self.view.frame.size.height);
    }else
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
        self.chat_View.frame = CGRectMake(self.chat_View.frame.origin.x, 105, self.chat_View.frame.size.width, 428);
    }
    
    [UIView commitAnimations];
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.submitBtn.hidden=YES;
    [self.sendBtn setImage:[UIImage imageNamed:@"msgsending_img"] forState: UIControlStateNormal];
    self.chat_Btn.hidden = YES;
    self.sendBtn.hidden = NO;
    resizeView.hidden = NO;
    
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    [self.chat_Btn setImage:[UIImage imageNamed:@"chat_img"] forState:UIControlStateNormal];
    self.chat_Btn.hidden = NO;
    self.sendBtn.hidden = YES;
    self.becauseTxtf_img.hidden = YES;
    self.chatView_chatBtn.hidden = YES;
    self.becauseTxtf_ChatView.hidden = YES;
    
    
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
//for selecting the  ABCD options for piczly question. 

- (IBAction)BtnActionForABCD:(UIButton *)sender
{
    [dataArrayForTableView removeAllObjects];
    
    addThreeObjs=3;
    _loadMorePrp.alpha=1.0;
    _loadMorePrp.userInteractionEnabled=YES;
    
    if (sender.tag==0)
    {
        btnIndexValue=0;
        [_btnAPrp setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnAPrp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnBPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnBPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        
        [_btnCPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnCPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnDPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnDPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        for (int i=0; i<3; i++)
        {
            if (aDataArray.count>i)
            {
                [dataArrayForTableView addObject:[aDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
        
       
        
    }
    else if (sender.tag==1)
    {
        btnIndexValue=1;
        [_btnBPrp setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnBPrp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnAPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnAPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnCPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnCPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnDPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnDPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        for (int i=0; i<3; i++)
        {
            
            if (bDataArray.count>i)
            {
                [dataArrayForTableView addObject:[bDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
            
        }
    }
    else if (sender.tag==2)
    {
        btnIndexValue=2;
        [_btnCPrp setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnCPrp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnBPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnBPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnAPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnAPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnDPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnDPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        for (int i=0; i<3; i++)
        {
            
            if (cDataArray.count>i)
            {
                [dataArrayForTableView addObject:[cDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
            
        }
    }
    else
    {
        btnIndexValue=3;
        [_btnDPrp setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnDPrp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnBPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnBPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnCPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnCPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        [_btnAPrp setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                            forState:UIControlStateNormal];
        [_btnAPrp setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100] forState:UIControlStateNormal];
        
        for (int i=0; i<3; i++)
        {
            if (dDataArray.count>i)
            {
                [dataArrayForTableView addObject:[dDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    
    [_tableViewForUserAnswers reloadData];
}
- (IBAction)buttonActionForViewCheckout:(id)sender
{
    
    UIButton *btn=sender;
    if (btn.tag==0)
    {
        [_aBtnViewPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                forState:UIControlStateNormal];
        [_bBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_cBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_dBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:0];
    }
    else if (btn.tag==1)
    {
        [_bBtnViewPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                forState:UIControlStateNormal];
        [_aBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_cBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_dBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:1];
    }
    else if (btn.tag==2)
    {
        [_cBtnViewPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                forState:UIControlStateNormal];
        [_bBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_aBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_dBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:2];
    }
    else
    {
        [_dBtnViewPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                forState:UIControlStateNormal];
        [_bBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        [_cBtnViewPrp   setBackgroundImage:[UIImage imageNamed:@""]
                                  forState:UIControlStateNormal];
        [_aBtnViewPrp setBackgroundImage:[UIImage imageNamed:@""]
                                forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:3];
    }
    
    self.submitBtn.hidden =NO;
    
    
}
- (IBAction)btnActionForABCDCheckMark:(id)sender
{
    
    UIButton *btn=sender;
    if (btn.tag==0)
    {
        [_aBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                            forState:UIControlStateNormal];
        [_bBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_cBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_dBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:0];
    }
    else if (btn.tag==1)
    {
        [_bBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                            forState:UIControlStateNormal];
        [_aBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_cBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_dBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:1];
    }
    else if (btn.tag==2)
    {
        [_cBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                            forState:UIControlStateNormal];
        [_bBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_aBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_dBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:2];
    }
    else
    {
        [_dBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                            forState:UIControlStateNormal];
        [_bBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_cBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        [_aBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                            forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:3];
    }
}


- (void)sendBtnClicked
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (_PCZ_DetailObjByCurrentUser)
    {
        _PCZ_DetailObjByCurrentUser.userAnswer=_txtf_because.text;
        if (detailViewObj[@"decision"])
        {
            _PCZ_DetailObjByCurrentUser.decision=detailViewObj[@"decision"];
        }
        [_PCZ_DetailObjByCurrentUser saveEventually];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"sucessfully updated" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
        [alert show];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else
    {
        
        detailViewObj[@"userAnswer"]=_txtf_because.text;
        detailViewObj[@"user_Info"]=[PiczlyUser currentUser];
        detailViewObj[@"piczly_Info"]=self.PCZObj;
        detailViewObj[@"answer_Type"]=self.PCZObj.answerType;
        [detailViewObj saveEventually:^(BOOL succeeded, NSError *error)
         {
             if(!error)
             {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"piczl-sucessfully sent" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                 [alert show];
             }
             else
             {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"Data not saved please try after sometime" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                 [alert show];
             }
         }];
    }
}
- (IBAction)sendBtnClicked:(UIButton *)sender
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if([_txtf_because.text isEqualToString:@""] || !detailViewObj[@"decision"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"please enter all the fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
    
    else if(![_txtf_because.text isEqualToString:@""])
    {
        
        if (_PCZ_DetailObj)
        {
            _PCZ_DetailObj.userAnswer=_txtf_because.text;
            if (detailViewObj[@"decision"])
            {
                _PCZ_DetailObj.decision=detailViewObj[@"decision"];
            }
            [_PCZ_DetailObj saveEventually];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"sucessfully updated" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
            [alert show];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else
        {
            detailViewObj[@"userAnswer"]=_txtf_because.text;
            detailViewObj[@"user_Info"]=[PFUser currentUser];
            detailViewObj[@"piczly_Info"]=self.PCZObj;
            detailViewObj[@"answer_Type"]=self.PCZObj.answerType;
            [detailViewObj saveEventually:^(BOOL succeeded, NSError *error)
             {
                 if(!error)
                 {
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"sucessfully saved" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                     [alert show];
                 }
                 else
                 {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"Data not saved please try after sometime" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                     [alert show];
                 }
             }];
        }
    }
    
    
}

- (IBAction)cancelBtn_Clicked:(id)sender
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.chatPopUp_View.hidden = YES;
}


- (IBAction)submiBtnClicked:(id)sender
{
    [self sendBtnClicked];
}

#pragma mark-Navigating To ChatViewController

- (IBAction)chatBtn_Clicked:(id)sender
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    
    vc.piczly=self.PCZObj;
    vc.piczlyUser=(PiczlyUser*)[PFUser currentUser];
    vc.chatcount=chatcount;
    self.navigationController. modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:NO completion:nil];
    vc.view.backgroundColor = [UIColor clearColor];
    
}


#pragma mark
#pragma mark Parsing For ChatMessage Count

-(void)numofChatMessages
{
    PFQuery *query = [PFQuery queryWithClassName:@"ChatForParticularPiczly"];
    
    [query orderByAscending:@"createdAt"];
    [query whereKey:@"piczlyForChat" equalTo:_PCZObj];
    [query includeKey:@"piczlyUser"];
    [query includeKey:@"piczlyForChat"];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error)
     {
         if (!error)
         {
             
             chatcount=number;
             [self.chat_Btn setTitle:[NSString stringWithFormat:@"%d",number] forState:UIControlStateNormal];
             [self.chat_Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         }
         
     }];
}
@end
