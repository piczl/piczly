//
//  MyFeedTableViewCell.m
//  Piczly
//
//  Created by sekhar on 30/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "MyFeedTableViewCell.h"
#import "CreatePiczlyViewController.h"

@implementation MyFeedTableViewCell

- (void)awakeFromNib {
    self.dropDownBtnView.hidden = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)shareBtnAction:(id)sender {
    UIButton *btn=(UIButton*)sender;
    btn.selected =!btn.selected;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapOnShareBtn:btnIndex:)]){
        [_MyFeedDelegate cellDidTapOnShareBtn:self btnIndex:(int)btn.tag];
    }
}
- (IBAction)optionBtnAction:(id)sender {
    
    
    UIButton *btn=(UIButton*)sender;
    btn.selected =!btn.selected;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapOnDropDownBtn:btnIndex:)]){
        [_MyFeedDelegate cellDidTapOnDropDownBtn:self btnIndex:(int)btn.tag];
    }
}
- (IBAction)editPiczlBtnAction:(id)sender
{
    //CreatePiczlyViewController *createPiczlVC = [self.sto]
    UIButton *btn=(UIButton *)sender;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapOptionFeedBtn:btnIndex:)]){
        [_MyFeedDelegate cellDidTapOptionFeedBtn:self btnIndex:(int)btn.tag];
        
    }
    
    
}

- (IBAction)inviteMorePeopleBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapInviteMorePeopleBtn:btnIndex:)]){
        [_MyFeedDelegate cellDidTapInviteMorePeopleBtn:self btnIndex:(int)btn.tag];
        
    }
}

- (IBAction)seeAllParticipantsBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapOnSeeAllParticipants:btnIndex:)]){
        [_MyFeedDelegate cellDidTapOnSeeAllParticipants:self btnIndex:(int)btn.tag];
        
    }
}

- (IBAction)createNewPiczlBasedOnItBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapCreateNewPiczlBasedOnIt:btnIndex:)]){
        [_MyFeedDelegate cellDidTapCreateNewPiczlBasedOnIt:self btnIndex:(int)btn.tag];
    }
}

- (IBAction)CreatePiczlyWithSameTopicBtnAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapCreateOnThatTopic:btnIndex:)]){
        [_MyFeedDelegate cellDidTapCreateOnThatTopic:self btnIndex:(int)btn.tag];
        
    }
    
}

- (IBAction)deletePiczlyBtnAction:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    if(_MyFeedDelegate && [_MyFeedDelegate respondsToSelector:@selector(cellDidTapOnDeleteBtn:btnIndex:)]){
        [_MyFeedDelegate cellDidTapOnDeleteBtn:self btnIndex:(int)btn.tag];
    }
    self.dropDownBtnView.hidden=YES;
}


@end
