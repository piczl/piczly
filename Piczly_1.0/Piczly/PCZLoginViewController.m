
//
//  ViewController.m
//  Piczly
//
//  Created by sekhar on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "PCZLoginViewController.h"
#import "PiczlyUser.h"
#import "PCZHomeViewController.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
@interface PCZLoginViewController ()
{
    NSDictionary *userData;
}

@end
UIActivityIndicatorView *activityIndicator;
@implementation PCZLoginViewController

NSString *facebookID;
NSString *name;
NSString *location;
NSString *gender;
NSString *birthday;
NSString *relationship;
NSURL    *pictureURL;
NSString *email;
NSString *firstname;
NSString *lastName;

#pragma mark
#pragma mark View LifeCycle

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]])
    {
        [self presentUserDetailsViewControllerAnimated];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark
#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark FBSignUp

- (IBAction)signUpWithFBAction:(UIButton *)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email",@"user_friends"];
    
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if (!user) {
            NSString *errorMessage = nil;
            if (!error) {
                
                errorMessage = @"Uh oh. The user cancelled the Facebook login.";
            } else {
                
                errorMessage = [error localizedDescription];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Dismiss", nil];
            [alert show];
        }
        else
        {
            if (user.isNew)
            {
                [self loadData];
            }
            else
            {
                [self updateUserProfile];
                [self presentUserDetailsViewControllerAnimated];
                
                
            }
        }
    }];
    
    [activityIndicator startAnimating];
    
}

-(void)presentUserDetailsViewControllerAnimated
{
    
    
    
    SWRevealViewController *SWRController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRController"];
    
    
    self.PCZHomeVC.profileName.text=name;
    self.PCZHomeVC.userData = userData;
    
    
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
    
    // Run network request asynchronously
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError == nil && data != nil)
         {
             
             self.PCZHomeVC.profilePic.image = [UIImage imageWithData:data];
         }
     }];
    
    
    [self.navigationController pushViewController:SWRController animated:YES];
    
    self.navigationController.navigationBarHidden = YES;
    
}

#pragma mark
#pragma mark Update User Data

-(void)updateUserProfile
{
    FBRequest *reqToUpdate=[FBRequest requestForMe];
    [reqToUpdate startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if(!error)
        {
            userData=(NSDictionary*)result;
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", userData[@"id"]]];
            
            PFFile *userImage = [PFFile fileWithData:[NSData dataWithContentsOfURL:pictureURL]];
            [PiczlyUser currentUser].userImageFile =userImage;
            [[PiczlyUser currentUser] saveInBackground];
            
        };
        
    }];
}

- (void)loadData
{
    // ...
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         
         if (!error)
         {
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             userData = (NSDictionary *)result;
             __block PiczlyUser *user=[PiczlyUser currentUser];
             PFQuery *query=[PiczlyUser query];
             [query whereKey:@"email" equalTo:userData[@"email"]];
             [query countObjectsInBackgroundWithBlock:^(int number, NSError *error)
              {
                  if(number>0)
                  {
                      [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                      [self presentUserDetailsViewControllerAnimated];
                      Com_AlertView(@"info",@" It looks like you already have an account");
                      
                  }
                  else
                  {
                      user[@"facebookId"]  = userData[@"id"];
                      user[@"displayName"] = userData[@"first_name"];
                      user[@"firstName"]   = userData[@"first_name"];
                      user[@"lastName"]    = userData[@"last_name"];
                      user[@"username"]    = userData[@"name"];
                      facebookID           = userData[@"id"];
                      name                 = userData[@"name"];
                      user[@"email"]       = userData[@"email"];
                      NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", userData[@"id"]]];
                      PFFile *userImage = [PFFile fileWithData:[NSData dataWithContentsOfURL:pictureURL]];
                      [PiczlyUser currentUser].userImageFile = userImage;
                      [[PiczlyUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
                       {
                           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                           if (!error)
                           {
                               PCZHomeViewController *piczlyLoginVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                               piczlyLoginVC.profileName.text=user[@"username"];
                               [self.navigationController pushViewController:piczlyLoginVC animated:YES];
                               
                           }
                           else
                           {
                               Com_AlertView(@"Error", error.localizedDescription);
                           }
                           
                       }];
                  }
              }];
         }
         else
         {
             Com_AlertView(@"error", error.localizedDescription);
         }
     }];
}

#pragma mark
#pragma mark FBSession

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state)
    {
        case FBSessionStateOpen:
            if (!error)
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *FBuser, NSError *error)
                 {
                     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     if (!error)
                     {
                         PFQuery *querys = [PiczlyUser query];
                         [querys whereKey:@"email" equalTo:[FBuser objectForKey:@"email"]];
                         
                         NSArray *existingUserAry = [querys findObjects];
                         
                         if ([existingUserAry count]>0)
                         {
                             
                             [self presentUserDetailsViewControllerAnimated];
                             
                         }
                         
                         else
                         {
                             
                             PiczlyUser *user = [PiczlyUser object];
                             user.username = [FBuser objectForKey:@"email"];
                             user.password = [FBuser objectForKey:@"email"];
                             user.email  = [FBuser objectForKey:@"email"];
                             user[@"facebookId"]  = FBuser[@"id"];
                             user.displayName = FBuser[@"name"];
                             user.location = FBuser[@"location"][@"name"];
                             [[PiczlyUser currentUser] saveEventually];
                             BOOL status = [user signUp];
                             if(status)
                             {
                                 
                                 [self presentUserDetailsViewControllerAnimated];
                                 
                             }
                             
                         }
                         
                     }
                 }];
                
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
}

-(void)request:(FBRequest *)request didLoad:(id)result
{
    
}

@end
