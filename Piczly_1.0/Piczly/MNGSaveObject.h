/**********************************************************************************************
 Author      : Umesh Naidu.C
 Description : MNGSaveObject class
 Date Created: 16/01/13.
 Copyright (c) 2013 Mangohealthcare. All rights reserved.
 ***********************************************************************************************/

#import <Foundation/Foundation.h>

@interface MNGSaveObject : NSObject
{
}
+(void) saveToUserDefaults :(NSString*)key :(NSString *)value ;
+(NSString *) retrieveFromUserDefaults :(NSString *)key ;
+(void)saveArrayToUserDefaults:(NSString*)key :(NSMutableArray *)value;
+(NSMutableArray *)retrieveArrayFromUserDefaults : (NSString *) key;
+(void)saveDictionaryToUserDefaults:(NSString *)key :(NSMutableDictionary *)value; 
+(NSMutableDictionary *)retrieveDictFromUserDefaults : (NSString *) key;
@end
