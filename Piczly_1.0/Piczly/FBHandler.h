//
//  FBHandler.h
//  Breakk
//
//  Created by Indicode on 10/29/14.
//  Copyright (c) 2014 Umesh Naidu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@protocol FBDelegate

@required

-(void) OnFBSuccess;
-(void) OnFBFailed : (NSError *)error;

@end

@interface FBHandler : NSObject{
    NSInvocation *_callback;
}

@property (nonatomic, weak) id fbDelegate;

-(void)inviteFriends;

@end
