//
//  PCZ_Piczly.m
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "PCZ_Piczly.h"

@implementation PCZ_Piczly
@dynamic topic;
@dynamic image;
@dynamic latitude;
@dynamic longitude;
@dynamic question;
@dynamic answerType;
@dynamic yes_no_options;
@dynamic a_b_c_d_options;
@dynamic optionA;
@dynamic optionB;
@dynamic optionC;
@dynamic optionD;
@dynamic createdDate;
@dynamic createdBy;
@dynamic invitedPeopleCount;
@dynamic topicCount;
@dynamic piczlyStatus;

+(NSString*)parseClassName
{
    return @"Piczly";
}
@end
