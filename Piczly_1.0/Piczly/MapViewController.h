//
//  MapViewController.h
//  Piczly
//
//  Created by sekhar on 16/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<MKMapViewDelegate>
@property  float latitude;
@property  float longitude;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
