//
//  CircleShapeLayer.m
//  CircularProgressControl
//
//  Created by Carlos Eduardo Arantes Ferreira on 22/11/14.
//  Copyright (c) 2014 Mobistart. All rights reserved.
//

#import "CircleShapeLayer.h"

@interface CircleShapeLayer ()

@property (assign, nonatomic) double initialProgress;

@property (nonatomic, strong) CAShapeLayer *progressLayer;

@property (nonatomic, strong) CAShapeLayer *progressLayer1;

@property (nonatomic, assign) CGRect frame;

@end

@implementation CircleShapeLayer

@synthesize percent = _percent;
@synthesize colorForeground,colorBackground;

- (instancetype)init {
    if ((self = [super init]))
    {
      //  [self setupLayer];
     //   [self setupLayer1];
    }
    
    return self;
}


- (void)layoutSublayers {

    self.path = [self drawPathWithArcCenter];
    self.progressLayer.path = [self drawPathWithArcCenter];
    
//    self.path = [UIBezierPath bezierPathWithRoundedRect:[self drawPathWithArcCenter]
//                                      byRoundingCorners:UIRectCornerAllCorners
//                                            cornerRadii:8.0];
    // self.progressLayer.strokeColor = UIRectCornerAllCorners;
    [super layoutSublayers];
}

- (void)setupLayer {
    
    self.path = [self drawPathWithArcCenter];
    self.fillColor = [UIColor clearColor].CGColor;
   // self.strokeColor = [UIColor colorWithRed:0.86f green:0.86f blue:0.86f alpha:0.4f].CGColor;
    self.strokeColor = [UIColor brownColor].CGColor;
   // self.strokeColor =(__bridge CGColorRef)([UIColor grayColor]);
    self.lineWidth = 20;
    self.progressLayer = [CAShapeLayer layer];
    self.progressLayer.path = [self drawPathWithArcCenter];
    self.progressLayer.fillColor = [UIColor clearColor].CGColor;
   // self.progressLayer.strokeColor = [UIColor greenColor].CGColor;
    self.progressLayer.lineWidth = 18;
    self.progressLayer.lineCap = kCALineJoinBevel;
    self.progressLayer.lineJoin = kCALineJoinBevel;
    

    
    
    //This is New Lines
    [self addSublayer:self.progressLayer];
}
- (void)setupLayer1:(UIColor *)color
{
    
    self.path = [self drawPathWithArcCenter];
    self.fillColor = [UIColor clearColor].CGColor;
    // self.strokeColor = [UIColor colorWithRed:0.86f green:0.86f blue:0.86f alpha:0.4f].CGColor;
    self.strokeColor = color.CGColor;
    // self.strokeColor =(__bridge CGColorRef)([UIColor grayColor]);
    self.lineWidth = 20;
    self.progressLayer = [CAShapeLayer layer];
    self.progressLayer.path = [self drawPathWithArcCenter];
    self.progressLayer.fillColor = [UIColor clearColor].CGColor;
    // self.progressLayer.strokeColor = [UIColor greenColor].CGColor;
    self.progressLayer.lineWidth = 13;
    self.progressLayer.lineCap = kCALineJoinBevel;
    self.progressLayer.lineJoin = kCALineJoinBevel;
    
    
    
    
    //This is New Lines
    [self addSublayer:self.progressLayer];
}

- (void)setupLayer1 {
    
    self.path = [self drawPathWithArcCenter];
    self.fillColor = [UIColor clearColor].CGColor;
    // self.strokeColor = [UIColor colorWithRed:0.86f green:0.86f blue:0.86f alpha:0.4f].CGColor;
    self.strokeColor = [UIColor blueColor].CGColor;
    // self.strokeColor =(__bridge CGColorRef)([UIColor grayColor]);
    self.lineWidth = 20;
    self.progressLayer1 = [CAShapeLayer layer];
    self.progressLayer1.path = [self drawPathWithArcCenter];
    self.progressLayer1.fillColor = [UIColor clearColor].CGColor;
    // self.progressLayer.strokeColor = [UIColor greenColor].CGColor;
    self.progressLayer1.lineWidth = 15;
    self.progressLayer1.lineCap = kCALineJoinBevel;
    self.progressLayer1.lineJoin = kCALineJoinBevel;
    
  
    //This is New Lines
    [self addSublayer:self.progressLayer1];
}


- (CGPathRef)drawPathWithArcCenter {
    
//    CGFloat position_y = self.frame.size.height/2;
//    CGFloat position_x = self.frame.size.width/2;
    CGFloat position_y = self.frame.size.height/2;
    CGFloat position_x = self.frame.size.width/2;
    
  
// Working Good
//    return [UIBezierPath bezierPathWithArcCenter:CGPointMake(position_x, position_y)
//                                          radius:position_y
//                                      startAngle:-400
//                                        endAngle:-910
//                                       clockwise:YES].CGPath;
// Working Good
        return [UIBezierPath bezierPathWithArcCenter:CGPointMake(position_x, position_y)
                                              radius:position_y
                                          startAngle:-500
                                            endAngle:-810
                                           clockwise:YES].CGPath;
 
}


- (void)setElapsedTime:(NSTimeInterval)elapsedTime {
    _initialProgress = [self calculatePercent:_elapsedTime toTime:_timeLimit];
    _elapsedTime = elapsedTime;
    
    self.progressLayer.strokeEnd = self.percent;
    [self startAnimation];
}

- (double)percent {
    
    _percent = [self calculatePercent:_elapsedTime toTime:_timeLimit];
    return _percent;
}

- (void)setProgressColor:(UIColor *)progressColor {
    self.progressLayer.strokeColor = progressColor.CGColor;
}

- (double)calculatePercent:(NSTimeInterval)fromTime toTime:(NSTimeInterval)toTime {
    
    if ((toTime > 0) && (fromTime > 0)) {
        
        CGFloat progress = 0;
        
        progress = fromTime / toTime;
        
        if ((progress * 100) > 100) {
            progress = 1.0f;
        }
        
       
        
        return progress;
    }
    else
        return 0.0f;
}

- (void)startAnimation {
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 1.0;
    pathAnimation.fromValue = @(self.initialProgress);
    pathAnimation.toValue = @(self.percent);
    pathAnimation.removedOnCompletion = YES;
    
    [self.progressLayer addAnimation:pathAnimation forKey:nil];
    
}

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
