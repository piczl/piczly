//
//  SeeAllPiczysFromUserNameViewController.m
//  Piczly
//
//  Created by sekhar on 22/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "SeeAllPiczysFromUserNameViewController.h"


@interface SeeAllPiczysFromUserNameViewController ()
{
    NSMutableArray *otherUsersCreatedPiczlys;
}

@end

@implementation SeeAllPiczysFromUserNameViewController

#pragma mark View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    otherUsersCreatedPiczlys=[[NSMutableArray alloc]init];
    [self showData];
    self.title=@"AllPiczlyFromUser";
    self.navigationItem.leftBarButtonItem=nil;
    // Do any additional setup after loading the view.
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark retriving data from parse database and storing in array

-(void)showData
{
    PFQuery *query=[PFQuery queryWithClassName:@"Piczly"];
    [query whereKey:@"createdBy" equalTo:self.userForCreatedPiczly];
    [query whereKey:@"piczlyStatus" notEqualTo:[NSNumber numberWithInt:0]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects.count>0)
        {
            [otherUsersCreatedPiczlys addObjectsFromArray:objects];
            [_piczsTableView reloadData];
            
        }
    }];
}

#pragma mark
#pragma mark Tableview Datasource methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [otherUsersCreatedPiczlys count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 360;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"myfeedTVC";
    MyFeedTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil)
    {
        cell=[[MyFeedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.userProfileImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.userProfileImage.layer.cornerRadius=cell.userProfileImage.frame.size.height/2;
    cell.userProfileImage.layer.borderWidth=6.0;
    cell.userProfileImage.layer.masksToBounds = YES;
    cell.userProfileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    _pCZObjForTableView=[otherUsersCreatedPiczlys objectAtIndex:indexPath.row];
    
    
    PFFile *fileForProfileImage=self.userForCreatedPiczly.userImageFile;
    cell.userProfileName.text=self.userForCreatedPiczly.username;
    
    [fileForProfileImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
     {
         if (data)
         {
             MyFeedTableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
             if (updateCell)
             {
                 updateCell.userProfileImage.image=[UIImage imageWithData:data];
             }
         }
         
     }];
    cell.itemQuestion.text=_pCZObjForTableView.question;
    cell.itemTopicName.text=_pCZObjForTableView.topic;
    PFFile *fileForImage= _pCZObjForTableView.image;
    [fileForImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        cell.itemImage.layer.masksToBounds=YES;
        cell.itemImage.layer.cornerRadius=10.0;
        cell.itemImage.image=[UIImage imageWithData:data];
    }];
    
    int number=[_pCZObjForTableView.answerType doubleValue];
    if (number ==[[NSNumber numberWithInt:0] doubleValue])
    {
        cell.itemChoice.text=@"Yes-No";
    }
    else
    {
        cell.itemChoice.text=@"A-B-C-D";
    }
    
    PFQuery *queryForObj=[PFQuery queryWithClassName:@"Piczly"];
    [queryForObj whereKey:@"topic" equalTo:_pCZObjForTableView.topic];
    [queryForObj findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        cell.noOfTopicWithThatPiczl.text= [NSString stringWithFormat:@"%ld",objects.count];
    }];
    
    return cell;
}

@end
