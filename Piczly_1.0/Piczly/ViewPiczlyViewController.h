//
//  ViewPiczlyViewController.h
//  Piczly
//
//  Created by sekhar on 25/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPCZCustomTableViewCell.h"
#import <Parse/Parse.h>
#import <MBProgressHUD.h>
#import "PCZ_Piczly.h"

@interface ViewPiczlyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray   *arrayForCreatedPiczlyes;
    PCZ_Piczly       *piczlyObj;
}
@property (weak, nonatomic) IBOutlet UITableView *ViewPCZTableView;

@end
