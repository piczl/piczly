//
//  SeeAllParticipentsViewController.m
//  Piczly
//
//  Created by BHANU on 17/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "SeeAllParticipentsViewController.h"

@interface SeeAllParticipentsViewController ()

@end

@implementation SeeAllParticipentsViewController

#pragma mark ViewLifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setTitle:@"Participants Controller"];
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Tableview DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _participantsArry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text=[[_participantsArry valueForKey:@"displayName"] objectAtIndex:indexPath.row] ;
    PFFile *fileForProfileImage=[[_participantsArry valueForKey:@"userImageFile"] objectAtIndex:indexPath.row];
    [ fileForProfileImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        cell.imageView.image=[UIImage imageWithData:data];
    }];
    
    return cell;
}

@end
