//
//  ImageViewCell.h
//  Piczly
//
//  Created by Hanuman Saini on 27/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *leftBluredImage;
@property (strong, nonatomic) IBOutlet UIImageView *rightBluredImage;
@property (strong, nonatomic) IBOutlet UILabel *profileName;

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@end
