//
//  PCZ_Chat.h
//  Piczly
//
//  Created by sekhar on 27/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@class PiczlyUser;
@class PCZ_Piczly;

@interface PCZ_Chat : PFObject<PFSubclassing>

@property (nonatomic , strong) NSString   *Text;
@property (nonatomic , strong) NSString   *time;
@property (nonatomic , strong) PiczlyUser *piczlyUser;
@property (nonatomic , strong) PCZ_Piczly *piczlyForChat;

+(NSString*)parseClassName;

@end