//
//  SeeAllPiczysFromUserNameViewController.h
//  Piczly
//
//  Created by sekhar on 22/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PiczlyUser.h"
#import "PCZ_Piczly.h"
#import "MyFeedTableViewCell.h"
#import "AppDelegate.h"

@interface SeeAllPiczysFromUserNameViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *piczsTableView;
@property (nonatomic, strong) PCZ_Piczly           *PCZObj,*pCZObjForTableView;
@property (nonatomic, strong) PiczlyUser           *userForCreatedPiczly;

@end
