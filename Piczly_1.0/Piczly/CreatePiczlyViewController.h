//
//  CreatePiczlyViewController.h
//
//
//  Created by BHANU on 19/02/15.
//
//

#import <UIKit/UIKit.h>
#import "TextFieldCornerRadius.h"
#import "DOPDropDownMenu.h"
#import "ImageViewCornerRadius.h"
#import <TPKeyboardAvoidingScrollView.h>
#import "optionsTableViewCell.h"
#import "Define.h"
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import "PCZ_topics.h"
#import "PCZ_Piczly.h"
#import "PCZ_InvitedFriends.h"
#import "PCZHomeViewController.h"
#import "InviteViewController.h"



@protocol CreatePCZDelegate;

@interface CreatePiczlyViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,optionsTableViewCellDelegate,UIImagePickerControllerDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UINavigationControllerDelegate>

{
    NSMutableArray *arrForOptions,*arrayForUpdatingObjects;
    NSMutableArray *arrForOptionsLabel;
    NSString       *newString;
    int            tfTagValueInt;
    NSMutableArray *topics;
    PCZ_topics     *pcz_topicsObj;
    UIToolbar      *toolBar;
}
@property (weak, nonatomic)   IBOutlet UIButton    *dropDownBtn;
@property (strong, nonatomic) IBOutlet UITextField *PCZQuestionTF;
@property (strong, nonatomic) IBOutlet UIButton    *sendToBtn;
@property (strong, nonatomic) IBOutlet UILabel     *PCZTopicLabel;
@property (strong, nonatomic) IBOutlet UILabel     *PCZAnswerLabel;
@property (weak, nonatomic)   IBOutlet MKMapView   *PCZMapview;
@property (weak, nonatomic)   IBOutlet UIButton    *PCZAnswerDropdownBtn;
@property (strong, nonatomic) IBOutlet UIImageView *PCZQuesImageView;
@property (strong, nonatomic) IBOutlet UIImageView *PCZAnswerImageView;
//For facebook friends displaying properties
@property (strong, nonatomic) IBOutlet UIView      *piczlyInviteView;
@property (strong, nonatomic) IBOutlet UITableView *piczlyInviteTableView;
//TableView TextField Values Dictionary
@property (weak, nonatomic)   IBOutlet UIImageView *PCZAddPeopleImageView;
@property (weak, nonatomic)   IBOutlet UITableView *dropDownTableView;
@property (weak, nonatomic)   IBOutlet UIButton    *abcdBtn;
@property (weak, nonatomic)   IBOutlet UIButton    *yesnoBtn;
@property (weak, nonatomic)   IBOutlet UIButton    *addOptionBtn;
@property (weak, nonatomic)   IBOutlet UITableView *optionsTableView;
@property (strong, nonatomic) IBOutlet UIButton    *cameraBtnPrp;
@property (weak, nonatomic)   IBOutlet UIButton    *exceedBtn;
@property (weak, nonatomic)   IBOutlet UIButton    *cancelPCZQuesBtn;
@property (weak, nonatomic)   IBOutlet UIButton    *confirmPCZQuesBtn;
@property (strong, nonatomic) IBOutlet UILabel     *peopleAdded;
@property (strong, nonatomic) PCZ_Piczly          *piczly;
@property (strong, nonatomic) NSString            *nameOfTheClass;
@property (strong, nonatomic) NSString            *createOnThtTopic,*createOnThtTopicRef;
@property (nonatomic, copy)   NSArray             *citys;
@property (nonatomic ,strong) NSString            *editPiczy;
@property (nonatomic, strong) NSString            *menuNavStr;
@property (strong, nonatomic) NSMutableArray      *topicNamesArray;
@property (nonatomic, strong) NSMutableDictionary *tableViewTFValues;

@property (weak, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (strong, nonatomic) IBOutlet ImageViewCornerRadius        *PCZImageView;
@property (nonatomic, assign) id       <CreatePCZDelegate>          delegate;




- (IBAction)postPiczlyBtnAction:(UIButton *)sender;
- (IBAction)answerCategoriesBtnAction:(UIButton *)sender;
- (IBAction)yesnoBtnAction:(UIButton *)sender;
- (IBAction)cancelPCZQuesTFAction:(UIButton *)sender;
- (IBAction)confirmPCZQuesTFAction:(UIButton *)sender;
- (IBAction)abcdBtnAction:(UIButton *)sender;
- (IBAction)sendToBtnAction:(UIButton *)sender;
- (IBAction)createNewPiczlyAction:(UIButton *)sender;
- (IBAction)cameraBtnAction:(UIButton *)sender;
- (IBAction)photoLibraryAction:(UIButton *)sender;
- (IBAction)gpsLocationAction:(UIButton *)sender;
- (IBAction)dropDownBtnAction:(UIButton *)sender;

@end

@protocol CreatePCZDelegate <NSObject>

-(NSDictionary*)getValuesOfTableViewTextfieds:(NSDictionary*)dic;


@end
