//
//  HomeTableViewCell.h
//  Piczly
//
//  Created by Admin on 11/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HomeTableViewCellDelegate;
@interface HomeTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel     *topicNameLbl;
@property (strong, nonatomic) IBOutlet UIImageView *piczlyImage;
@property (nonatomic, strong) id <HomeTableViewCellDelegate>HomeTableCell;
@property (strong, nonatomic) IBOutlet UIButton *shareBtnprp;
@property (strong, nonatomic) IBOutlet UILabel  *itemQuestionLbl;
@property (strong, nonatomic) IBOutlet UILabel  *iteamStatus;
@property (strong, nonatomic) IBOutlet UILabel  *tearOffTopicNameLbl;
@property (strong, nonatomic) IBOutlet UILabel  *createdAtDateLbl;
@property (strong, nonatomic) IBOutlet UIButton *dropDownPrp;
@property (weak, nonatomic)   IBOutlet UILabel  *statusLbl;
@property (strong, nonatomic) IBOutlet UILabel  *respondedPeopleToThatQuestion;
@property (strong, nonatomic) IBOutlet UIView   *viewForDropDown;
@property (strong, nonatomic) IBOutlet UIButton *createNewPiczlBtnPrp;
@property (strong, nonatomic) IBOutlet UIButton *seeOnMapBtnPrp;
@property (strong, nonatomic) IBOutlet UIButton *ReportPiczlBtnPrp;

- (IBAction)reportPiczlBtnAction:(id)sender;
- (IBAction)shareBtnAction:(id)sender;
- (IBAction)dropDownAction:(id)sender;
- (IBAction)seeOnMapBtnAction:(id)sender;
- (IBAction)createNewPiczlBtnAction:(id)sender;


@end
//prtocal delegate methods for drop down acctions

@protocol HomeTableViewCellDelegate <NSObject>

-(void)cellDidTapOnOptionsBtn:(HomeTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnCreatePiczlyBasedOnItBtn:(HomeTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnSeeOnMapBtn:(HomeTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnReportThePiczlyBtn:(HomeTableViewCell *)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnFBShareBtn:(HomeTableViewCell*)myCell btnIndex:(int)myBtn;
@end

