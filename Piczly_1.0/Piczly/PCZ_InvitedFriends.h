//
//  PCZ_InvitedFriends.h
//  Piczly
//
//  Created by BHANU on 15/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
@class PCZ_Piczly;
@class PiczlyUser;
@interface PCZ_InvitedFriends : PFObject<PFSubclassing>

@property (nonatomic, strong) PiczlyUser *invitedFriends;
@property (nonatomic, strong) PCZ_Piczly *piczlyObjectId;
@property (nonatomic, strong) NSNumber   *AcceptanceStatus;

+(NSString*)parseClassName;
@end
