//
//  ImageViewCornerRadius.h
//  Piczly
//
//  Created by BHANU on 13/02/15.
//  Copyright (c) 2015 Betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ImageViewCornerRadius : UIImageView
@property (nonatomic, assign) int style;
@end
