//
//  ViewPiczlyViewController.m
//  Piczly
//
//  Created by sekhar on 25/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "ViewPiczlyViewController.h"
#import "Detail_ViewPCZViewController.h"
#import "Detail_ViewPCZ_ABCDViewController.h"

@interface ViewPiczlyViewController ()

@end

@implementation ViewPiczlyViewController

#pragma mark View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:53.0/255.0 blue:101.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelButtonitem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction:)];
    
    self.navigationItem.leftBarButtonItem = cancelButtonitem;
    
    arrayForCreatedPiczlyes=[[NSMutableArray alloc]init];
    [self fetchingPiczlyData];
}

#pragma mark Pop To Previous Controller

-(void)cancelButtonAction:(UIBarButtonItem*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Parsing Piczly Data

-(void)fetchingPiczlyData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *queryForPiczly=[PFQuery queryWithClassName:@"Piczly"];
    [queryForPiczly orderByDescending:@"updatedAt"];
    [queryForPiczly findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (objects.count>0)
         {
             [arrayForCreatedPiczlyes addObjectsFromArray:objects];
             [_ViewPCZTableView reloadData];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             UIAlertController *projectSelector = [UIAlertController alertControllerWithTitle:@"" message:@"No Piczly to display" preferredStyle:UIAlertControllerStyleAlert];
             [projectSelector addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 [self dismissViewControllerAnimated:YES completion:nil];
             }]];
             
             [self presentViewController:projectSelector animated:YES completion:nil];
         }
     }];
}

#pragma mark- Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark- Tableview Delegate and Datasource methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 165;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayForCreatedPiczlyes.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ViewPCZCustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.topicLabel.text=[[arrayForCreatedPiczlyes objectAtIndex:indexPath.row] valueForKey:@"topic"];
    cell.PCZQuesLabel.text=[[arrayForCreatedPiczlyes objectAtIndex:indexPath.row] valueForKey:@"question"];
    cell.PCZDateLabel.text=[[arrayForCreatedPiczlyes objectAtIndex:indexPath.row] valueForKey:@"createdDate"];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    piczlyObj=[arrayForCreatedPiczlyes objectAtIndex:indexPath.row];
    
    if ([piczlyObj.answerType isEqualToNumber:[NSNumber numberWithInt:0]]) {
        
        
        Detail_ViewPCZViewController *DVC=[self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
        DVC.PCZobj=piczlyObj;
        [self.navigationController pushViewController:DVC animated:YES];
    }
    else
    {
        Detail_ViewPCZ_ABCDViewController *DVC=[self.storyboard instantiateViewControllerWithIdentifier:@"Details_abcd"];
        DVC.PCZObj=piczlyObj;
        [self.navigationController pushViewController:DVC animated:YES];
    }
}
@end
