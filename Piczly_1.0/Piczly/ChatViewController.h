//
//  ChatViewController.h
//  Piczly
//
//  Created by sekhar on 24/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PiczlyUser.h"
#import "PCZ_Piczly.h"
#import "PiczlyUser.h"


@interface ChatViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *chatTVC;
@property (strong, nonatomic) IBOutlet UITextField *chatTxtFld;
@property (strong, nonatomic) IBOutlet UIView      *notificationView;
@property (strong, nonatomic) IBOutlet UILabel     *piczlyStatusLbl;
@property (strong, nonatomic) IBOutlet UILabel     *chatLbl;
@property (strong, nonatomic) IBOutlet UIView      *viewForBorder;
@property (strong, nonatomic) IBOutlet UIButton    *chatMsgBtn;
@property (strong, nonatomic) IBOutlet UIView      *viewForNoChatMsg;
@property (strong, nonatomic) IBOutlet UILabel     *predefineMsgLbl;

@property (assign, nonatomic) NSInteger       chatcount;
@property (strong, nonatomic) NSMutableArray *chatData;
@property (strong, nonatomic) PCZ_Piczly     *piczly;
@property (strong, nonatomic) PiczlyUser     *piczlyUser;


@end
