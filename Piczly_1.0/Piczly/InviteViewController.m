//
//  InviteViewController.m
//  Piczly
//
//  Created by sekhar on 20/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "InviteViewController.h"
#import "PiczlyUser.h"
#import "FBHandler.h"
#import "InviteTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "AVCamPreviewView.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "PiczlyUser.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface InviteViewController ()
{
    NSMutableSet *selectedPiczlyUsersSet,*selectedUsersSet,*checkedUserList;
    NSMutableArray *arrayCheckUnchek,*selectedUserDetailsArray;

}

@end

NSMutableArray *checkedList;

@implementation InviteViewController

#pragma mark ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    checkedList=[[NSMutableArray alloc]init];

    [_piczlyInviteTableView.layer setBorderWidth: 0.4];
    [_piczlyInviteTableView.layer setCornerRadius:15.0f];
    [_piczlyInviteTableView.layer setMasksToBounds:YES];
    [_piczlyInviteTableView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    _viewImageView.image=_viewBackgroundImage;
    _viewImageView.alpha=0.3;
    
    if(_piczly)
    {
        [self loadPiczly];
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    selectedUsersSet=[[NSMutableSet alloc]init];
    checkedUserList=[[NSMutableSet alloc]init];
    selectedUserDetailsArray=[[NSMutableArray alloc]init];
}
//load piczly data
-(void)loadPiczly
{
    PFQuery *queryForInvitedFriends=[PFQuery queryWithClassName:@"InvitedFriends"];
    [queryForInvitedFriends includeKey:@"piczlyObjectId"];
    [queryForInvitedFriends includeKey:@"invitedFriends"];
    [queryForInvitedFriends whereKey:@"piczlyObjectId" equalTo:_piczly];
    [queryForInvitedFriends findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (objects.count>0)
        {
            [checkedList addObjectsFromArray:[objects valueForKey:@"invitedFriends"]];
            [checkedUserList addObjectsFromArray:[objects valueForKey:@"invitedFriends"]];
            [_piczlyInviteTableView reloadData];
        }
        
    }];
    
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView Delegate and Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 60;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_piczlyUsersArray count];

}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *cellIdentifier = @"cellIdentifier";
        
        InviteTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(!cell2)
        {
            
            cell2 = [[InviteTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
                   }
        
        PiczlyUser *user;
        user = [_piczlyUsersArray objectAtIndex:indexPath.row];
    
        cell2.facebookFriendsImage.layer.cornerRadius = cell2.facebookFriendsImage.frame.size.width / 2;
        cell2.facebookFriendsImage.clipsToBounds = YES;
        [cell2.facebookFriendsImage.layer setBorderWidth:3.5];
    
        
        cell2.facebookFriendsImage.layer.borderColor = [[UIColor colorWithRed:248.0 green:248.0 blue:250.0 alpha:100]CGColor];
        cell2.delegateForInvite=self;
        
        cell2.facebookFriendsImage.image = [UIImage imageNamed:@"profile_default"];
        
        NSURL *url = [NSURL URLWithString:user.userImageFile.url];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage = [UIImage imageNamed:@"profile_default"];
        [cell2.facebookFriendsImage setContentMode:UIViewContentModeScaleAspectFit];
        
        
        NSSet *containsObject = [selectedUsersSet filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@", user.objectId]];
        
        
        
        if ([containsObject count] > 0)
        {
            cell2.chekBtn.selected = true;
            cell2.selected=true;
        }
        else
        {
            cell2.chekBtn.selected = false;
            cell2.selected=false;
        }
        
        [cell2.facebookFriendsImage setImageWithURLRequest:request
                                          placeholderImage:placeholderImage
                                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                       cell2.facebookFriendsImage.image = image;
                                                       [cell2.facebookFriendsImage setNeedsLayout];
                                                       
                                                   } failure:nil];
        
        cell2.facebookFriendsNames.text = user.displayName;
        
        
        
        
        
        if (checkedList.count>0)
        {
            NSSet *containsObject = [checkedUserList filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@", user.objectId]];
            
            
            
            if ([containsObject count] > 0)
            {
                cell2.chekBtn.selected = true;
                cell2.selected=true;
            }
            else
            {
                cell2.chekBtn.selected = false;
                cell2.selected=false;
            }
            
        }
        
        return cell2;
    
}
-(void)cellDidTapCheckedBtn:(InviteTableViewCell *)myCell isSelected:(BOOL)isSelected
{
    PiczlyUser *user=[_piczlyUsersArray objectAtIndex:[_piczlyInviteTableView indexPathForCell:myCell].row];

    if (_piczly)
    {
        if (isSelected) {
            
            [checkedUserList addObject:user];
            [checkedList addObject:user];
        }
        else
        {
            
            NSSet *containsObject = [checkedUserList filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",user.objectId]];
            for (id czeUser in containsObject)
            {
                [checkedUserList removeObject:czeUser];

            
            }
            [checkedList removeObject:user];
        }
        
    }
    else
    {
        if (isSelected)
        {
            
            [selectedUsersSet addObject:user];
            [selectedUserDetailsArray addObject:user];
        }
        else
        {
            NSSet *containsObject = [selectedUsersSet filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",user.objectId]];
            for (id czeUser in containsObject)
            {
                [selectedUsersSet removeObject:czeUser];
                [selectedUserDetailsArray removeObject:czeUser];
            }
            
        }
    }
    [_piczlyInviteTableView reloadData];
    
    
}
#pragma mark Pop To previous Controller

- (IBAction)cancelActn:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)inviteeBtnClicked:(id)sender
{
    
    if (_piczly)
    {
        [_delegate dataTransfertoViewController:checkedList];
    }
    else{
        
        [_delegate dataTransfertoViewController:selectedUserDetailsArray];

    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
