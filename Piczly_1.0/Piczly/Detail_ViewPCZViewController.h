//
//  Detail_ViewPCZViewController.h
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "PCZ_Piczly.h"
#import "ResponseTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"


@class CircleProgressView;

@interface Detail_ViewPCZViewController : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *arr_userDetails;
    NSMutableArray *dataArray,*yesDataArray,*noDataArray,*dataArrayForTableView;
    
    
    CGRect ViewFrame;
    
    
    __weak IBOutlet UILabel     *lblForNoPercentage;
    
    __weak IBOutlet UILabel     *lblForYesPercentage;
    
    
    __weak IBOutlet UIImageView *innerGraphImgView;
    
    __weak IBOutlet UIImageView *outerGaphImgView;
    
    
    __weak IBOutlet UILabel     *titleForImgInside;
    
    
    __weak IBOutlet UILabel     *titleForImgOutside;
    
}
@property (nonatomic ,strong) NSString         *currentUser;

@property (nonatomic, strong) UIButton         *sendBtn;
@property (nonatomic, strong) PCZ_Piczly       *PCZobj;
@property (weak, nonatomic)   IBOutlet UILabel *detailTopicPCZLabel;
@property (weak, nonatomic)   IBOutlet UILabel *detailQuestionPCZLabel;

//UIScrollView
@property (weak, nonatomic) IBOutlet UIScrollView *scrollV_View;
//Swipe onImageView and UIView Properties
@property (weak, nonatomic) IBOutlet UIView       *view_Swipe;
@property(nonatomic,retain) IBOutlet UIButton     *btn_yes;
@property(nonatomic,retain) IBOutlet UIButton     *btn_no;
@property(nonatomic,retain) IBOutlet UIButton     *btn_loadMore;
@property(nonatomic,retain) IBOutlet UIImageView  *imgv_viewSwipe;

//SwipeView Methods Declarations
-(IBAction)buttonYes_Click:(id)sender;
-(IBAction)buttonNo_Click:(id)sender;
-(IBAction)buttonloadMore_Click:(id)sender;

@property (weak, nonatomic) IBOutlet  UIButton           *loadMorePrp;


//UITableview and UITableview Cell
@property(nonatomic,retain)IBOutlet   UITableView         *tablV_responseTable;
@property(nonatomic,retain)IBOutlet   UITableViewCell     *tablVCell_responseTable;
//UIbutton Flipside Property & Method Declaration
@property (nonatomic,retain) IBOutlet UIButton            *btn_flipButton;
@property (nonatomic,retain) IBOutlet UIButton            *btn_flipbackButton;
@property (nonatomic,retain) IBOutlet UIView              *view_filpside;

-(IBAction)btnflip_Click:(id)sender;
-(IBAction)btnflipBack_Click:(id)sender;

//ProgressView Properties
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView1;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView2;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView3;
@property (nonatomic,retain)  IBOutlet UIImageView        *imgv_progressbg;

//user choosing opetions
@property (weak, nonatomic)   IBOutlet UIButton   *noBtnPrp;
@property (weak, nonatomic)   IBOutlet UIButton   *yesBtnPrp;


- (IBAction)YesNoBtnAction:(UIButton *)sender;
@property (weak, nonatomic)   IBOutlet UIImageView *detailImgPCZ;
@property (weak, nonatomic)   IBOutlet UITextField *PCZ_txtField;

@property (weak, nonatomic)   IBOutlet UITextField *txtf_because;
@property (strong, nonatomic) IBOutlet UIButton    *submitBtn;
@property (strong, nonatomic) IBOutlet UIButton    *chatBtn;
@property (strong, nonatomic) IBOutlet UITextField *chatView_txtf;

- (IBAction)chatBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_ChatPopUp;

- (IBAction)cancelBtn_Clicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView      *Chat_View;
@property (strong, nonatomic) IBOutlet UIButton    *chatView_chatBtn;
@property (strong, nonatomic) IBOutlet UIImageView *becauseTxtf_Img;
@property (strong, nonatomic) IBOutlet UITextField *chatView_becauseTxtf;

@end
