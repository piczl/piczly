//
//  MyFeedViewController.h
//  Piczly
//
//  Created by sekhar on 26/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PiczlyUser.h"
#import "PCZHomeViewController.h"
#import "PCZ_Piczly.h"
#import "MyFeedTableViewCell.h"
#import "CreatePiczlyViewController.h"
#import "Detail_ViewPCZViewController.h"
#import "Detail_ViewPCZ_ABCDViewController.h"
#import "AppDelegate.h"
#import "SeeAllPiczysFromUserNameViewController.h"

@class PCZHomeViewController;
@interface MyFeedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MyFeedTableViewDelegate>
{
    PCZHomeViewController *HomeVC;
    
    
    __weak IBOutlet UIView *feedProfileView;
    __weak IBOutlet UIImageView *profilePicLeftImgView;
    
    __weak IBOutlet UIImageView *profilePicRightImgView;
    
    __weak IBOutlet UIImageView *profilePicImgView;
    
    __weak IBOutlet UILabel *profileNameLbl;    
    
    __weak IBOutlet UITableView *feedsTableView;
    
    PCZ_InvitedFriends  *PCZIFrnds;
    NSMutableArray      *userCreatedPiczly,*piczlyImage,*InvitedUsers;
    PiczlyUser          *pczUserForParticularCell;
    
    
}
@property (weak, nonatomic)   IBOutlet UIButton *createPiczybtn;
@property (weak, nonatomic)   IBOutlet UILabel  *followinglbl;
@property (weak, nonatomic)   IBOutlet UILabel  *frndsLbl;
@property (weak, nonatomic)   IBOutlet UILabel  *followersLbl;

@property (nonatomic, strong) PCZ_Piczly *PCZObj,*pCZObjForTableView;
@property (nonatomic, strong) PiczlyUser *PCZUserForTableView;

@property NSMutableSet *set_SelectedCell;

- (IBAction)createPiczlyBtnAction:(id)sender;




@end
