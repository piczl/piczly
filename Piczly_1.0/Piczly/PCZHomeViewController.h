//
//  piczlyLoginViewController.h
//  Piczly
//
//  Created by BHANU on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PiczlyUser.h"
#import "HomeTableViewCell.h"
#import <Social/Social.h>
#import "PCZ_Piczly.h"
#import "Detail_ViewPCZViewController.h"
#import "Detail_ViewPCZ_ABCDViewController.h"
#import "MapViewController.h"

@interface PCZHomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,HomeTableViewCellDelegate>

@property (strong, nonatomic) IBOutlet  UILabel     *profileName;
@property (strong, nonatomic) IBOutlet  UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet  UITableView *newsFeedTableView;
@property (weak, nonatomic)   IBOutlet  UIButton    *createPiczlyBtn;
@property (nonatomic, strong) PCZ_Piczly *PCZObj;

@property NSMutableSet   *set_SelectedCell;
@property NSMutableArray *invitedPeopleArray;
@property NSMutableArray *piczlyArray;
@property NSDictionary   *userData;

@end
