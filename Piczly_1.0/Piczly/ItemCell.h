//
//  ItemCell.h
//  Piczly
//
//  Created by Hanuman Saini on 27/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *itemImg;
@property (strong, nonatomic) IBOutlet UILabel     *menuLbl;
@property (weak, nonatomic)   IBOutlet UIImageView *detailImg;

@end
