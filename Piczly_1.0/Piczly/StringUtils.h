//
//  StringUtils.h
//  LytePole
//
//  Created by Umesh Naidu on 09/09/14.
//  Copyright (c) 2014 Umesh Naidu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject

@property (nonatomic,retain)NSString *manditoryStr;
@property (nonatomic, retain) NSString *emailInvaidStr;
@property (nonatomic, retain)NSString *passwordsMatch;

+ (StringUtils *)sharedGlobals;

@end
