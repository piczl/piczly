//
//  Detail_ViewPCZ_ABCDViewController.h
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "PCZ_Piczly.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "ResponseTableViewCell.h"
#import "MBProgressHUD.h"
#import "Session.h"


@class CircleProgressView;

@interface Detail_ViewPCZ_ABCDViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSMutableArray          *dataArray,*aDataArray,*bDataArray,*cDataArray,*dDataArray,*dataArrayForTableView;
    Session                 *sessionObj;
    
    CGRect ViewFrame;

    __weak IBOutlet UILabel *dTitleLbl;
    
    __weak IBOutlet UILabel *cTitleLbl;
    
    __weak IBOutlet UILabel *bTitleLbl;
    
    __weak IBOutlet UILabel *aTitleLbl;
    
    
    __weak IBOutlet UILabel *total_countLbl;
    
    
}
@property (nonatomic ,strong) NSString                 *currentUser;
@property (nonatomic,retain)  IBOutlet UITableViewCell *tablVCell_responseTable;
@property (nonatomic, strong) PCZ_Piczly               *PCZObj;
@property (weak, nonatomic)   IBOutlet UILabel         *detailPCZTopicLbl;
@property (weak, nonatomic)   IBOutlet UILabel         *detailQusetionPCZLbl;
@property (weak, nonatomic)   IBOutlet UILabel         *detailOptionALbl;
@property (weak, nonatomic)   IBOutlet UILabel         *detailOptionBLbl;
@property (weak, nonatomic)   IBOutlet UILabel         *detailOptionCLbl;
@property (weak, nonatomic)   IBOutlet UILabel         *detailOptionDLbl;
@property (weak, nonatomic)   IBOutlet UIImageView     *detailPCZImg;

//ProgressView Properties
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView1;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView2;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView3;
@property (nonatomic,retain)  IBOutlet UIImageView        *imgv_progressbg;
@property (weak, nonatomic)   IBOutlet UIView             *flipView;
@property (weak, nonatomic)   IBOutlet CircleProgressView *circleProgressViewA;
@property (weak, nonatomic)   IBOutlet CircleProgressView *circleProgressViewB;

@property (weak, nonatomic) IBOutlet CircleProgressView   *circleProgressViewC;

@property (weak, nonatomic) IBOutlet CircleProgressView   *circleProgressViewD;
@property (weak, nonatomic) IBOutlet UIButton             *flipPrp;
@property (weak, nonatomic) IBOutlet UIView               *view_flip;
@property (weak, nonatomic) IBOutlet UIImageView          *img_ViewSwipe;
@property (weak, nonatomic) IBOutlet UITableView          *tableViewForUserAnswers;
@property (weak, nonatomic) IBOutlet UIButton             *loadMorePrp;
- (IBAction)loadMoreBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField          *txtf_because;



@property (weak, nonatomic) IBOutlet UIButton *btnAPrp;
@property (weak, nonatomic) IBOutlet UIButton *btnBPrp;
@property (weak, nonatomic) IBOutlet UIButton *btnCPrp;
@property (weak, nonatomic) IBOutlet UIButton *btnDPrp;
- (IBAction)BtnActionForABCD:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewProgBr;


@property (weak, nonatomic) IBOutlet UILabel *lblDPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblCPercentage;

@property (weak, nonatomic) IBOutlet UILabel *lblBPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblAPercentage;

@property (weak, nonatomic) IBOutlet UIButton *aBtnPrp;
@property (weak, nonatomic) IBOutlet UIButton *bBtnPrp;

@property (weak, nonatomic) IBOutlet UIButton *cBtnPrp;

@property (weak, nonatomic) IBOutlet UIButton *dBtnPrp;

- (IBAction)btnActionForABCDCheckMark:(id)sender;

- (IBAction)sendBtnClicked:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewForFullView;
@property (strong, nonatomic) IBOutlet UIButton     *scrollPrp;
@property (weak, nonatomic)   IBOutlet UIButton     *aBtnViewPrp;
@property (weak, nonatomic)   IBOutlet UIButton     *bBtnViewPrp;
@property (weak, nonatomic)   IBOutlet UIButton     *cBtnViewPrp;
@property (strong, nonatomic) IBOutlet UILabel      *aLabelPrp;
@property (strong, nonatomic) IBOutlet UILabel      *bLabelPrp;
@property (strong, nonatomic) IBOutlet UILabel      *cLabelPrp;
@property (strong, nonatomic) IBOutlet UILabel      *dLabelPrp;

@property (weak, nonatomic)   IBOutlet UIButton     *dBtnViewPrp;
@property (strong, nonatomic) IBOutlet UIView       *becauseTxtf_View;

@property (weak, nonatomic)   IBOutlet UIView       *makeDecissionView;
@property (strong, nonatomic) IBOutlet UIButton     *submitBtn;
- (IBAction)submiBtnClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton     *chat_Btn;
- (IBAction)chatBtn_Clicked:(id)sender;
@property UIButton *sendBtn;

@property (strong, nonatomic) IBOutlet UIView      *chatPopUp_View;
@property (strong, nonatomic) IBOutlet UIView      *chat_View;
@property (strong, nonatomic) IBOutlet UITextField *becauseTxtf_ChatView;
@property (strong, nonatomic) IBOutlet UIImageView *becauseTxtf_img;
@property (strong, nonatomic) IBOutlet UIButton    *chatView_chatBtn;

@end
