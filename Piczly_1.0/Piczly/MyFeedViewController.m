//
//  MyFeedViewController.m
//  Piczly
//
//  Created by sekhar on 26/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "MyFeedViewController.h"
#import "SWRevealViewController.h"
#import "MyFeedTableViewCell.h"
#import "PCZ_Piczly.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "SeeAllParticipentsViewController.h"

@interface MyFeedViewController ()
{
    int newBtn_;
}
@end
NSInteger selectedIndex;

NSMutableArray *participantsArray;
@implementation MyFeedViewController

#pragma mark ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    feedsTableView.delegate=self;
    feedsTableView.dataSource=self;
    // Do any additional setup after loading the view.
    HomeVC=[[PCZHomeViewController alloc]init];
    userCreatedPiczly=[[NSMutableArray alloc]init];
    InvitedUsers=[[NSMutableArray alloc]init];
    piczlyImage=[[NSMutableArray alloc]init];
    self.navigationController.navigationBar.hidden=NO;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:53.0/255.0 blue:101.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    participantsArray=[[NSMutableArray alloc]init];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_btn"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = barBtn;
    self.navigationItem.rightBarButtonItem.title=@"My Profile";
    
    [self profilePicture];
    _PCZObj=[[PCZ_Piczly alloc]init];
    [self showData];
    self.set_SelectedCell = [[NSMutableSet alloc]init];
    
}

#pragma mark
#pragma mark retriving data from parse database and storing in array

-(void)showData
{
    PFQuery *query=[PFQuery queryWithClassName:@"Piczly"];
    [query includeKey:@"createdBy"];
    [query orderByDescending:@"updatedAt"];
    [query whereKey:@"createdBy" equalTo:[PiczlyUser currentUser]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (objects.count>0)
         {
             [userCreatedPiczly addObjectsFromArray:objects ];
             [feedsTableView reloadData];
         }
     }];
    
    PFQuery *queryForInvitedFriends=[PFQuery queryWithClassName:@"InvitedFriends"];
    [queryForInvitedFriends includeKey:@"invitedFriends"];
    [queryForInvitedFriends includeKey:@"piczlyObjectId.createdBy"];
    [queryForInvitedFriends whereKey:@"invitedFriends" equalTo:[PFUser currentUser]];
    [queryForInvitedFriends findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
        
        if(objects1.count>0)
        {
            [userCreatedPiczly addObjectsFromArray:objects1];
            [feedsTableView reloadData];
        }
    }];
    
}


-(void)profilePicture
{
    profileNameLbl.text=[PiczlyUser currentUser].username;
    profilePicImgView.image=[UIImage imageNamed:@"images.jpeg"];
    
    profilePicImgView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    profilePicImgView.layer.cornerRadius=profilePicImgView.frame.size.height/2;
    profilePicImgView.layer.borderWidth=6.0;
    profilePicImgView.layer.masksToBounds = YES;
    profilePicImgView.layer.borderColor=[[UIColor whiteColor] CGColor];
    [profilePicImgView sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    profilePicLeftImgView.image=[UIImage imageNamed:@"images.jpeg"];
    
    profilePicLeftImgView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    profilePicLeftImgView.layer.cornerRadius=profilePicLeftImgView.frame.size.height/2;
    profilePicLeftImgView.layer.borderWidth=6.0;
    profilePicLeftImgView.layer.masksToBounds = YES;
    profilePicLeftImgView.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    [profilePicLeftImgView sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    profilePicRightImgView.image=[UIImage imageNamed:@"images.jpeg"];
    
    profilePicRightImgView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    profilePicRightImgView.layer.cornerRadius=profilePicRightImgView.frame.size.height/2;
    profilePicRightImgView.layer.borderWidth=6.0;
    profilePicRightImgView.layer.masksToBounds = YES;
    profilePicRightImgView.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    [profilePicRightImgView sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
}

#pragma mark DropDown Actions


-(void)cellDidTapOnDropDownBtn:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    myCell. itemStatus.enabled =YES;
    UIButton *btn =(UIButton *)myCell;
    btn.selected =!btn.selected;
    
    if ([[userCreatedPiczly objectAtIndex:[feedsTableView indexPathForCell:myCell].row] valueForKey:@"invitedFriends"] )
    {
        
        pczUserForParticularCell=[[[userCreatedPiczly objectAtIndex:[feedsTableView indexPathForCell:myCell].row ] valueForKey:@"piczlyObjectId"]valueForKey:@"createdBy"];
    }
    else
    {
        
        pczUserForParticularCell=[[userCreatedPiczly objectAtIndex:[feedsTableView indexPathForCell:myCell].row ] valueForKey:@"createdBy"];
    }
    
    
    if([pczUserForParticularCell.objectId isEqualToString:[PiczlyUser currentUser].objectId])
    {
        if (btn.selected)
        {
            myCell.dropDownBtnView.hidden = NO;
            myCell.dropDownBtnView.backgroundColor=[UIColor whiteColor];
            myCell.dropDownBtnView.layer.borderWidth=1.0;
            myCell.dropDownBtnView.layer.borderColor=[UIColor grayColor].CGColor;
            myCell.dropDownBtnView.layer.cornerRadius=8.0;
            
            [myCell.editPiczBtnPrp setTitle:@"Edit piczly" forState:UIControlStateNormal];
            [myCell.inviteMorePeoplePrp setTitle:@"Invite more people" forState:UIControlStateNormal];
            
        }
        else
        {
            myCell.dropDownBtnView.hidden = YES;
            
        }
    }else{
        
        if (btn.selected)
        {
            myCell.dropDownBtnView.hidden = NO;
            myCell.dropDownBtnView.backgroundColor=[UIColor whiteColor];
            myCell.dropDownBtnView.layer.borderWidth=1.0;
            myCell.dropDownBtnView.layer.borderColor=[UIColor grayColor].CGColor;
            myCell.dropDownBtnView.layer.cornerRadius=8.0;
            [myCell.editPiczBtnPrp setTitle:@"Edit answer" forState:UIControlStateNormal];
            [myCell.inviteMorePeoplePrp setTitle:@"See all piczls from User’s name" forState:UIControlStateNormal];
        }
        else
        {
            myCell.dropDownBtnView.hidden = YES;
            
        }
    }
    
    
}

-(void)cellDidTapOptionFeedBtn:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    if([_PCZUserForTableView.objectId isEqualToString:[PiczlyUser currentUser].objectId])
    {
        
        CreatePiczlyViewController *createPCZ=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
        createPCZ.PCZQuestionTF.text=[userCreatedPiczly objectAtIndex:myBtn];
        createPCZ.editPiczy=@"Edit";
        createPCZ.piczly=[userCreatedPiczly objectAtIndex:myBtn];
        
        
        [self.navigationController pushViewController:createPCZ animated:YES];
    }
    else
    {
        _PCZObj=[userCreatedPiczly objectAtIndex:myBtn];
        
        if ([[userCreatedPiczly objectAtIndex:myBtn] valueForKey:@"invitedFriends"] )
        {
            _PCZObj=[[userCreatedPiczly objectAtIndex:myBtn] valueForKey:@"piczlyObjectId"];
        }
        else
        {
            _PCZObj=[userCreatedPiczly objectAtIndex:myBtn];
        }
        
        if ([_PCZObj.answerType isEqualToNumber:[NSNumber numberWithInt:0]]) {
            
            
            Detail_ViewPCZViewController *DVC=[self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
            DVC.PCZobj=_PCZObj;
            [self.navigationController pushViewController:DVC animated:YES];
        }
        else
        {
            Detail_ViewPCZ_ABCDViewController *DVC=[self.storyboard instantiateViewControllerWithIdentifier:@"Details_abcd"];
            DVC.PCZObj=_PCZObj;
            [self.navigationController pushViewController:DVC animated:YES];
        }
        
    }
    
}
//Create a piczly based on original piczly

-(void)cellDidTapCreateOnThatTopic:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    
    CreatePiczlyViewController *createPCZ=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
    createPCZ.piczly=[userCreatedPiczly objectAtIndex:myBtn];
    createPCZ.createOnThtTopicRef=@"topic";
    [self.navigationController pushViewController:createPCZ animated:YES];
}

-(void)cellDidTapCreateNewPiczlBasedOnIt:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    CreatePiczlyViewController *createPCZ=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
    createPCZ.piczly=[userCreatedPiczly objectAtIndex:myBtn];
    createPCZ.createOnThtTopicRef=@"topic";
    [self.navigationController pushViewController:createPCZ animated:YES];
    
}

//delete piczly

-(void)cellDidTapOnDeleteBtn:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    
    [self showAlert];
    newBtn_ = myBtn;
    
}
//Inite more people and see all piczlys from username

-(void)cellDidTapInviteMorePeopleBtn:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    if([pczUserForParticularCell.objectId isEqualToString:[PiczlyUser currentUser].objectId])
    {
        CreatePiczlyViewController *createPCZ=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
        createPCZ.piczly=[userCreatedPiczly objectAtIndex:myBtn];
        
        [self.navigationController pushViewController:createPCZ animated:YES];
    }
    else
    {
        SeeAllPiczysFromUserNameViewController *SAPVZ=[self.storyboard instantiateViewControllerWithIdentifier:@"seeAllPiczlys"];
        SAPVZ.userForCreatedPiczly=pczUserForParticularCell;
        [self.navigationController pushViewController:SAPVZ animated:YES];
        
    }
    
    
}

//see all participants for particular piczly

-(void)cellDidTapOnSeeAllParticipants:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    
    PFQuery *queryForParticipants=[PFQuery queryWithClassName:@"InvitedFriends"];
    [queryForParticipants includeKey:@"invitedFriends"];
    [queryForParticipants whereKey:@"piczlyObjectId" equalTo:[userCreatedPiczly objectAtIndex:myBtn]] ;
    
    
    [queryForParticipants findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(objects.count>0)
        {
            [participantsArray addObjectsFromArray:[objects valueForKey:@"invitedFriends"]];
            SeeAllParticipentsViewController *SAPVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ParticipentsVC"];
            SAPVC.participantsArry =[NSMutableArray arrayWithArray:[objects valueForKey:@"invitedFriends"]];
            
            [self.navigationController pushViewController:SAPVC animated:YES];
            
        }
        else
        {
            
            Com_AlertView(@"Info", @"No Participants for this Piczl ");
        }
    }];
    
    
    
}
// share through the social network

-(void)cellDidTapOnShareBtn:(MyFeedTableViewCell *)myCell btnIndex:(int)myBtn
{
    SLComposeViewController *composeController = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [composeController setInitialText:[NSString stringWithFormat:@"%@ have shared this item  topic name: %@ question:%@  ",[PiczlyUser currentUser].username,[[userCreatedPiczly valueForKey:@"topic"] objectAtIndex:myBtn],[[userCreatedPiczly valueForKey:@"question"] objectAtIndex:myBtn]]] ;
    
    PFFile *imagefile=[[userCreatedPiczly valueForKey:@"image"] objectAtIndex:myBtn];
    [imagefile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        [composeController addImage:[UIImage imageWithData:data]];
    }];
    
    [composeController addURL: [NSURL URLWithString:
                                @""]];
    
    [self presentViewController:composeController
                       animated:YES completion:nil];
    
    
    
    
}
#pragma mark
#pragma mark tableview DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userCreatedPiczly count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier=@"myfeedTVC";
    MyFeedTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil)
    {
        cell=[[MyFeedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if ([[userCreatedPiczly objectAtIndex:indexPath.row] valueForKey:@"invitedFriends"] )
    {
        _pCZObjForTableView=[[userCreatedPiczly objectAtIndex:indexPath.row] valueForKey:@"piczlyObjectId"];
        _PCZUserForTableView=[[[userCreatedPiczly objectAtIndex:indexPath.row] valueForKey:@"piczlyObjectId"] valueForKey:@"createdBy"];
        
    }
    else
    {
        _pCZObjForTableView=[userCreatedPiczly objectAtIndex:indexPath.row];
        _PCZUserForTableView=[[userCreatedPiczly objectAtIndex:indexPath.row] valueForKey:@"createdBy"];
    }
    
    
    cell.userProfileName.text=_PCZUserForTableView.username;
    
    cell.userProfileName.textColor = [UIColor colorWithRed:72/255.0 green:72/255.0 blue:72/255.0 alpha:1];
    cell.userProfileImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.userProfileImage.layer.cornerRadius=cell.userProfileImage.frame.size.height/2;
    cell.userProfileImage.layer.borderWidth=6.0;
    cell.userProfileImage.layer.masksToBounds = YES;
    cell.userProfileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    PFFile *fileForProfileImage=_PCZUserForTableView.userImageFile;
    
    [fileForProfileImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
     {
         if (data)
         {
             MyFeedTableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
             if (updateCell)
             {
                 updateCell.userProfileImage.image=[UIImage imageWithData:data];
             }
         }
         
     }];
    
    cell.itemQuestion.text=_pCZObjForTableView.question;
    cell.itemQuestion.textColor = [UIColor colorWithRed:72/255.0 green:72/255.0 blue:72/255.0 alpha:1];
    
    cell.itemTopicName.text=_pCZObjForTableView.topic;
    PFFile *fileForImage= _pCZObjForTableView.image;
    [fileForImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        cell.itemImage.layer.masksToBounds=YES;
        cell.itemImage.layer.cornerRadius=10.0;
        cell.itemImage.image=[UIImage imageWithData:data];
    }];
    
    int number=[_pCZObjForTableView.answerType doubleValue];
    if (number ==[[NSNumber numberWithInt:0] doubleValue])
    {
        cell.itemChoice.textColor = [UIColor colorWithRed:72/255.0 green:72/255.0 blue:72/255.0 alpha:1];
        cell.itemChoice.text=@"Yes-No";
    }
    else
    {
        cell.itemChoice.text=@"A-B-C-D";
    }
    
    
    PFQuery *queryForDetailView=[PFQuery queryWithClassName:@"DetailView"];
    [queryForDetailView whereKey:@"piczly_Info" equalTo:[userCreatedPiczly objectAtIndex:indexPath.row]];
    [queryForDetailView findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        cell.noOfPersonsRespondedToQuestion.text= [NSString stringWithFormat:@"%ld",objects.count];
    }];
    
    
    
    cell.optionsBtnPrp.tag=indexPath.row;
    cell.deletePiczlyPrp.tag=indexPath.row;
    cell.MyFeedDelegate=self;
    cell.dropDownBtnView.tag=1001;
    cell.editPiczBtnPrp.tag=indexPath.row;
    cell.createPiczlyWithinTheTopic.tag=indexPath.row;
    cell.inviteMorePeoplePrp.tag=indexPath.row;
    cell.createNewPiczlyBasedOnItPrp.tag=indexPath.row;
    cell.seeAllPaticipanrtsPrp.tag=indexPath.row;
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapOfCell:)];
    tapGesture.numberOfTapsRequired=1;
    [cell.itemImage addGestureRecognizer:tapGesture];
    cell.itemImage.tag=indexPath.row;
    cell.itemImage.userInteractionEnabled=YES;
    selectedIndex=indexPath.row;
    cell.userProfileImage.tag = indexPath.row;
    cell.noOfTopicWithThatPiczl.text= [NSString stringWithFormat:@"%@",_pCZObjForTableView.topicCount];
    PCZ_Piczly *pczObjTemp=[userCreatedPiczly  objectAtIndex:indexPath.row];
    NSSet *objectPresent = [self.set_SelectedCell filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",pczObjTemp.objectId]];
    if (objectPresent.count>0)
    {
        cell.dropDownBtnView.hidden=NO;
        
    }
    else
    {
        cell.dropDownBtnView.hidden=YES;
        
    }
    
    return cell;
}

-(void)onTapOfCell:(id)sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    
    _PCZObj=[userCreatedPiczly objectAtIndex:gesture.view.tag];
    Detail_ViewPCZViewController *DVC=[self.storyboard instantiateViewControllerWithIdentifier:@"Detail"];
    Detail_ViewPCZ_ABCDViewController *DVC1=[self.storyboard instantiateViewControllerWithIdentifier:@"Details_abcd"];
    
    if ([[userCreatedPiczly objectAtIndex:gesture.view.tag] valueForKey:@"invitedFriends"] )
    {
        _PCZObj=[[userCreatedPiczly objectAtIndex:gesture.view.tag] valueForKey:@"piczlyObjectId"];
        
    }
    else
    {
        _PCZObj=[userCreatedPiczly objectAtIndex:gesture.view.tag];
        DVC.currentUser=@"currentUser";
        DVC1.currentUser=@"currentUser";
    }
    
    if ([_PCZObj.answerType isEqualToNumber:[NSNumber numberWithInt:0]]) {
        
        
        
        DVC.PCZobj=_PCZObj;
        [self.navigationController pushViewController:DVC animated:YES];
    }
    else
    {
        
        DVC1.PCZObj=_PCZObj;
        [self.navigationController pushViewController:DVC1 animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 360;
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark
#pragma mark AlertView and AlertView Delegate Methods

- (void) showAlert
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to delete the cell" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if ([[userCreatedPiczly objectAtIndex:newBtn_] valueForKey:@"invitedFriends"] )
        {
            PCZIFrnds=[userCreatedPiczly objectAtIndex:newBtn_];
            _PCZObj=PCZIFrnds.piczlyObjectId;
            [PCZIFrnds deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded)
                {
                    
                    int count=[_PCZObj.invitedPeopleCount intValue];
                    count--;
                    _PCZObj.invitedPeopleCount=[NSNumber numberWithInt:count];
                    [_PCZObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded)
                        {
                            //for successful saving the data in parse
                        }
                    }];
                    [userCreatedPiczly removeObjectAtIndex:newBtn_];
                    [feedsTableView reloadData];
                }
            }];
            
        }
        else{
            
            _PCZObj=[userCreatedPiczly objectAtIndex:newBtn_];
            
            _PCZObj.piczlyStatus=[NSNumber numberWithInt:0];
            
            [_PCZObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded)
                {
                    
                    [userCreatedPiczly removeObjectAtIndex:newBtn_];
                    [feedsTableView reloadData];
                }
            }];
        }
        
        
    }
}

#pragma mark
#pragma mark CreatePiczly Action

- (IBAction)createPiczlyBtnAction:(id)sender
{
    
    CreatePiczlyViewController *createPiczlyVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
    
    UINavigationController *navController = AppDelegate.getInstance.navigationController;
    navController = [[UINavigationController alloc] initWithRootViewController:createPiczlyVC];
    
    [AppDelegate.getInstance.window.rootViewController presentViewController:navController animated:YES completion:nil];
}

#pragma mark
#pragma mark ScrollView Delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    _createPiczybtn.hidden=YES;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    _createPiczybtn.hidden=NO;
}

@end



