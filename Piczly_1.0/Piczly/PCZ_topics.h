//
//  PCZ_topics.h
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <Parse/Parse.h>

@interface PCZ_topics : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString *topicName;

+(NSString*)parseClassName;

@end
