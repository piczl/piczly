//
//  piczlyLoginViewController.m
//  Piczly
//
//  Created by BHANU on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "PCZHomeViewController.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "PCZLoginViewController.h"
#import "CreatePiczlyViewController.h"
#import "ViewPCZCustomTableViewCell.h"
#import "ViewPiczlyViewController.h"
#import "FBHandler.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "SeeAllParticipentsViewController.h"




@interface PCZHomeViewController ()
{
    NSArray *piczlyUsersArray;
    NSMutableSet *selectedPiczlyUsersSet;
    FBHandler *fbHandler;
    NSInteger value;
}
@end

@implementation PCZHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    self.newsFeedTableView.delegate=self;
    self.newsFeedTableView.dataSource=self;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:53.0/255.0 blue:101.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    UIImage* logoImage = [UIImage imageNamed:@"Logo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_btn"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    _PCZObj=[[PCZ_Piczly alloc]init];
    self.navigationItem.leftBarButtonItem = barBtn;
    
    
    selectedPiczlyUsersSet = [NSMutableSet set];
    piczlyUsersArray       = [NSArray array];
    _invitedPeopleArray    = [[NSMutableArray alloc]init];
    _piczlyArray           = [[NSMutableArray alloc]init];
    
    
    
    [self retriveData];
}
#pragma mark Parsing Data
//fetch the list of piczly's from data base.
-(void)retriveData
{
    PFQuery *queryForInvitedFriends=[PFQuery queryWithClassName:@"InvitedFriends"];
    [queryForInvitedFriends includeKey:@"invitedFriends"];
    [queryForInvitedFriends includeKey:@"piczlyObjectId.createdBy"];
    [queryForInvitedFriends whereKey:@"invitedFriends" equalTo:[PFUser currentUser]];
    [queryForInvitedFriends whereKey:@"AcceptanceStatus"notEqualTo:[NSNumber numberWithInt:0]];
    [queryForInvitedFriends findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
        if(objects1.count>0)
        {
            [_invitedPeopleArray addObjectsFromArray:[[objects1 valueForKey:@"piczlyObjectId"] valueForKey:@"createdBy"]];
            [_piczlyArray addObjectsFromArray:[objects1 valueForKey:@"piczlyObjectId"]];
            [_newsFeedTableView reloadData];
        }
    }];
    
    
    
    
}
#pragma mark MenuView

-(void)menuBtnAction:(UIBarButtonItem *)sender
{
    MenuViewController *MenuVC=[self.storyboard instantiateViewControllerWithIdentifier:@"menuVC"];
    UINavigationController *navController = AppDelegate.getInstance.navigationController;
    navController = [[UINavigationController alloc] initWithRootViewController:MenuVC];
    [AppDelegate.getInstance.window.rootViewController presentViewController:navController animated:YES completion:nil];
}
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark DropDown Actions

-(void)cellDidTapOnOptionsBtn:(HomeTableViewCell *)myCell btnIndex:(int)myBtn
{
    UIButton *btn =(UIButton *)myCell;
    btn.selected =!btn.selected;
    if (btn.selected)
    {
        myCell.viewForDropDown.hidden=NO;
        myCell.viewForDropDown.backgroundColor=[UIColor whiteColor];
        myCell.viewForDropDown.layer.borderWidth=1.0;
        myCell.viewForDropDown.layer.borderColor=[UIColor grayColor].CGColor;
        
        myCell.viewForDropDown.layer.cornerRadius=8.0;
        
        
    }
    else
    {
        myCell.viewForDropDown.hidden = YES;
        
    }
    
    
}
//creating new piczly based on Original Piczly

-(void)cellDidTapOnCreatePiczlyBasedOnItBtn:(HomeTableViewCell *)myCell btnIndex:(int)myBtn
{
    CreatePiczlyViewController *createPCZ=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
    createPCZ.piczly=[_piczlyArray objectAtIndex:myBtn];
    [self.navigationController pushViewController:createPCZ animated:YES];
    
    
}
// see on the map

-(void)cellDidTapOnSeeOnMapBtn:(HomeTableViewCell *)myCell btnIndex:(int)myBtn
{
    
    float f= [[[_piczlyArray objectAtIndex:myCell.dropDownPrp.tag]valueForKey:@"latitude"] floatValue];
    if (f)
    {
        myCell.viewForDropDown.hidden=YES;
        MapViewController *MapVC=[self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
        MapVC.latitude=[[[_piczlyArray  objectAtIndex:myCell.dropDownPrp.tag]valueForKey:@"latitude"] floatValue];
        MapVC.longitude=[[[_piczlyArray  objectAtIndex:myCell.dropDownPrp.tag] valueForKey:@"longitude"]floatValue];
        [self.navigationController pushViewController:MapVC animated:YES];
    }
    else
    {
        myCell.viewForDropDown.hidden=YES;
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"No location configured for this Piczl" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        
    }
}

//sharing through social network

-(void)cellDidTapOnFBShareBtn:(HomeTableViewCell *)myCell btnIndex:(int)myBtn
{
    
    SLComposeViewController *composeController = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [composeController setInitialText:[NSString stringWithFormat:@"%@ have shared this item  topic name: %@ question:%@  ",[PiczlyUser currentUser].username,[[_piczlyArray valueForKey:@"topic"] objectAtIndex:myBtn],[[_piczlyArray valueForKey:@"question"] objectAtIndex:myBtn]]] ;
    
    PFFile *imagefile=[[_piczlyArray valueForKey:@"image"] objectAtIndex:myBtn];
    [imagefile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        [composeController addImage:[UIImage imageWithData:data]];
    }];
    
    [composeController addURL: [NSURL URLWithString:
                                @""]];
    
    [self presentViewController:composeController
                       animated:YES completion:nil];
    
    
}
#pragma mark - UITableViewDatasource methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 360;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_piczlyArray count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"HomeTableVC";
    HomeTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell=[[HomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    _PCZObj=[_piczlyArray objectAtIndex:indexPath.row];
    
    cell.topicNameLbl.text=[[_piczlyArray valueForKey:@"topic"] objectAtIndex:indexPath.row];
    cell.tearOffTopicNameLbl.text=[[_piczlyArray valueForKey:@"topic"] objectAtIndex:indexPath.row];
    
    cell.itemQuestionLbl.text=[[_piczlyArray valueForKey:@"question"] objectAtIndex:indexPath.row];
    cell.createdAtDateLbl.text=[[_piczlyArray valueForKey:@"createdDate"] objectAtIndex:indexPath.row];
    
    int number=[[[_piczlyArray valueForKey:@"answerType"] objectAtIndex:indexPath.row] doubleValue];
    if(number ==[[NSNumber numberWithInt:0]doubleValue])
    {
        cell.iteamStatus.text=@"Yes-No";
    }else
    {
        cell.iteamStatus.text=@"A-B-C-D";
    }
    PFFile *piczlyImgFile= [[_piczlyArray valueForKey:@"image"]objectAtIndex:indexPath.row];
    [piczlyImgFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        cell.piczlyImage.layer.masksToBounds=YES;
        cell.piczlyImage.layer.cornerRadius=10.0;
        cell.piczlyImage.image=[UIImage imageWithData:data];
    }];
    cell.HomeTableCell=self;
    cell.shareBtnprp.tag=indexPath.row;
    cell.dropDownPrp.tag=indexPath.row;
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapOfCell:)];
    tapGesture.numberOfTapsRequired=1;
    [cell.piczlyImage addGestureRecognizer:tapGesture];
    cell.piczlyImage.tag=indexPath.row;
    cell.piczlyImage.userInteractionEnabled=YES;
    
    PFQuery *queryForDetailView=[PFQuery queryWithClassName:@"DetailView"];
    [queryForDetailView whereKey:@"piczly_Info" equalTo:[_piczlyArray objectAtIndex:indexPath.row]];
    [queryForDetailView findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        cell.respondedPeopleToThatQuestion.text= [NSString stringWithFormat:@"%ld",objects.count];
    }];
    
    PCZ_Piczly *pczObjTemp=[_piczlyArray  objectAtIndex:indexPath.row];
    NSSet *objectPresent = [self.set_SelectedCell filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"objectId == %@",pczObjTemp.objectId]];
    if (objectPresent.count>0)
    {
        
        cell.viewForDropDown.hidden=NO;
    }
    else
    {
        
        cell.viewForDropDown.hidden=YES;
    }
    
    
    
    return cell;
}


-(void)onTapOfCell:(id)sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Like to attend this event with your friend and have a good time" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes  ", nil];
    [alert show];
    
    _PCZObj=[_piczlyArray objectAtIndex:gesture.view.tag];
    value=gesture.view.tag;
    
    
    
}
#pragma mark Alerview Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        
        PFQuery *queryforDeletingInvite=[PFQuery queryWithClassName:@"InvitedFriends"];
        [queryforDeletingInvite whereKey:@"piczlyObjectId" equalTo:_PCZObj];
        [queryforDeletingInvite findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            for(int m=0;m<[objects count];m++)
            {
                PFObject *obj=[PFObject objectWithoutDataWithClassName:@"InvitedFriends" objectId:[[objects valueForKey:@"objectId"] objectAtIndex:m]];
                obj[@"AcceptanceStatus"]=[NSNumber numberWithInt:0];
                [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(!error)
                    {
                        [_piczlyArray removeObjectAtIndex:value];
                        [_newsFeedTableView reloadData];
                    }
                }];
            }
        }];
        
    }
    else if (buttonIndex==1)
    {
        
        CreatePiczlyViewController *createVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
        createVC.piczly=_PCZObj;
        [self.navigationController pushViewController:createVC animated:NO];
        
    }
}
#pragma mark Create New Piczly



- (IBAction)createPiczlyBtn:(id)sender
{
    
    CreatePiczlyViewController *createPiczlyVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
    
    UINavigationController *navController = AppDelegate.getInstance.navigationController;
    navController = [[UINavigationController alloc] initWithRootViewController:createPiczlyVC];
    [AppDelegate.getInstance.window.rootViewController presentViewController:navController animated:YES completion:nil];
}


#pragma mark ScrollView Delegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    
    _createPiczlyBtn.hidden=YES;
    
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    _createPiczlyBtn.hidden=NO;
    
}
@end
