//
//  Detail_ViewPCZViewController.m
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "Detail_ViewPCZViewController.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "CircleProgressView.h"
#import "Session.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ChatViewController.h"
#import "PCZ_DetailView.h"
#import "PiczlyUser.h"


@interface Detail_ViewPCZViewController ()
{
    int btnIndex,addThreeObjs,tableViewTotalCount;
    int btnTag,chatcount;
}

@property (nonatomic)         Session        *session;
@property (nonatomic, strong) PCZ_DetailView *PCZ_DetailObj;
@property (nonatomic, strong) PiczlyUser     *PCZUser;

@end

PFObject *detailViewObj;

@implementation Detail_ViewPCZViewController

@synthesize scrollV_View,view_Swipe;
@synthesize tablV_responseTable,tablVCell_responseTable;
@synthesize imgv_viewSwipe;
@synthesize btn_flipButton,btn_flipbackButton,view_filpside,imgv_progressbg;
@synthesize btn_yes,btn_loadMore,btn_no;
@synthesize txtf_because;

#pragma mark View LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.txtf_because.textColor = [UIColor colorWithRed:72/255.0 green:72/255.0 blue:72/255.0 alpha:1];
    self.sendBtn  =[[UIButton alloc]initWithFrame:CGRectMake(275, 435, 30, 30)];
    [self.view addSubview:self.sendBtn];
    self.sendBtn.hidden = YES;
    [self.sendBtn addTarget:self action:@selector(sendBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap)];
    [singleTap setNumberOfTapsRequired:1];
    [self.detailImgPCZ setUserInteractionEnabled:YES];
    [self.detailImgPCZ addGestureRecognizer:singleTap];
    [self.Chat_View setUserInteractionEnabled:YES];
    [self.Chat_View addGestureRecognizer:singleTap];
    [self.view_ChatPopUp setUserInteractionEnabled:YES];
    [self.view_ChatPopUp addGestureRecognizer:singleTap];
    
    self.chatView_txtf.delegate = self;
    self.chatView_becauseTxtf.delegate = self;
    self.chatView_becauseTxtf.hidden = NO;
    self.chatView_chatBtn.hidden = NO;
    self.becauseTxtf_Img.hidden =NO;
    self.chatView_becauseTxtf.delegate =self;
    [self gettingDetailViewData];
    [self.navigationItem setTitle:@"Opened Piczl"];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor]];
    if([_currentUser isEqualToString:@"currentUser"])
    {
    }
    else
    {
        btn_flipButton.hidden=YES;
        
    }
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"downarrowopenedpiczly_2.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(dropDown_Clicked:)];
    
    [[self navigationItem] setRightBarButtonItem:rightItem];
    detailViewObj=[PFObject objectWithClassName:@"DetailView"];
    
    dataArray             = [[NSMutableArray alloc]init];
    yesDataArray          = [[NSMutableArray alloc]init];
    noDataArray           = [[NSMutableArray alloc]init];
    dataArrayForTableView = [[NSMutableArray alloc]init];
    
    tablV_responseTable.delegate=self;
    tablV_responseTable.dataSource=self;
    view_Swipe.hidden=YES;
    view_filpside.hidden=YES;
    txtf_because.delegate=self;
    //UITable Array
    arr_userDetails =[[NSMutableArray alloc]initWithObjects:@"One",@"Two",@"Three", nil];
    
    //Default Yes Seelction Button Click
    [btn_yes setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                       forState:UIControlStateNormal];
    [btn_yes setTitle:@"Yes" forState:UIControlStateNormal];
    [btn_yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    
    
    //UIProgressView Color bar code
    self.session = [[Session alloc] init];
    self.session.state = kSessionStateStop;
    
    
    [self.circleProgressView1 setupView:[UIColor colorWithRed:39.0/255.0 green:145.0/255.0 blue:123.0/255.0 alpha:100.0]];
    [self.circleProgressView setupView:[UIColor colorWithRed:68.0/255.0 green:82.0/255.0 blue:117.0/255.0 alpha:100.0]];
    
    
    self.circleProgressView1.tintColor=[UIColor colorWithRed:115.0/255.0 green:231.0/255.0 blue:137.0/255.0 alpha:100];
    self.circleProgressView.tintColor=[UIColor colorWithRed:231.0/255.0 green:115.0/255.0 blue:115.0/255.0 alpha:100];
    
    
    
    self.circleProgressView1.timeLimit =100;
    
    self.circleProgressView.timeLimit =100;
    
    //Adjuting the UIScrollView Frame
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        scrollV_View.contentSize = CGSizeMake(scrollV_View.frame.size.width,  scrollV_View.frame.size.height);
    }else{
        scrollV_View.contentSize = CGSizeMake(scrollV_View.frame.size.width,scrollV_View.frame.size.height);
    }
    //UISwipegesture RecognizerRight on imageview
    UISwipeGestureRecognizer *oneFingerSwipeRight =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    [_detailImgPCZ setUserInteractionEnabled:YES];
    [_detailImgPCZ addGestureRecognizer:oneFingerSwipeRight];
    
    //UISwipegesture RecognizerLeft on imageview
    UISwipeGestureRecognizer *oneFingerSwipeLeft =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionRight];
    imgv_viewSwipe.userInteractionEnabled = YES;
    [imgv_viewSwipe addGestureRecognizer:oneFingerSwipeLeft];
    
    //viewflipside image gesture recognizers
    UISwipeGestureRecognizer *progressRight =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(progressswipeRight)];
    [progressRight setDirection:UISwipeGestureRecognizerDirectionLeft];
    imgv_progressbg.userInteractionEnabled = YES;
    [imgv_progressbg addGestureRecognizer:progressRight];
    
    
    
    if (self.PCZobj)
    {
        _detailTopicPCZLabel.text=_PCZobj.topic;
        _detailQuestionPCZLabel.text=_PCZobj.question;
        
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = _detailImgPCZ.center;
        activityIndicator.hidesWhenStopped = YES;
        [_detailImgPCZ addSubview:activityIndicator];
        [activityIndicator startAnimating];
        [_detailImgPCZ sd_setImageWithURL:[NSURL URLWithString:_PCZobj.image.url] placeholderImage:[UIImage imageNamed:@"vdo_preview.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
        
        self.submitBtn.hidden = YES;
        //self.view.hidden = NO;
        self.view_ChatPopUp.hidden = YES;
        self.view_ChatPopUp.layer.borderWidth=0.1;
        [self.view_ChatPopUp.layer setCornerRadius:10.0f];
        [self.view_ChatPopUp.layer setMasksToBounds:YES];
        self.view_ChatPopUp.layer.borderColor=[[UIColor grayColor] CGColor];
        self.Chat_View.layer.borderWidth=0.1;
        [self.Chat_View.layer setCornerRadius:10.0f];
        [self.Chat_View.layer setMasksToBounds:YES];
        self.Chat_View.layer.borderColor=[[UIColor grayColor] CGColor];
        
        
        
    }
    [self numofChatMessages];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
}
#pragma mark Keyboard Disaapearing Methods

-(void)dismissKeyboard
{
    [self.txtf_because resignFirstResponder];
}

-(void)oneTap
{
    [self.view endEditing:YES];
    [self.Chat_View endEditing:YES];
    [self.view_ChatPopUp endEditing:YES];
    
}

#pragma mark Parsing For Piczly Detailed Data

-(void)gettingDetailViewData
{
    PFQuery *detailViewQuery=[PCZ_DetailView query];
    [detailViewQuery whereKey:@"piczly_Info" equalTo:self.PCZobj];
    [detailViewQuery whereKey:@"user_Info" equalTo:[PiczlyUser currentUser]];
    [detailViewQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects.count>0) {
            _PCZ_DetailObj=[objects objectAtIndex:0];
            txtf_because.text=_PCZ_DetailObj.userAnswer;
            if ([_PCZ_DetailObj.decision intValue]==0)
            {
                
                [_noBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                     forState:UIControlStateNormal];
                [_yesBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                                      forState:UIControlStateNormal];
            }
            else
            {
                
                [_noBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                                     forState:UIControlStateNormal];
                [_yesBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                                      forState:UIControlStateNormal];
            }
            
            
        }
        
    }];
    
    
}

- (IBAction)dropDown_Clicked:(id)sender
{
    
    
}


-(IBAction)buttonYes_Click:(id)sender
{
    
    [btn_yes setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                       forState:UIControlStateNormal];
    [btn_yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btn_no setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                      forState:UIControlStateNormal];
    
    [btn_no setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100.0] forState:UIControlStateNormal];
    
    
    [dataArrayForTableView removeAllObjects];
    [dataArrayForTableView addObjectsFromArray:yesDataArray];
    [tablV_responseTable reloadData];
    
    
}
-(IBAction)buttonNo_Click:(id)sender
{
    [btn_no setBackgroundImage:[UIImage imageNamed:@"SelectedBtnInViewPCZ.png"]
                      forState:UIControlStateNormal];
    
    [btn_no setTitle:@"No" forState:UIControlStateNormal];
    [btn_no setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [btn_yes setTitle:@"Yes" forState:UIControlStateNormal];
    [btn_yes setTitleColor:[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100.0] forState:UIControlStateNormal];
    [btn_yes setBackgroundImage:[UIImage imageNamed:@"DeselectedBtnInViewPCZ.png"]
                       forState:UIControlStateNormal];
    
    [dataArrayForTableView removeAllObjects];
    [dataArrayForTableView addObjectsFromArray:noDataArray];
    [tablV_responseTable reloadData];
    
}
// load more Users's lists amoun of data for particular piczly

-(IBAction)buttonloadMore_Click:(id)sender{
    
    tableViewTotalCount=(int)[dataArrayForTableView count];
    // int tableViewTotalCount
    [dataArrayForTableView removeAllObjects];
    if (btnIndex==0)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (yesDataArray.count>i)
            {
                [dataArrayForTableView addObject:[yesDataArray objectAtIndex:i]];
                
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    else if (btnIndex==0)
    {
        for (int i=0; i<tableViewTotalCount+addThreeObjs; i++)
        {
            if (noDataArray.count>i)
            {
                [dataArrayForTableView addObject:[noDataArray objectAtIndex:i]];
                
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    addThreeObjs+=3;
    [tablV_responseTable reloadData];
    
}

#pragma mark Piczly Graph Distribution

-(IBAction)btnflip_Click:(id)sender;
{
    
    
    if([[PiczlyUser currentUser].objectId isEqualToString:_PCZobj.createdBy.objectId])
    {
        view_filpside.hidden=NO;
        [self fetchingDataForTableViewAndQuardCurve];
        
        
        [UIView transitionWithView:view_filpside duration:.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            
        } completion:^(BOOL finished){
        }];
        
    }else
    {
        
        view_filpside.hidden=NO;
        
        
        UIImageView *imgView=[[UIImageView alloc]initWithFrame:view_filpside.bounds];
        [view_filpside addSubview:imgView];
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:_PCZobj.image.url] placeholderImage:[UIImage imageNamed:@"profile_bg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
        
    }
    
    
    
    
    
}

-(IBAction)btnflipBack_Click:(id)sender
{
    view_filpside.hidden=YES;
    
    [UIView transitionWithView:view_filpside duration:.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        
    } completion:^(BOOL finished){
    }];
}

#pragma mark User Answers
//To view the user answers for particualr piczly
-(void)progressswipeRight
{
    view_filpside.hidden=YES;
    view_Swipe.hidden=NO;
    
}
-(void)swipeLeft
{
    view_Swipe.hidden=YES;
    view_filpside.hidden=YES;
}

-(void)swipeRight
{
    view_Swipe.hidden=NO;
    [self fetchingDataForTableViewAndQuardCurve];
    
}
#pragma mark Parsing Data For Tableview

-(void)fetchingDataForTableViewAndQuardCurve
{
    [dataArrayForTableView removeAllObjects];
    [dataArray removeAllObjects];
    [noDataArray removeAllObjects];
    [yesDataArray removeAllObjects];
    
    self.circleProgressView.elapsedTime=0;
    self.circleProgressView1.elapsedTime=0;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    PFQuery *query=[PFQuery queryWithClassName:@"DetailView"];
    [query whereKey:@"piczly_Info" equalTo:self.PCZobj];
    [query whereKey:@"user_Info" notEqualTo:_PCZobj.createdBy];
    [query includeKey:@"user_Info"];
    [query includeKey:@"piczly_Info"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (objects.count>0)
        {
            [dataArray addObjectsFromArray:objects];
            
            for (int i=0; i<dataArray.count; i++)
            {
                _PCZ_DetailObj=[dataArray objectAtIndex:i];
                if ([_PCZ_DetailObj.decision doubleValue]==0)
                {
                    [noDataArray addObject:[dataArray objectAtIndex:i]];
                }
                else
                {
                    [yesDataArray addObject:[dataArray objectAtIndex:i]];
                }
            }
            
            for (int i=0; i<3; i++)
            {
                if (yesDataArray.count>i)
                {
                    [dataArrayForTableView addObject:[yesDataArray objectAtIndex:i]];
                }
                else
                {
                    _loadMorePrp.alpha=0.5;
                    _loadMorePrp.userInteractionEnabled=NO;
                }
            }
            [tablV_responseTable reloadData];
            
            float a,b,c;
            int i;
            a=(float)noDataArray.count ;
            b=(float)yesDataArray.count;
            c=(float)dataArray.count;
            i=(a/c)*100;
            
            lblForYesPercentage.textColor=[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:100.0];
            lblForNoPercentage.textColor=[UIColor colorWithRed:231.0/255.0 green:115.0/255.0 blue:115.0/255.0 alpha:100.0];
            
            if (yesDataArray.count>=noDataArray.count)
            {
                
                self.circleProgressView.elapsedTime=i;
                self.circleProgressView1.elapsedTime=100-i;
                
                titleForImgInside.text=@"No";
                titleForImgOutside.text=@"Yes";
                
                lblForNoPercentage.text=[NSString stringWithFormat:@"%d%%",(int)i];
                lblForYesPercentage.text=[NSString stringWithFormat:@"%d%%",100-i];
            }
            else
            {
                self.circleProgressView1.elapsedTime=i;
                self.circleProgressView.elapsedTime=100-i;
                
                titleForImgOutside.text=@"No";
                titleForImgInside.text=@"Yes";
                
                lblForYesPercentage.text=[NSString stringWithFormat:@"%d%%",(int)i];
                lblForNoPercentage.text=[NSString stringWithFormat:@"%d%%",100-i];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }];
    
    
    
}

#pragma mark - tableview Delegate and datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArrayForTableView count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ResponseTableViewCell";
    ResponseTableViewCell *cell = (ResponseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"ResponseTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    _PCZ_DetailObj=[dataArrayForTableView objectAtIndex:indexPath.row];
    _PCZUser=_PCZ_DetailObj.user_Info;
    cell.lbl_userName.text=_PCZUser.username;
    cell.lbl_userDiscription.text=_PCZ_DetailObj.userAnswer;
    cell.imgview_userImg.layer.backgroundColor=[[UIColor clearColor] CGColor];
    cell.imgview_userImg.layer.cornerRadius=cell.imgview_userImg.frame.size.height/2;
    cell.imgview_userImg.layer.borderWidth=4.0;
    cell.imgview_userImg.layer.masksToBounds = YES;
    cell.imgview_userImg.layer.borderColor=[[UIColor grayColor] CGColor];
    NSString *str=[self.session timeLeftSinceDate:_PCZ_DetailObj.updatedAt];
    str=[str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    cell.lbl_userTime.text=str;
    
    [cell.imgview_userImg sd_setImageWithURL:[NSURL URLWithString:_PCZUser.userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    return cell;
}

#pragma mark - UITextfield Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationBeginsFromCurrentState:YES];
    //Adjsuting the frames
    if (textField ==self.txtf_because)
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x,-180.0,
                                     self.view.frame.size.width, self.view.frame.size.height);
    }else{
        
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
        self.Chat_View.frame = CGRectMake(20, 110, 270, 160);
        
        self.view_ChatPopUp.frame = CGRectMake(self.view_ChatPopUp.frame.origin.x,320, self.view_ChatPopUp.frame.size.width, self.view_ChatPopUp.frame.size.height);
        ViewFrame=self.Chat_View.frame;
        self.Chat_View.frame = CGRectMake(self.Chat_View.frame.origin.x,100, self.Chat_View.frame.size.width, 185);
        
        self.chatView_chatBtn=[[UIButton alloc]initWithFrame:CGRectMake(225, 133, 40, 30)];
        self.becauseTxtf_Img= [[UIImageView alloc]initWithFrame:CGRectMake(10, 130, 210, 45)];
        self.becauseTxtf_Img.image = [UIImage imageNamed:@"Detail_View_BecauseTxt"];
        [self.Chat_View addSubview:self.becauseTxtf_Img];
        self.chatView_becauseTxtf =[[UITextField alloc]initWithFrame:CGRectMake(25, 133, 95, 30)];
        self.chatView_becauseTxtf.placeholder=@"Because...";
        [self.Chat_View addSubview:self.chatView_becauseTxtf];
        
        [self.chatView_chatBtn setBackgroundImage:[UIImage imageNamed:@"msgsending_img"] forState:UIControlStateNormal];
        
        [self.Chat_View addSubview:self.chatView_chatBtn];
        
    }
    
    [UIView commitAnimations];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.chatView_becauseTxtf.hidden = YES;
    self.chatView_chatBtn.hidden = YES;
    self.becauseTxtf_Img.hidden = YES;
    if (textField ==self.txtf_because)
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,+65,
                                      self.view.frame.size.width, self.view.frame.size.height);
        
    }
    else
    {
        self.view .frame = CGRectMake(self.view.frame.origin.x,0,
                                      self.view.frame.size.width, self.view.frame.size.height);
        self.Chat_View.frame = CGRectMake(self.Chat_View.frame.origin.x, 105, self.Chat_View.frame.size.width, 428);
        
    }
    [UIView commitAnimations];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.submitBtn.hidden=YES;
    self.chatBtn.hidden=YES;
    self.sendBtn.hidden=NO;
    [self.sendBtn setImage:[UIImage imageNamed:@"msgsending_img"] forState:UIControlStateNormal];
    
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.sendBtn.hidden=YES;
    self.chatBtn.hidden=NO;
    [self.chatBtn setImage:[UIImage imageNamed:@"chat_img"] forState:UIControlStateNormal];
    
    return YES;
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark User Selctive type
// submitting user Selective type Yes or No

- (IBAction)YesNoBtnAction:(UIButton *)sender
{
    
    self.submitBtn.hidden = NO;
    [self.submitBtn addTarget:self action:@selector(sendBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [dataArrayForTableView removeAllObjects];
    
    addThreeObjs=3;
    _loadMorePrp.alpha=1.0;
    _loadMorePrp.userInteractionEnabled=YES;
    
    if (sender.tag==0)
    {
        
        btnIndex=0;
        
        [_noBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                             forState:UIControlStateNormal];
        [_yesBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                              forState:UIControlStateNormal];
        
        detailViewObj[@"decision"]=[NSNumber numberWithInt:0];
        
        
        for (int i=0; i<3; i++)
        {
            if (yesDataArray.count>i)
            {
                [dataArrayForTableView addObject:[yesDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
    }
    else
    {
        btnIndex=1;
        
        [_noBtnPrp setBackgroundImage:[UIImage imageNamed:@""]
                             forState:UIControlStateNormal];
        [_yesBtnPrp setBackgroundImage:[UIImage imageNamed:@"checkMark_Img.png"]
                              forState:UIControlStateNormal];
        detailViewObj[@"decision"]=[NSNumber numberWithInt:1];
        for (int i=0; i<3; i++)
        {
            if (noDataArray.count>i)
            {
                [dataArrayForTableView addObject:[noDataArray objectAtIndex:i]];
            }
            else
            {
                _loadMorePrp.alpha=0.5;
                _loadMorePrp.userInteractionEnabled=NO;
            }
        }
        
        
    }
}

//submitting the user decision

- (void)sendBtnClicked
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.view_ChatPopUp.hidden = YES;
    if (_PCZ_DetailObj)
    {
        _PCZ_DetailObj.userAnswer=txtf_because.text;
        if (detailViewObj[@"decision"])
        {
            _PCZ_DetailObj.decision=detailViewObj[@"decision"];
        }
        [_PCZ_DetailObj saveEventually:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                if (detailViewObj[@"decision"])
                {
                    PFQuery *query=[PFQuery queryWithClassName:@"InvitedFriends"];
                    [query whereKey:@"invitedFriends" equalTo:[PFUser currentUser]];
                    [query whereKey:@"piczlyObjectId" equalTo:self.PCZobj];
                    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
                     {
                         object[@"respondToPiczlyQuestion"]=_PCZ_DetailObj.decision;
                         [object saveEventually];
                     }];
                }
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"sucessfully updated" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                [alert show];
                
            }
        }];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else
    {
        
        detailViewObj[@"userAnswer"]=txtf_because.text;
        detailViewObj[@"user_Info"]=[PiczlyUser currentUser];
        detailViewObj[@"piczly_Info"]=self.PCZobj;
        detailViewObj[@"answer_Type"]=self.PCZobj.answerType;
        [detailViewObj saveEventually:^(BOOL succeeded, NSError *error) {
            if(!error)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (detailViewObj[@"decision"])
                {
                    PFQuery *query=[PFQuery queryWithClassName:@"InvitedFriends"];
                    [query whereKey:@"invitedFriends" equalTo:[PFUser currentUser]];
                    [query whereKey:@"piczlyObjectId" equalTo:self.PCZobj];
                    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
                     {
                         object[@"respondToPiczlyQuestion"]=detailViewObj[@"decision"];
                         [object saveEventually];
                         
                     }];
                }
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"piczl-sucessfully sent" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                [alert show];
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"Data not saved please try after sometime" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                [alert show];
            }
        }];
    }
    
    
    
    
}

- (IBAction)sendBtnClicked:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_PCZ_DetailObj)
    {
        _PCZ_DetailObj.userAnswer=txtf_because.text;
        if (detailViewObj[@"decision"])
        {
            _PCZ_DetailObj.decision=detailViewObj[@"decision"];
        }
        [_PCZ_DetailObj saveEventually];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"sucessfully updated" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
        [alert show];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    else
    {
        
        detailViewObj[@"userAnswer"]=txtf_because.text;
        detailViewObj[@"user_Info"]=[PiczlyUser currentUser];
        detailViewObj[@"piczly_Info"]=self.PCZobj;
        detailViewObj[@"answer_Type"]=self.PCZobj.answerType;
        [detailViewObj saveEventually:^(BOOL succeeded, NSError *error) {
            if(!error)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"piczl-sucessfully sent" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                [alert show];
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"info" message:@"Data not saved please try after sometime" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
                [alert show];
            }
        }];
    }
    
    
    
}

#pragma mark -Navigating to Chat View Controller

- (IBAction)chatBtnAction:(id)sender
{
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    vc.piczlyUser=(PiczlyUser*)[PFUser currentUser];
    vc.piczly=self.PCZobj;
    vc.chatcount=chatcount;
    self.navigationController. modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:NO completion:nil];
    vc.view.backgroundColor=[UIColor clearColor];
    
}
- (IBAction)cancelBtn_Clicked:(id)sender
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.view_ChatPopUp.hidden = YES;
}
// displaying chat message count on chat icon
-(void)numofChatMessages
{
    PFQuery *query = [PFQuery queryWithClassName:@"ChatForParticularPiczly"];
    
    [query orderByAscending:@"createdAt"];
    [query whereKey:@"piczlyForChat" equalTo:self.PCZobj];
    [query includeKey:@"piczlyUser"];
    [query includeKey:@"piczlyForChat"];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            // The count request succeeded. Log the count
            
            [self.chatBtn setTitle:[NSString stringWithFormat:@"%d",number] forState:UIControlStateNormal];
            [self.chatBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            chatcount=number;
        }
        
    }];
}

@end
