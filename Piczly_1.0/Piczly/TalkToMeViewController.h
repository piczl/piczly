//
//  TalkToMeViewController.h
//  Piczly
//
//  Created by sekhar on 05/05/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"


@interface TalkToMeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *talkToMeTV;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarFortopicAndPeople;
@property (strong, nonatomic) IBOutlet UIView      *searchBarView;
@property (strong, nonatomic) IBOutlet UIButton    *searchDisplayBtn;
@property (strong, nonatomic) IBOutlet UITextField *serachTxtFld;
@property (strong, nonatomic) IBOutlet UIButton    *searchBtn;

- (IBAction)searchActn:(id)sender;
- (IBAction)searchDisplayActn:(id)sender;

@end
