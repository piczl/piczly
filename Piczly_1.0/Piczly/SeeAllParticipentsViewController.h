//
//  SeeAllParticipentsViewController.h
//  Piczly
//
//  Created by BHANU on 17/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "PiczlyUser.h"
#import "PCZ_Piczly.h"
#import "AppDelegate.h"

@interface SeeAllParticipentsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView   *participentsTV;
@property (strong, nonatomic) NSMutableArray         *participantsArry;

@end
