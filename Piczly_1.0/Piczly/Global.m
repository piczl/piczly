////
////  Global.m
////  Breakk
////
////  Created by Skynet on 23/08/14.
////  Copyright (c) 2014 Umesh Naidu. All rights reserved.
////
//
//#import "Global.h"
//
//@implementation Global
//+(UIColor *)profileListNameColor
//{
//    return  [UIColor colorWithRed:0.113 green:0.576 blue:0.80 alpha:1.0];
//}
//+(UIColor *)profileListPaymentColor
//{
//    return  [UIColor colorWithRed:0.180 green:0.69 blue:0.29 alpha:1.0];
//}
//+(UILabel *)setPageTitle:(NSString *)labelName
//{
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont fontWithName:@"AvenirNext-Medium" size:17];//Avenir-Medium
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];//[UIColor colorWithRed:0.266 green:0.276 blue:0.45 alpha:1];
//    label.text = labelName;
//    [label sizeToFit];
//    return label;
//}
//
//+(UIColor *)getSegmentUnselectedClr
//{
//    return  [UIColor colorWithRed:0.4 green:0.74 blue:0.88 alpha:1.0];
//}
//+(UIColor *)getSegmentselectedClr
//{
//    return  [UIColor colorWithRed:0.1 green:0.57 blue:0.79 alpha:1.0];
//}
///*************************************************************
// Author : Umesh Naidu.C
// Desc.  : This function is for displaying alert in all class
// Input  : NSString
// Return : Vaue for the Key
// *************************************************************/
//+(void)alertDisplay:(NSString *)alertTxt
//{
//    UIAlertView *alerts=[[UIAlertView alloc]initWithTitle:@"Info" message:alertTxt delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alerts show];
//}
///************************************************************************
// Author : Umesh
// Desc : Checking Email Validation
// ***********************************************************************/
//+(BOOL)emailValidation:(NSString *)emailTxt
//{
//    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
//    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
//    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:emailTxt];
//}
//+(void)networkAlert
//{
//    [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:NO];
//}
//+(void)showAlert
//{
//    UIAlertView *myAlert = [[UIAlertView alloc]
//                            initWithTitle:@"No Internet connection"   message:@"You require an Internet Connection via WiFi or Cellular Network"
//                            delegate:self
//                            cancelButtonTitle:@" Ok"
//                            otherButtonTitles:nil];
//    [myAlert show];
//}
//
//+(NSString *)convertDateToString :(NSDate *)inputDate
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//    NSString *stringDate = [dateFormatter stringFromDate:inputDate];
//    return stringDate;
//}
//
//@end
