//
//  Session.m
//  CircularProgressControl
//
//  Created by Carlos Eduardo Arantes Ferreira on 22/11/14.
//  Copyright (c) 2014 Mobistart. All rights reserved.
//

#import "Session.h"

@implementation Session

- (NSTimeInterval)progressTime {
    
    if (_finishDate) {
        return [_finishDate timeIntervalSinceDate:self.startDate];
    }
    else {
        return [[NSDate date] timeIntervalSinceDate:self.startDate];
    }
}

-(NSString*) timeLeftSinceDate :(NSDate *)dateT
{
    NSString *timeLeft;
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dateT];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds += days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds += hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds += minutes * 60;
    
    if(days) {
        timeLeft = [NSString stringWithFormat:@"%ld DAY", (long)days*-1];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld HOURS", (long)hours*-1];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld MIN", (long)minutes*-1];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld SEC", (long)seconds*-1];
    
    return timeLeft;
}

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
