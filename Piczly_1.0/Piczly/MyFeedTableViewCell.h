//
//  MyFeedTableViewCell.h
//  Piczly
//
//  Created by sekhar on 30/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyFeedTableViewDelegate;

@interface MyFeedTableViewCell : UITableViewCell

@property (nonatomic, strong) id <MyFeedTableViewDelegate>MyFeedDelegate;

@property (weak, nonatomic)   IBOutlet UIButton    *shareBtnPrp;
@property (weak, nonatomic)   IBOutlet UILabel     *itemTopicName;
@property (weak, nonatomic)   IBOutlet UILabel     *userProfileName;
@property (weak, nonatomic)   IBOutlet UILabel     *itemQuestion;
@property (weak, nonatomic)   IBOutlet UILabel     *itemChoice;
@property (weak, nonatomic)   IBOutlet UIButton    *optionsBtnPrp;
@property (strong, nonatomic) IBOutlet UIButton    *editPiczBtnPrp;
@property (weak, nonatomic)   IBOutlet UIButton    *inviteMorePeoplePrp;
@property (weak, nonatomic)   IBOutlet UIButton    *seeAllPaticipanrtsPrp;
@property (weak, nonatomic)   IBOutlet UIButton    *createNewPiczlyBasedOnItPrp;
@property (weak, nonatomic)   IBOutlet UIButton    *createPiczlyWithinTheTopic;
@property (weak, nonatomic)   IBOutlet UIButton    *deletePiczlyPrp;
@property (strong, nonatomic) IBOutlet UILabel     *noOfPersonsRespondedToQuestion;
@property (strong, nonatomic) IBOutlet UILabel     *noOfTopicWithThatPiczl;
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic)   IBOutlet UIImageView *itemImage;
@property (weak, nonatomic)   IBOutlet UILabel     *itemStatus;
@property (strong, nonatomic) IBOutlet UIView      *dropDownBtnView;


- (IBAction)shareBtnAction:(id)sender;
- (IBAction)optionBtnAction:(id)sender;
- (IBAction)editPiczlBtnAction:(id)sender;
- (IBAction)inviteMorePeopleBtnAction:(id)sender;
- (IBAction)seeAllParticipantsBtnAction:(id)sender;
- (IBAction)createNewPiczlBasedOnItBtnAction:(id)sender;
- (IBAction)CreatePiczlyWithSameTopicBtnAction:(id)sender;
- (IBAction)deletePiczlyBtnAction:(id)sender;



@end

//protocal delegate methods for drop down actions

@protocol MyFeedTableViewDelegate <NSObject>

-(void)cellDidTapOptionFeedBtn:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapCreateOnThatTopic:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapCreateNewPiczlBasedOnIt:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnSeeAllParticipants:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapInviteMorePeopleBtn:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnDeleteBtn:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnDropDownBtn:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;
-(void)cellDidTapOnShareBtn:(MyFeedTableViewCell*)myCell btnIndex:(int)myBtn;



@end

