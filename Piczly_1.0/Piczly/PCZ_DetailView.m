//
//  PCZ_DetailView.m
//  Piczly
//
//  Created by Admin on 14/03/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "PCZ_DetailView.h"

@implementation PCZ_DetailView

@dynamic user_Info;
@dynamic decision;
@dynamic userAnswer;
@dynamic piczly_Info;
@dynamic answer_Type;


+(NSString*)parseClassName
{
    return @"DetailView";
}

@end
