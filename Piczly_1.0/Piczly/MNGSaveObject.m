/**********************************************************************************************
 Author      : Umesh Naidu.C
 Description : MNGSaveObject class
 Date Created: 16/01/13.
 Copyright (c) 2013 Mangohealthcare. All rights reserved.
 ***********************************************************************************************/

#import "MNGSaveObject.h"

@implementation MNGSaveObject

/************************************************************* 
 Author : Umesh
 Desc.  : This function stores NSString local disk with specified key. 
 Input  : Key and NSString
 Return : void
 *************************************************************/
+(void)saveToUserDefaults:(NSString*)key :(NSString *)value  
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];	
    if (standardUserDefaults) 
    {
        [standardUserDefaults setObject:value forKey: key ];
        [standardUserDefaults synchronize];
    }	
}
/************************************************************* 
 Author : Umesh
 Desc.  : This function stores Arraysinto local disk with specified key. 
 Input  : Key and NSMutableArray
 Return : void
 *************************************************************/
+(void)saveArrayToUserDefaults:(NSString *)key :(NSMutableArray *)value  
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:value] forKey:key];
}
/************************************************************* 
 Author : parvin
 Desc.  : This function stores dicts into local disk with specified key. 
 Input  : Key and NSMutableDictionary
 Return : void
 *************************************************************/
+(void)saveDictionaryToUserDefaults:(NSString *)key :(NSMutableDictionary *)value  
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:value] forKey:key];
}

/************************************************************* 
 Author : Umesh
 Desc.  : This function Returns NSSTRING from local disk with specified key. 
 Input  : Key 
 Return :  NSString
 *************************************************************/
+(NSString*)retrieveFromUserDefaults : (NSString *) key
{
	int flag=0;
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
	
	@try 
	{
        
		if (standardUserDefaults) 
		{
			val = [standardUserDefaults objectForKey: key ];
			flag=1;
		}
	}
    @catch (NSException *exception) 
    {
        flag = 0;
        [NSException exceptionWithName:exception.name reason:exception.reason
                              userInfo: nil];
        @throw exception;
    }
	@finally 
	{
		if (flag!=1) 
		{
			val=nil;
		}	
	}
	return val;
}
/************************************************************* 
 Author : Umesh
 Desc.  : This function Returns Arrays from local disk with specified key. 
 Input  : Key 
 Return :  NSMutableArray
 *************************************************************/
+(NSMutableArray *)retrieveArrayFromUserDefaults : (NSString *) key
{
	NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:key];
    NSMutableArray *oldSavedArray;
    if (dataRepresentingSavedArray != nil)
    {
        oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
    }
    return oldSavedArray;
}
/************************************************************* 
 Author : parvin
 Desc.  : This function Returns dicts from local disk with specified key. 
 Input  : Key 
 Return :  NSMutableDictionary
 *************************************************************/
+(NSMutableDictionary *)retrieveDictFromUserDefaults : (NSString *) key
{
	NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:key];
    NSMutableDictionary *oldSavedDict;
    if (dataRepresentingSavedDict != nil)
    {
        oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
    }
    return oldSavedDict;
}
@end
