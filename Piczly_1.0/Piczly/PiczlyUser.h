//
//  PiczlyUser.h
//  
//
//  Created by BHANU on 16/02/15.
//
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@interface PiczlyUser : PFUser <PFSubclassing>

@property (nonatomic, strong) NSString   *userName;
@property (nonatomic, strong) NSString   *email;
@property (nonatomic, strong) PFFile     *userImageFile;
@property (nonatomic, strong) NSString   *location;
@property (nonatomic, strong) NSString   *firstName;
@property (nonatomic, strong) NSString   *lastName;
@property (nonatomic, strong) NSString   *displayName;
@property (nonatomic, strong) NSString   *password;

@property (nonatomic, strong) PFRelation *friends;

@end
