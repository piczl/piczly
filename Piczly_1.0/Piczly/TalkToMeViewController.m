//
//  TalkToMeViewController.m
//  Piczly
//
//  Created by sekhar on 05/05/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "TalkToMeViewController.h"
#import "SWRevealViewController.h"
#import "PiczlyUser.h"
#import "PCZ_Piczly.h"
#import "PCZ_Chat.h"
#import "TalkToMeCell.h"



@interface TalkToMeViewController ()
{
    NSMutableArray *usersArry,*piczlyarry,*filteredPiczlyArray,*userfilteredArray;
    
    BOOL searchingFlag;
    NSString *_tempStrForSearchText;
    NSInteger dex;
}

@end

@implementation TalkToMeViewController

#pragma mark View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:5.0/255.0 green:53.0/255.0 blue:101.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    UIImage* logoImage = [UIImage imageNamed:@"talktomelogo"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    
    
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu_btn"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = barBtn;
    filteredPiczlyArray=[NSMutableArray arrayWithCapacity:[piczlyarry count]];
    userfilteredArray=[NSMutableArray arrayWithCapacity:[usersArry count]];
    usersArry =[[NSMutableArray alloc]init];
    piczlyarry=[[NSMutableArray alloc]init];
    [_serachTxtFld setValue:[UIColor blackColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    
    
    _serachTxtFld.delegate=self;
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, 54.0f, _searchBarView.frame.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [_searchBarView.layer addSublayer:bottomBorder];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self listOfPersonsWhoSendALastmessage];
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark Parsing

-(void)listOfPersonsWhoSendALastmessage
{
    // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *querForAllPiczlysCreatedByCurrentUser=[PFQuery queryWithClassName:@"Piczly"];
    
    [querForAllPiczlysCreatedByCurrentUser whereKey:@"createdBy" equalTo:[PFUser currentUser]];
    [querForAllPiczlysCreatedByCurrentUser whereKey:@"piczlyStatus" equalTo:[NSNumber numberWithInt:1]];
    [querForAllPiczlysCreatedByCurrentUser includeKey:@"createdBy"];
    NSArray *piczlysArry= (NSArray*)[querForAllPiczlysCreatedByCurrentUser findObjects];
    if (piczlysArry.count>0)
    {
        for (int i=0; i<piczlysArry.count; i++)
        {
            PFQuery *queryForChatList=[PFQuery queryWithClassName:@"ChatForParticularPiczly"];
            PCZ_Piczly *piczly=[piczlysArry objectAtIndex:i];
            [queryForChatList whereKey:@"piczlyForChat" equalTo:piczly];
            [queryForChatList orderByDescending:@"updatedAt"];
            [queryForChatList includeKey:@"piczlyUser"];
            
            PCZ_Chat *chatMessage= (PCZ_Chat*)[queryForChatList getFirstObject];
            if (chatMessage)
            {
                PFQuery *queryForChatList=[PFQuery queryWithClassName:@"ChatForParticularPiczly"];
                [queryForChatList whereKey:@"piczlyForChat" equalTo:[piczlysArry objectAtIndex:i] ];
                [queryForChatList includeKey:@"piczlyUser"];
                [queryForChatList whereKey:@"piczlyUser" equalTo:[chatMessage valueForKey:@"piczlyUser"]];
                NSArray *countMessages=(NSArray*)    [queryForChatList findObjects];
                [usersArry addObject:countMessages];
                [piczlyarry addObject:piczly];
                
                
            }
            if (i+1==piczlysArry.count)
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.talkToMeTV reloadData];
            }
        }
        
    }
    else
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}
#pragma mark
#pragma mark Tableview datasource  methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(searchingFlag==YES)
    {
        return [filteredPiczlyArray count];
    }
    else
        
        return piczlyarry.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier=@"talkTVC";
    TalkToMeCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell==nil)
    {
        cell=[[TalkToMeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    PiczlyUser *user;
    
    PCZ_Piczly *piczly;
    if (searchingFlag==YES)
    {
        piczly=[filteredPiczlyArray objectAtIndex:indexPath.row];
        [cell.noOfChatMsgsBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[userfilteredArray objectAtIndex:indexPath.row] count]] forState:UIControlStateNormal];
        user=[[[userfilteredArray objectAtIndex:indexPath.row] valueForKey:@"piczlyUser"] objectAtIndex:0];
        
        
    }
    else
    {
        piczly=[piczlyarry objectAtIndex:indexPath.row];
        [cell.noOfChatMsgsBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[usersArry objectAtIndex:indexPath.row] count]] forState:UIControlStateNormal];
        user=[[[usersArry objectAtIndex:indexPath.row] valueForKey:@"piczlyUser"] objectAtIndex:0];
        
        
    }
    cell.topicNameLbl.text=[piczly valueForKey:@"topic"] ;
    
   
    if (user.userImageFile)
    {
        PFFile *fileForProfileImage=user.userImageFile;
        [fileForProfileImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
         {
             cell.userProfileImgView.image=[UIImage imageWithData:data];
             cell.userProfileImgView.layer.backgroundColor=[[UIColor clearColor] CGColor];
             cell.userProfileImgView.layer.cornerRadius=cell.userProfileImgView.frame.size.height/2;
             cell.userProfileImgView.layer.borderWidth=6.0;
             cell.userProfileImgView.layer.masksToBounds = YES;
             cell.userProfileImgView.layer.borderColor=[[UIColor whiteColor] CGColor];
         }];
    }
    
    return cell;
    
}

//search bar view

- (IBAction)searchDisplayActn:(id)sender
{
    if (_searchBarView.hidden)
    {
        _searchBarView.hidden=NO;
        searchingFlag = NO;
        [_talkToMeTV reloadData];
    }
    else
    {
        _searchBarView.hidden=YES;
        [_talkToMeTV reloadData];
        
    }
}
#pragma mark- SearchBar Delegate Methods


- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    [_searchBarFortopicAndPeople setShowsCancelButton:NO animated:YES];
    if(!searchingFlag==YES)
        return;
    searchingFlag = YES;
    
    if (theSearchBar.text.length==0) {
        [_talkToMeTV reloadData];
    }
    
}

//search bar text change - typing text in search bar
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    
    if([searchText length] > 0)
    {
        searchingFlag = YES;
        [_talkToMeTV setScrollEnabled:YES];
        [self searchDiscussions:searchText];
    }
    else
    {
        searchingFlag = NO;
        [_talkToMeTV reloadData];
    }
    
    
    
    
}
// search results for user name or topic name

- (void)searchDiscussions:(NSString *)searchBarText
{
    
    
    [filteredPiczlyArray removeAllObjects];
    [userfilteredArray removeAllObjects];
    for(NSDictionary *piczly in piczlyarry)
    {
        NSString *topicname = [piczly objectForKey:@"topic"];
        
        NSRange range = [topicname rangeOfString:searchBarText options:NSCaseInsensitiveSearch];
        if(range.location != NSNotFound)
        {
            [filteredPiczlyArray addObject:piczly];
            dex =[piczlyarry indexOfObject:piczly];
            
            [userfilteredArray addObject:[usersArry objectAtIndex:dex]];
            
        }
        
        
    }
    if(filteredPiczlyArray.count<=0)
    {
        
        
        for (NSDictionary *users in usersArry)
        {
            PiczlyUser *user =[[users  valueForKey:@"piczlyUser"] objectAtIndex:0] ;
            NSString *userName=[user objectForKey:@"username"] ;
            NSRange range = [userName rangeOfString:searchBarText options:NSCaseInsensitiveSearch];
            if(range.location != NSNotFound)
            {;
                [userfilteredArray addObject:users];
                dex =[usersArry indexOfObject:users];
                [filteredPiczlyArray addObject:[piczlyarry objectAtIndex:dex]];
                
                
            }
            
        }
    }
    
    
    
    [_searchBarFortopicAndPeople resignFirstResponder];
    [_talkToMeTV reloadData];
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    _tempStrForSearchText=searchBar.text;
    if (_tempStrForSearchText.length==0) {
        searchingFlag=NO;
        [_talkToMeTV reloadData];
    }
    [self performSelector:@selector(searchBarDisappear) withObject:nil afterDelay:5.0];
}

//search bar search button click call
- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [_searchBarFortopicAndPeople resignFirstResponder];
    [_searchBarView setHidden:YES];
    [self searchDiscussions:theSearchBar.text];
}
-(void)searchBarDisappear
{
    [_searchBarFortopicAndPeople resignFirstResponder];
    [_searchBarView setHidden:YES];
    
}

//search using

- (IBAction)searchActn:(id)sender
{
    if([_serachTxtFld.text length] > 0)
    {
        searchingFlag = YES;
        [_talkToMeTV setScrollEnabled:YES];
        [self searchDiscussions:_serachTxtFld.text];
        [self.view endEditing:YES];
        _searchBarView.hidden=YES;
        _serachTxtFld.text=@"";
      
    }
    else
    {
        searchingFlag = NO;
    
        [_talkToMeTV reloadData];
        
        Com_AlertView(@"", @"Enter TopicName or UserName");
        
    }
    
}
#pragma mark
#pragma mark TextField Delegate Methods
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _tempStrForSearchText=textField.text;
    if (_tempStrForSearchText.length==0)
    {
        searchingFlag=NO;
        [_talkToMeTV reloadData];
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [_serachTxtFld resignFirstResponder];
}

@end
