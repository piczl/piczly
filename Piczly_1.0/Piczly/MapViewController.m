//
//  MapViewController.m
//  Piczly
//
//  Created by sekhar on 16/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

#pragma mark ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self seePiczlyLocationOnMap];
    
    
    // Do any additional setup after loading the view.
}
#pragma mark Memory Mangaement

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CL-Location

-(void)seePiczlyLocationOnMap

{
    CLLocationCoordinate2D coord = {.latitude = _latitude, .longitude =  _longitude};
    MKCoordinateSpan span = {.latitudeDelta =  0.2, .longitudeDelta =  0.2};
    MKCoordinateRegion region = {coord, span};
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = coord;
    self.navigationController.title=@"Piczly Location";
    [self.mapView addAnnotation:annotationPoint];
    [self.mapView setRegion:region];
    [self.mapView regionThatFits:region];
}


@end
