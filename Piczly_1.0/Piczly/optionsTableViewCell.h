//
//  optionsTableViewCell.h
//  Piczly
//
//  Created by sekhar on 23/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol optionsTableViewCellDelegate;


@interface optionsTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic)  IBOutlet UITextField *optionsTF;
@property (weak, nonatomic)  IBOutlet UILabel     *optionLabel;
@property (nonatomic,strong) NSMutableArray       *stringArray;
@property (nonatomic,strong) NSMutableDictionary  *stringDic;
@property (weak, nonatomic)  IBOutlet UIButton    *optionsRemoveBtn;
@property (nonatomic , strong)id <optionsTableViewCellDelegate> delegate;


- (IBAction)optionsRemoveBtnAction:(UIButton *)sender;
@end

@protocol optionsTableViewCellDelegate <NSObject>

-(void)removeBtnPressed:(optionsTableViewCell*)myCell value:(int)myValue button:(UIButton*)btn;
-(void)getTexfieldIndexValue:(int)tfTag;
-(void)getValuesOfTableView:(NSString*)dic andTagValue:(int)tag;
@end

