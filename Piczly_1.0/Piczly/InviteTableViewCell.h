//
//  InviteTableViewCell.h
//  Piczly
//
//  Created by Admin on 12/03/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InviteTableViewCellDelegate;

@interface InviteTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *facebookFriendsImage;
@property (strong, nonatomic) IBOutlet UILabel     *facebookFriendsNames;
@property (strong, nonatomic) IBOutlet UIButton    *chekBtn;
@property (nonatomic, strong) id<InviteTableViewCellDelegate>delegateForInvite;

- (IBAction)didSelectCheckMarkBtn:(UIButton *)sender;

@end


@protocol InviteTableViewCellDelegate <NSObject>

-(void)cellDidTapCheckedBtn:(InviteTableViewCell*)myCell  isSelected:(BOOL)isSelected;

@end