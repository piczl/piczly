//
//  TalkToMeCell.h
//  Piczly
//
//  Created by sekhar on 05/05/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TalkToMeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImgView;
@property (strong, nonatomic) IBOutlet UILabel *topicNameLbl;
@property (strong, nonatomic) IBOutlet UIButton *noOfChatMsgsBtn;

@end
