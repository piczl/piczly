//
//  InviteTableViewCell.m
//  Piczly
//
//  Created by Admin on 12/03/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "InviteTableViewCell.h"

@implementation InviteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (IBAction)didSelectCheckMarkBtn:(UIButton *)sender {
    sender.selected=!sender.selected;
    
    if (_delegateForInvite && [_delegateForInvite respondsToSelector:@selector(cellDidTapCheckedBtn:isSelected:)])
    {
        [_delegateForInvite cellDidTapCheckedBtn:self isSelected:sender.selected];
    }
}
@end
