//
//  StringUtils.m
//  LytePole
//
//  Created by Umesh Naidu on 09/09/14.
//  Copyright (c) 2014 Umesh Naidu. All rights reserved.
//

#import "StringUtils.h"

static StringUtils *sharedGlobals = nil;

@implementation StringUtils

@synthesize manditoryStr;
@synthesize emailInvaidStr;
@synthesize passwordsMatch;

#pragma mark -
#pragma mark Singleton Methods

+ (StringUtils *)sharedGlobals {
	
	if(sharedGlobals == nil)
	{
		sharedGlobals = [[super allocWithZone:NULL] init];
	}
	
	return sharedGlobals;
}
+ (id)allocWithZone:(NSZone *)zone
{
    //	return [self sharedManager];
    return nil;
}
/*************************************************************
 Author : Umesh Naidu.C
 Desc.  : This function is for assigning strings to alerts
 Input  : nil
 Return : nil
 *************************************************************/
- (id)init
{
    if (self = [super init])
    {
        manditoryStr = @"Please enter proper username and password";
        emailInvaidStr = @"Invalid email, please enter vaild email address";
        passwordsMatch = @"Passwords not matched";
    }
    return self;
}


@end
