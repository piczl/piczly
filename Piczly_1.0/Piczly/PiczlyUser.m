//
//  PiczlyUser.m
//
//
//  Created by BHANU on 16/02/15.
//
//

#import "PiczlyUser.h"

@implementation PiczlyUser

@dynamic userName;
@dynamic firstName;
@dynamic lastName;
@dynamic email;
@dynamic displayName;
@dynamic password;
@dynamic location;
@dynamic userImageFile;


@synthesize friends = _friends;



+ (PiczlyUser *)currentUser {
    return (PiczlyUser *)[PFUser currentUser];
}

- (PFRelation *)friends
{
    if (!_friends) _friends = [self relationForKey:@"friends"];
    return _friends;
}


-(BOOL)isEqual:(PiczlyUser*)otherObject
{
    if ([[self objectId] isEqualToString:[otherObject objectId]])
        return YES;
    
    return NO;
}

@end
