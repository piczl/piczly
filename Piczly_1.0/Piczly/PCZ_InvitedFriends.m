//
//  PCZ_InvitedFriends.m
//  Piczly
//
//  Created by BHANU on 15/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "PCZ_InvitedFriends.h"

@implementation PCZ_InvitedFriends
@dynamic invitedFriends;
@dynamic piczlyObjectId;
@dynamic AcceptanceStatus;

+(NSString*)parseClassName;
{
    return @"InvitedFriends";
}
@end
