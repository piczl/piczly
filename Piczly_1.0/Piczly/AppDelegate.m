//
//  AppDelegate.m
//  Piczly
//
//  Created by sekhar on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Parse/Parse.h>
#import "PiczlyUser.h"
#import "PCZLoginViewController.h"
#import "PCZ_Piczly.h"
#import "PCZ_topics.h"
#import "PCZ_DetailView.h"
#import "PCZ_InvitedFriends.h"
#import "PCZ_Chat.h"

@interface AppDelegate ()

@property (strong, nonatomic) PCZLoginViewController *PCZLoginVC;
@end
@implementation AppDelegate
@synthesize appDelegate;
+(AppDelegate *)getInstance
{
    return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [PiczlyUser registerSubclass];
    [PCZ_Piczly registerSubclass];
    [PCZ_topics registerSubclass];
    [PCZ_InvitedFriends registerSubclass];
    [PCZ_DetailView registerSubclass];
    [PCZ_Chat registerSubclass];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Override point for customization after application launch.
    
    [Parse setApplicationId:@"Qri1KwhLQJK8rUSpd0uL84gg1vxI2eez21Hh0tjP" clientKey:@"Pf87BQahdRLyRCu40zXNrIBPmWxm5BpZKOjXXr3Z"];
    
    
    [FBSettings setDefaultAppID:@"593599354105270"];
    
    [PFFacebookUtils initializeFacebook];
    
    [FBProfilePictureView class];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [PFUser logOut];
    [FBSession.activeSession close];
    
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:[PFFacebookUtils session]];
}





@end
