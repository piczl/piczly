//
//  AppDelegate.h
//  Piczly
//
//  Created by sekhar on 16/02/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCZLoginViewController.h"
#import "PCZHomeViewController.h"
#import "TalkToMeViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow              *window;
@property (nonatomic,strong) id                     appDelegate;
@property  int                                      btnTagValue;
@property (strong, nonatomic) UIStoryboard          *storyboard;
@property (strong, nonatomic) PCZHomeViewController *PCZHomeVC;

@property (strong, nonatomic) UINavigationController *navigationController;

+ (AppDelegate *)getInstance;


@end

