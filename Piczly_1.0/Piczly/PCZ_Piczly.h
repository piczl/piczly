//
//  PCZ_Piczly.h
//  Piczly
//
//  Created by Sai kumar Gourishetty on 2/25/15.
//  Copyright (c) 2015 betabulls. All rights reserved.
//

#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import "PiczlyUser.h"

@class PiczlyUser; //*createdBy;

@interface PCZ_Piczly : PFObject <PFSubclassing>

@property (nonatomic , strong) NSString   *topic;
@property (nonatomic , strong) PFFile     *image;
@property (nonatomic , strong) NSString   *latitude;
@property (nonatomic , strong) NSString   *longitude;
@property (nonatomic , strong) NSString   *question;
@property (nonatomic , strong) NSNumber   *answerType;
@property (nonatomic , strong) NSNumber   *piczlyStatus;
@property (nonatomic , strong) NSNumber   *invitedPeopleCount;
@property BOOL yes_no_options;
@property (nonatomic , strong) NSString   *a_b_c_d_options;
@property (nonatomic , strong) NSString   *optionA;
@property (nonatomic , strong) NSString   *optionB;
@property (nonatomic , strong) NSString   *optionC;
@property (nonatomic , strong) NSString   *optionD;
@property (nonatomic , strong) NSString   *createdDate;
@property (nonatomic , strong) NSNumber   *topicCount;
@property (nonatomic , strong) PiczlyUser *createdBy;


+(NSString*)parseClassName;
@end
