//
//  MenuViewController.m
//  Piczly
//
//  Created by BHANU on 27/03/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "ImageViewCell.h"
#import "ItemCell.h"
#import "MyFeedViewController.h"
#import "PCZLoginViewController.h"
#import "PCZHomeViewController.h"
#import "Session.h"
#import "TalkToMeViewController.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"


@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataArray =[[NSMutableArray alloc]initWithObjects:@"News Feed",@"Create a Piczl",@"Talk To Me",@"My Topics",@"My Connections",@"Notifications",@"My Events", nil];
    self.settingsArray=[[NSMutableArray alloc]initWithObjects:@"General",@"privacy",@"Notifications",@"Support/Feedback", nil];
    self.itemImage=[[NSMutableArray alloc]initWithObjects:@"Menu_NewsFeedBtn",@"Menu_CreateBtn",@"talkToMeImg",@"Menu_MyTopicsBtn",@"Menu_MyConnectionsBtn",@"Menu_NotificationsBtn",@"Menu_EventsBtn", nil];
    
    self.settingImage=[[NSMutableArray alloc]initWithObjects:@"Menu_GeneralBtn",@"Menu_PrivacyBtn",@"Menu_NotificationGeneralBtn",@"Menu_SupportFeedbackBtn", nil];
    
    
    
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0)
    {
        return 1;
        
    }
    else if (section == 1)
    {
        return [self.dataArray count];
    }
    else if (section == 2)
    {
        
        return [self.settingsArray count];
    }
    
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"imgCell";
    static NSString *CellIdentifier2 =@"itemCell";
    
    if (indexPath.section == 0)
    {
        if (indexPath.row  == 0)
        {
            ImageViewCell *cell = nil;
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1 forIndexPath:indexPath];
            if (cell == nil)
            {
                cell = [[ImageViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            cell.imgView.image=[UIImage imageNamed:@"images.jpeg"];
            
            cell.imgView.layer.backgroundColor=[[UIColor clearColor] CGColor];
            cell.imgView.layer.cornerRadius=cell.imgView.frame.size.height/2;
            cell.imgView.layer.borderWidth=6.0;
            cell.imgView.layer.masksToBounds = YES;
            cell.imgView.layer.borderColor=[[UIColor whiteColor] CGColor];
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            cell.leftBluredImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
            cell.leftBluredImage.layer.cornerRadius=cell.leftBluredImage.frame.size.height/2;
            cell.leftBluredImage.layer.borderWidth=6.0;
            cell.leftBluredImage.layer.masksToBounds = YES;
            cell.leftBluredImage.layer.borderColor=[[UIColor whiteColor] CGColor];
            [cell.leftBluredImage sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            cell.rightBluredImage.layer.backgroundColor=[[UIColor clearColor] CGColor];
            cell.rightBluredImage.layer.cornerRadius=cell.imgView.frame.size.height/2;
            cell.rightBluredImage.layer.borderWidth=6.0;
            cell.rightBluredImage.layer.masksToBounds = YES;
            cell.rightBluredImage.layer.borderColor=[[UIColor whiteColor] CGColor];
            [cell.rightBluredImage sd_setImageWithURL:[NSURL URLWithString:[PiczlyUser currentUser].userImageFile.url] placeholderImage:[UIImage imageNamed:@"profile_bg"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            
            cell.profileName.text=[PiczlyUser currentUser].username;
            
            
            cell.imgView.userInteractionEnabled = YES;
            cell.imgView.tag = indexPath.row;
            cell.leftBluredImage.userInteractionEnabled = YES;
            cell.leftBluredImage.tag = indexPath.row;
            cell.rightBluredImage.userInteractionEnabled = YES;
            cell.rightBluredImage.tag = indexPath.row;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myFunction:)];
            tapped.numberOfTapsRequired = 1;
            [cell.imgView addGestureRecognizer:tapped];
            UITapGestureRecognizer *tapped1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myFunction:)];
            tapped.numberOfTapsRequired = 1;
            [cell.leftBluredImage addGestureRecognizer:tapped1];
            UITapGestureRecognizer *tapped2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myFunction:)];
            tapped.numberOfTapsRequired = 1;
            [cell.rightBluredImage addGestureRecognizer:tapped2];
            
            
            
            
            
            return cell;
        }}
    
    
    ItemCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2 forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[ItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
    }
    
    
    else if (indexPath.section==1)
    {
        cell.menuLbl.text = [self.dataArray objectAtIndex:indexPath.row];
        cell.itemImg.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.itemImg.layer.cornerRadius=cell.itemImg.frame.size.height/2;
        cell.itemImg.layer.borderWidth=1.0;
        cell.itemImg.layer.masksToBounds = YES;
        cell.itemImg.layer.borderColor=[[UIColor whiteColor] CGColor];
        cell.itemImg.image=[UIImage imageNamed:[self.itemImage objectAtIndex:indexPath.row]];
        
        
        return cell;
    }
    else if (indexPath.section==2)
    {
        cell.menuLbl.text = [self.settingsArray objectAtIndex:indexPath.row];
        cell.itemImg.image=[UIImage imageNamed:[self.settingImage objectAtIndex:indexPath.row]];
        return cell;
    }
    else if(indexPath.section==3)
    {
        cell.menuLbl.text = @"Log Out";
        
        cell.itemImg.layer.backgroundColor=[[UIColor clearColor] CGColor];
        cell.itemImg.layer.cornerRadius=cell.itemImg.frame.size.height/2;
        cell.itemImg.layer.borderWidth=1.0;
        cell.itemImg.layer.masksToBounds = YES;
        cell.itemImg.layer.borderColor=[[UIColor whiteColor] CGColor];
        cell.itemImg.image=[UIImage imageNamed:@"Menu_LogoutBtn"];
        cell.detailImg.hidden=YES;
        return cell;
    }
    
    return 0;
}

-(void)myFunction :(id) sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    
    MyFeedViewController *feed=[self.storyboard instantiateViewControllerWithIdentifier:@"myfeed"];
    UINavigationController *navController = AppDelegate.getInstance.navigationController;
    navController = [[UINavigationController alloc] initWithRootViewController:feed];
    [self.revealViewController pushFrontViewController:navController animated:YES];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        if (indexPath.row ==0)
        {
            return 200;
        }}
    else
    {
        return 65;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    NSString *title;
    
    if(section ==2)
    {
        title=@"Settings";
    }
    return title;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==1)
    {
        return 0;
    }else if (section ==2)
    {
        return 50;
    }else if(section ==3)
    {
        return 60;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section==3)
    {
        
        // create the parent view that will hold header Label
        UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 30.0, 300.0, 44.0)];
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectZero];
        img.frame=CGRectMake(10.0, 20.0, 30, 30);
        img.image=[UIImage imageNamed:@"Menu_LogoutBtn"];
        [customView addSubview:img];
        UIButton * headerBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        headerBtn.opaque = NO;
        [headerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        headerBtn.frame = CGRectMake(45.0, 20.0, 100.0, 30.0);
        [headerBtn setTitle:@"Log Out" forState:UIControlStateNormal];
        [headerBtn addTarget:self action:@selector(LogOutActn:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:headerBtn];
        
        return customView;
        
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            
        }
    }
    else if (indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            PCZHomeViewController  *HomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            UINavigationController *navController = AppDelegate.getInstance.navigationController;
            navController = [[UINavigationController alloc] initWithRootViewController:HomeVC];
            
            [self.revealViewController pushFrontViewController:navController animated:YES];
            
        }
        else if(indexPath.row==1)
        {
            CreatePiczlyViewController *createVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];
            createVC.menuNavStr=@"createPiczlFromMenu";
            [self.navigationController pushViewController:createVC animated:YES];
            
        }
        else   if(indexPath.row==2)
        {
            
            
            TalkToMeViewController  *talkVC=[self.storyboard instantiateViewControllerWithIdentifier:@"TalkVC"];
            UINavigationController *navController = AppDelegate.getInstance.navigationController;
            navController = [[UINavigationController alloc] initWithRootViewController:talkVC];
            
            [self.revealViewController pushFrontViewController:navController animated:YES];
            
        }
        
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    
}

#pragma mark LogOut


-(void)LogOutActn:(id)sender
{
    UIAlertController *projectSelector = [UIAlertController alertControllerWithTitle:@"You will be logged out. Continue?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil]];
    [projectSelector addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    [PFUser logOut];
                                    [[FBSession activeSession] close];
                                    [[FBSession activeSession] closeAndClearTokenInformation];
                                    [[PFFacebookUtils session] close];
                                    [[PFFacebookUtils session] closeAndClearTokenInformation];
                                    UINavigationController *navController = AppDelegate.getInstance.navigationController;
                                    
                                    PCZLoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                    navController = [[UINavigationController alloc]initWithRootViewController:vc];
                                    AppDelegate.getInstance.window.rootViewController = navController;
                                }]];
    
    [self presentViewController:projectSelector animated:YES completion:nil];
}

@end
