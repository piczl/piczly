//
//  ChatViewController.m
//  Piczly
//
//  Created by sekhar on 24/04/15.
//  Copyright (c) 2015 mahesh. All rights reserved.
//

#import "ChatViewController.h"
#import "STBubbleTableViewCell.h"
#import "Message.h"
#import "PCZ_Chat.h"
#import "CreatePiczlyViewController.h"
#define TEXTFIELD_HEIGHT 70.0f
#define MAX_ENTRIES_LOADED 25

@interface ChatViewController ()<STBubbleTableViewCellDataSource,STBubbleTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *chatMessages;

@end

@implementation ChatViewController
{
    PiczlyUser                 *PCZ_UserForChat;
    __weak IBOutlet UIView     *viewTxtField_;
    CreatePiczlyViewController *CPVC_;
}

#pragma mark View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _chatMessages             = [[NSMutableArray alloc]init];
    self.view.backgroundColor = [UIColor clearColor];
    _chatData                 = [[NSMutableArray alloc]init];
    [self loadLocalChat];
    _chatMsgBtn.userInteractionEnabled=NO;
    self.chatTxtFld.delegate=self;
    [_chatTxtFld setValue:[UIColor blackColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    CGSize screenSize = [[UIScreen mainScreen] applicationFrame].size;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, screenSize.width, 60.0f)];
  _chatTVC.tableFooterView = headerView;
   
    
        // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(loadLocalChat) userInfo:nil repeats:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if (_chatcount<=0)
    {

        _viewForNoChatMsg.hidden=NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}



#pragma mark - UITableViewDatasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.chatMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Bubble Cell";
    
    STBubbleTableViewCell *cell = (STBubbleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[STBubbleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = self.chatTVC.backgroundColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.dataSource = self;
        cell.delegate = self;
    }
    
 

    
    Message *message =[Message messageWithString:[[self.chatMessages  valueForKey:@"Text"]objectAtIndex:indexPath.row]];
    PCZ_UserForChat=[[self.chatMessages valueForKey:@"piczlyUser"] objectAtIndex:indexPath.row];
   
    
   
    if([[[self.chatMessages valueForKey:@"piczlyUser"] objectAtIndex:indexPath.row] isEqual:[PFUser currentUser]] )
    {
        
      
        cell.authorType = STBubbleTableViewCellAuthorTypeSelf;
        cell.bubbleColor = STBubbleTableViewCellBubbleColorGr;
            cell.textLabel.text = message.message;
         cell.textLabel.textColor=[UIColor whiteColor];

        
    }
    else
    {
        
        if (PCZ_UserForChat.userImageFile)
        {
            PFFile *fileForProfileImage=PCZ_UserForChat.userImageFile;
            [fileForProfileImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
             {
                 Message * message=[Message messageWithString:[[self.chatMessages  valueForKey:@"Text"]objectAtIndex:indexPath.row] image:[UIImage imageWithData:data]];
                 cell.textLabel.text = message.message;
                 cell.textLabel.textColor=[UIColor blackColor];
                 cell.imageView.image = message.avatar;
                 cell.authorType = STBubbleTableViewCellAuthorTypeOther;
                 cell.bubbleColor = STBubbleTableViewCellBubbleColorGray;
                 
                 
             }];
        }
        else
        {
                cell.textLabel.text = message.message;

        }
     
      
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];

    

    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    Message *message = [Message messageWithString:[[self.chatMessages  valueForKey:@"Text"]objectAtIndex:indexPath.row] ];
   
    CGSize size;
    
      PCZ_UserForChat=[[self.chatMessages valueForKey:@"piczlyUser"] objectAtIndex:indexPath.row];
    
    if ([PCZ_UserForChat isEqual:[PFUser currentUser]])
    {
        size = [message.message boundingRectWithSize:CGSizeMake(self.chatTVC.frame.size.width - [self minInsetForCell:nil atIndexPath:indexPath] - STBubbleWidthOffset, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}
                                             context:nil].size;
        
    }
    else
    {
    if(PCZ_UserForChat.userImageFile)
    {
        size = [message.message boundingRectWithSize:CGSizeMake(self.chatTVC.frame.size.width - [self minInsetForCell:nil atIndexPath:indexPath] - STBubbleImageSize - 8.0f - STBubbleWidthOffset, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}
                                             context:nil].size;
    }
    else
    {
        size = [message.message boundingRectWithSize:CGSizeMake(self.chatTVC.frame.size.width - [self minInsetForCell:nil atIndexPath:indexPath] - STBubbleWidthOffset, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}
                                             context:nil].size;
    }
    }
    // This makes sure the cell is big enough to hold the avatar
    if(size.height + 15.0f < STBubbleImageSize + 4.0f && PCZ_UserForChat.userImageFile && ![PCZ_UserForChat isEqual:[PFUser currentUser]])
    {
        return STBubbleImageSize + 4.0f;
    }
    
    return size.height + 15.0f;
}

#pragma mark - STBubbleTableViewCellDataSource methods

- (CGFloat)minInsetForCell:(STBubbleTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        return 100.0f;
    }
    
    return 50.0f;
}

#pragma mark - STBubbleTableViewCellDelegate methods

- (void)tappedImageOfCell:(STBubbleTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
   
    
    Message *message = [Message messageWithString:[[self.chatMessages  valueForKey:@"Text"]objectAtIndex:indexPath.row]];
   
}


- (IBAction)backActn:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark
#pragma mark ChatActn

- (IBAction)messageSendActn:(id)sender
{
    
    if (_chatTxtFld.text.length>0) {
        // updating the table immediately
        [_chatTxtFld resignFirstResponder];
       
        NSArray *keys = [NSArray arrayWithObjects:@"Text", @"piczlyUser",@"piczlyForChat", @"time", nil];
        NSArray *objects = [NSArray arrayWithObjects:_chatTxtFld.text, _piczlyUser,_piczly, [NSDate date], nil];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        [ self.chatMessages addObject:dictionary];
    
        NSMutableArray *insertIndexPaths = [[NSMutableArray alloc] init];
        NSIndexPath *newPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [insertIndexPaths addObject:newPath];
        [_chatTVC beginUpdates];
        [_chatTVC insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_chatTVC endUpdates];
        [_chatTVC reloadData];
        [self performSelector:@selector(goToBottom) withObject:nil afterDelay:1.0];
      
        [self framesAdjusting];
          // going for the parsing
        NSDate *date=[NSDate date];
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"d.MM.yyyy HH:mm:ss a"];
        
        
        PCZ_Chat *obj=[PCZ_Chat object];
        obj.Text=_chatTxtFld.text;
        obj.time=[NSString stringWithFormat:@"%@", [dateFormat stringFromDate:date]];
        obj.piczlyForChat=self.piczly;
        obj.piczlyUser=self.piczlyUser;
        
        [obj saveInBackground];
        
        _chatTxtFld.text = @"";
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"please enter message" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
   
   
}
#pragma mark
#pragma mark TextField delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [self framesAdjusting];
    

    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.text)
    {
        _chatMsgBtn.userInteractionEnabled=YES;
        [ _chatMsgBtn setBackgroundImage:[UIImage imageNamed:@"send button"] forState:UIControlStateNormal];
    }
    else
    {
        return YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    CGRect tblViewFrame = _chatTVC.frame;
    CGRect txtFrame = viewTxtField_.frame;
    CGRect viewFrame=_viewForNoChatMsg.frame;
   
    

    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
      
        
        _chatTVC.frame = CGRectMake(tblViewFrame.origin.x, tblViewFrame.origin.y, tblViewFrame.size.width, 109);
        _viewForNoChatMsg.frame=CGRectMake(viewFrame.origin.x, _chatTVC.frame.origin.y+_chatTVC.frame.size.height-50, viewFrame.size.width, viewFrame.size.height);
        viewTxtField_.frame = CGRectMake(txtFrame.origin.x, _chatTVC.frame.origin.y+_chatTVC.frame.size.height+2, txtFrame.size.width, txtFrame.size.height);
        if(_chatMessages.count>0)
        {
        [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0];
        }
        
        
        
    } completion:nil];
  
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    _chatLbl.backgroundColor=[UIColor colorWithRed:64.0/255.0 green:175.0/255.0 blue:133.0/255.0 alpha:1.0];
  

}

-(void)framesAdjusting
{
    CGRect tblViewFrame = _chatTVC.frame;
    CGRect txtFrame = viewTxtField_.frame;
    CGRect viewFrame=_viewForNoChatMsg.frame;
    int isChatExists = ([self.chatMessages count]>0)?YES:NO;
    
    switch (isChatExists) {
        case YES: {
            _viewForNoChatMsg.hidden=YES;
        }
            break;
            case NO:
        {
            _viewForNoChatMsg.frame=CGRectMake(viewFrame.origin.x, 280, viewFrame.size.width, viewFrame.size.height);
        }
            break;
    }
       _chatTVC.frame = CGRectMake(tblViewFrame.origin.x, tblViewFrame.origin.y, tblViewFrame.size.width, 288);
    viewTxtField_.frame = CGRectMake(txtFrame.origin.x, _chatTVC.frame.origin.y+_chatTVC.frame.size.height+2, txtFrame.size.width, txtFrame.size.height);
    
    
}


#pragma mark - Parse

- (void)loadLocalChat
{
    PFQuery *query = [PFQuery queryWithClassName:@"ChatForParticularPiczly"];
    
   
    __block int totalNumberOfEntries = 0;
    [query orderByAscending:@"createdAt"];
    [query whereKey:@"piczlyForChat" equalTo:self.piczly];
    [query includeKey:@"piczlyUser"];
    [query includeKey:@"piczlyForChat"];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            // The count request succeeded. Log the count
           
             totalNumberOfEntries = number;
            if (totalNumberOfEntries > [_chatMessages count]) {
                
                int theLimit;
                if (totalNumberOfEntries-[_chatMessages count]>MAX_ENTRIES_LOADED) {
                    theLimit = MAX_ENTRIES_LOADED;
                }
                else {
                    theLimit = totalNumberOfEntries-(int)[_chatMessages count];
                }
                
                query.limit = theLimit;
               
                if(theLimit<[_chatMessages count])
                {
                    
                    [query orderByDescending:@"createdAt"];
                    
                    
                }

                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    if (!error) {
                        // The find succeeded.
                       
                        
                        if(theLimit<[_chatMessages count])
                        {
                            NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
                            NSArray *descriptors=[NSArray arrayWithObject: descriptor];
                             NSArray *reverseOrder=[objects sortedArrayUsingDescriptors:descriptors];
                            [_chatMessages addObjectsFromArray:reverseOrder];
                        }
                        else
                        {
                        [_chatMessages addObjectsFromArray:objects];
                        }
                        
                        NSMutableArray *insertIndexPaths = [[NSMutableArray alloc] init];
                        for (int ind = 0; ind < objects.count; ind++) {
                            NSIndexPath *newPath = [NSIndexPath indexPathForRow:ind inSection:0];
                            [insertIndexPaths addObject:newPath];
                        }
                        [_chatTVC beginUpdates];
                        [_chatTVC insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                        [_chatTVC endUpdates];
                        [_chatTVC reloadData];
                        [_chatTVC scrollsToTop];
                    } else {
                        // Log details of the failure
                        
                    }
                }];
            }
            if (number>0)
            {
                _viewForNoChatMsg.hidden=YES;
                [self performSelector:@selector(goToBottom) withObject:nil afterDelay:1.0];

            }
           
  
        } else {
            // The request failed, we'll keep the chatData count?
            number = (int)[_chatMessages count];
        }
    }];
   

}
#pragma mark
#pragma mark TableViewBottom

-(NSIndexPath *)lastIndexPath
{
    NSInteger lastSectionIndex = MAX(0, [_chatTVC numberOfSections] - 1);
    NSInteger lastRowIndex = MAX(0, [_chatTVC numberOfRowsInSection:lastSectionIndex] - 1);
    return [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
}
-(void)goToBottom
{
    NSIndexPath *lastIndexPath = [self lastIndexPath];
    
    [_chatTVC scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

#pragma mark Create New Piczly 

//creating new piczly based on Original Piczly

- (IBAction)createNewPiczlyActn:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    CPVC_=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePiczly"];

   
    CPVC_.piczly=_piczly;
    CPVC_.nameOfTheClass=@"Chat";

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:CPVC_];
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:navController animated:YES completion:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

@end
