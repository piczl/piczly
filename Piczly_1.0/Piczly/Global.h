////
////  Global.h
////  Breakk
////
////  Created by Skynet on 23/08/14.
////  Copyright (c) 2014 Umesh Naidu. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@interface Global : NSObject
//+(UIColor *)profileListNameColor;
//+(UIColor *)profileListPaymentColor;
//+(UILabel *)setPageTitle:(NSString *)labelName;
//+(UIColor *)getSegmentUnselectedClr;
//+(UIColor *)getSegmentselectedClr;
//+(void)alertDisplay:(NSString *)alertTxt;
//+(BOOL)emailValidation:(NSString *)emailTxt;
//+(void)networkAlert;
//+(NSString *)convertDateToString :(NSDate *)inputDate;
//@end
